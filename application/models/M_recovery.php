<?php
	class M_recovery extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function getRecoveryGroup($group_id){
			$sql = " SELECT a.username"
				 . " FROM `v_member_atlet` as a"
				 . " WHERE a.master_licence_group = '$group_id'";
			$query	= $this->db->query($sql);
			
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getMonotony($atlet,$year,$month){
			if($month == "all"){
				$sql = " SELECT a.*, b.mdlmonotony_date FROM mdlmonotony_distribute as a"
				 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
				 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
				 . " AND b.mdlmonotony_active = '0' ORDER BY b.mdlmonotony_date DESC";
			}else{
				$sql = " SELECT a.*, b.mdlmonotony_date FROM mdlmonotony_distribute as a"
					 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
					 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
					 . " AND MONTH(b.mdlmonotony_date) = '$month'"
					 . " AND b.mdlmonotony_active = '0' ORDER BY b.mdlmonotony_date DESC";
			}
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getRecovery($monotony_id){
			$sql = " SELECT c.* FROM mdlmonotony as a"
				. " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.mdlmonotony_id"
				. " LEFT JOIN mdlmonotony_detail as c on c.detail_reference = a.mdlmonotony_id"
				// . " LEFT JOIN master_pmc as d on d.detail_id_pmc = c.detail_id"
				. " WHERE b.distribute_id = '$monotony_id'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function getRecoveryDetail($date,$monotony_id,$atlet){
			$query	= $this->db->query(" SELECT a.*,b.* FROM master_recovery as a"
									 . " LEFT JOIN master_recovery_point as b on b.point_id = a.point_id"
									 . " WHERE a.atlet_id='$atlet' AND a.recovery_dttm = '$date'");
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function get_actual(){
			$sql = "SELECT * FROM mdlmonotony_intensity WHERE intensity_active = '1'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				$result = $query->result();
				return $result;
			}
		}
		
		function get_autocomplete($search_data,$group_id)
		{				
			$sql_atlet = " SELECT b.value_wellness,b.wellness_date,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
					   . " FROM master_role AS a"
					   . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
					   . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
					   . " LEFT JOIN master_role as d on d.role_id = b.role_id"
					   . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
					   . " WHERE b.name like '%$search_data%'"
					   . " AND a.group_id = '$group_id' AND b.role_type = 'ATL'";
			$query_atlet = $this->db->query($sql_atlet);
			if($query_atlet->num_rows()>0){
				return $query_atlet->result();
			}else{
				return false;
			}
		}
		
		function RecoveryID(){
			$tr = "RECO_";
			$year = date("Y");
			$month = date("m");
			$day = date("d");
			$sql = "SELECT left(a.recovery_id,5) as tr, mid(a.recovery_id,6,4) as fyear," 
				 . " mid(a.recovery_id,10,2) as fmonth, mid(a.recovery_id,12,2) as fday,"
				 . " right(a.recovery_id,4) as fno FROM master_recovery AS a"
				 . " where left(a.recovery_id,5) = '$tr' and mid(a.recovery_id,6,4) = '$year'"
				 . " and mid(a.recovery_id,10,2) = '$month' and mid(a.recovery_id,12,2)= '$day'"
				 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				 
			$result = $this->db->query($sql);	
				
			if($result->num_rows($result) > 0) {
				$row = $result->row();
				$tr = $row->tr;
				$fyear = $row->fyear;
				$fmonth = $row->fmonth;
				$fday = $row->fday;
				$fno = $row->fno;
				$fno++;
			} else {
				$tr = $tr;
				$fyear = $year;
				$fmonth = $month;
				$fday = $day;
				$fno = 0;
				$fno++;
			}
			if (strlen($fno)==1){
				$strfno = "000".$fno;
			} else if (strlen($fno)==2){
				$strfno = "00".$fno;
			} else if (strlen($fno)==3){
				$strfno = "0".$fno;
			} else if (strlen($fno)==4){
				$strfno = $fno;
			}
			
			$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

			return $id_goal;
		}
	}
?>