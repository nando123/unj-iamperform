<?php
	class M_home extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function hitungHari($awal,$akhir){
			$tglAwal = strtotime($awal);
			$tglAkhir = strtotime($akhir);
			$jeda = abs($tglAkhir - $tglAwal);
			return floor($jeda/(60*60*24));
		}
	
		function getWeek(){
			$query	= $this->db->query("SELECT * FROM d_year_weekly");
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return null;
			}
		}
	
		function listWeekByID($weekID){
			$query	= $this->db->query("SELECT * FROM d_year_weekly where uid = '$weekID'");
			if($query->num_rows()>0){
				$row		= $query->row();
				$week 		= $row->weekly;
				$start_date	= $row->start_date;
				$end_date	= $row->end_date;
				$year		= $row->year;
				
				return array($week,$start_date,$end_date,$year);
			}else{
				return null;
			}
		}
		
		function getCidera($username){
			$query	= $this->db->query("SELECT * FROM v_cidera_atlet_today WHERE username = '$username'");
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getGroup($username){
			$query = $this->db->query("SELECT a.*,b.* FROM v_users as a"
			 	 . " LEFT JOIN master_role as b on b.role_id = a.role_id"
				 . " WHERE a.username = '$username'");
			
			if($query->num_rows() > 0){
				foreach($query->result() as $row){
					$role_id = $row->role_id;
					$role_type = $row->role_type;
					$group_id = $row->group_id;
				}
			}
			
			if($username == "nando"){
				$sql = "SELECT *, master_group_id as groupcode FROM v_master_group_2 WHERE username = '$username' ORDER BY master_group_name ASC";
	
			}else{
					if ($group_id == "GW200"){
				$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' and master_group_category = 'GW200' ORDER BY master_group_name ASC";
			}else if ($group_id == "GW300"){
				$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' and master_group_category = 'GW300' ORDER BY master_group_name ASC";
			}else if ($group_id == "GW400"){
				$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' and master_group_category = 'GW400' ORDER BY master_group_name ASC";
			}else{
				$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' ORDER BY master_group_name ASC";
			}
			}
			//$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' ORDER BY master_group_name ASC";
		
			// if($role_type == "SATLAK" OR $role_type == "PRIMA" OR $role_type == "RCV" OR $role_type == "MLP"){
				// $sql = " select *, master_group_id as groupcode from master_group "
					 // . " where master_group_category in ('GW200','GW300','GW400') "
					 // . " ORDER BY master_group_name";
			// }else{
				// $sql = " SELECT a.groupcode, b.master_group_name, b.master_group_logo FROM v_master_group_sc as a"
					 // . " LEFT JOIN master_group as b on b.master_group_id = a.groupcode"
					 // . " WHERE a.username = '$username'";
			// }
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}			
		}
		
		
		function getAtlet($group_id){	
			$sql_atlet = "SELECT *, nomor_event as master_atlet_nomor_event FROM v_member_atlet WHERE master_licence_group = '$group_id' ORDER BY name ASC";
			// $sql_atlet = " SELECT b.wellness_date,b.value_wellness,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
					   // . " FROM master_role AS a"
					   // . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
					   // . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
					   // . " LEFT JOIN master_role as d on d.role_id = b.role_id"
					   // . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
					   // . " WHERE a.group_id = '$group_id' AND b.role_type = 'ATL'"
					   // . " ORDER BY b.name ASC";
			$query_atlet = $this->db->query($sql_atlet);
			if($query_atlet -> num_rows() > 0){
				$result = $query_atlet->result();
				return $result;
			}else{
				return false;
			}
		}
		
		function get_userData($username){
			$sql = " Select a.*, b.*, c.*, d.*, e.* From master_information_personal as a"
				 . " LEFT join master_information_apparel as e on e.username = a.master_atlet_username"
				 . " Left join users as d on d.username = a.master_atlet_username"
				 . " Left join master_role as b on b.role_id = d.role_id"
				 . " Left join master_group as c on c.master_group_id = b.group_id"
				 . " where a.master_atlet_username = '$username' LIMIT 1";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				return $result;
			}else{
				return false;
			}			
		}
		
		function savePersonal($data){
			$sql_select = "select * from master_information_personal where master_atlet_username = '$data[username]'";
			$query_select = $this->db->query($sql_select);
			if($query_select->num_rows() > 0){
				$sql = "Update master_information_personal set master_atlet_nama = '$data[nama]',master_atlet_address = '$data[alamat]',master_atlet_telp = '$data[telp]',master_atlet_handphone = '$data[handphone]',"
					 . " master_atlet_email = '$data[email]', master_atlet_tempat_lahir = '$data[tempat_lahir]', master_atlet_tanggal_lahir = '$data[tgl_lahir]',master_atlet_agama = '$data[agama]',"
					 . " master_atlet_tinggi_badan = '$data[tinggi_badan]', master_atlet_golongan_darah = '$data[golongan_darah]', master_atlet_jenis_kelamin = '$data[jenis_kelamin]',"
					 . " master_atlet_pendidikan = '$data[pendidikan]', master_atlet_pekerjaan = '$data[pekerjaan]', master_atlet_status = '$data[status]', master_atlet_nomor_event = '$data[nomor_event]', nomor_ktp = '$data[nomor_ktp]', npwp = '$data[npwp]' where master_atlet_username = '$data[username]'";
				$query = $this->db->query($sql);
				$sql1 = "update users set name = '$data[nama]' where username = '$data[username]'";
				$query1 = $this->db->query($sql1);
			}else{
				$sql = "insert into master_information_personal (master_atlet_username,master_atlet_nama,master_atlet_address,master_atlet_telp,master_atlet_handphone,master_atlet_email,"
					 . " master_atlet_tempat_lahir,master_atlet_tanggal_lahir,master_atlet_agama,master_atlet_tinggi_badan,master_atlet_golongan_darah,"
					 . " master_atlet_jenis_kelamin,master_atlet_pendidikan,master_atlet_pekerjaan,master_atlet_status,master_atlet_nomor_event,npwp, nomor_ktp)"
					 . " values('$data[username]','$data[nama]','$data[alamat]','$data[telp]','$data[handphone]','$data[email]','$data[tempat_lahir]','$data[tgl_lahir]','$data[agama]',"
					 . " '$data[tinggi_badan]','$data[golongan_darah]','$data[jenis_kelamin]','$data[pendidikan]','$data[pekerjaan]','$data[status]','$data[nomor_event]','$data[npwp]','$data[nomor_ktp]')";			
				$query = $this->db->query($sql);
				$sql1 = "update users set name = '$data[nama]' where username = '$data[username]'";
				$query1 = $this->db->query($sql1);
			}
			// echo $sql;
			// return;
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		function get_userFamilyData($username){
			$sql = "Select * From master_information_family where master_atlet_keluarga_username = '$username'";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				return $result;
			}else{
				return false;
			}			
		}
		
		function updateFamily($data){
			$sql_select = "select * from master_information_family where master_atlet_keluarga_username = '$data[username]'";
			$query_select = $this->db->query($sql_select);
			if($query_select->num_rows() > 0){
				$sql = "Update master_information_family set master_atlet_keluarga_orangtua = '$data[orangtua]',master_atlet_keluarga_pasangan = '$data[pasangan]',"
					 . " master_atlet_keluarga_anak = '$data[anak]', master_atlet_keluarga_nomor_lain = '$data[nomor_lain]'"
					 . " where master_atlet_keluarga_username = '$data[username]'";
				$query = $this->db->query($sql);
			}else{
				$sql = "insert into master_information_family (master_atlet_keluarga_username,master_atlet_keluarga_orangtua,master_atlet_keluarga_pasangan,"
					 . " master_atlet_keluarga_anak,master_atlet_keluarga_nomor_lain)"
					 . " values('$data[username]','$data[orangtua]','$data[pasangan]','$data[anak]','$data[nomor_lain]')";			
				$query = $this->db->query($sql);
			}
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		function get_userHealthData($username){
			$sql = "Select * From master_information_health where username = '$username'";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				return $result;
			}else{
				return false;
			}
		}
		
		function updateHealth($data){
			$sql_select = "select * from master_information_health where username = '$data[username]'";
			$query_select = $this->db->query($sql_select);
			if($query_select->num_rows() > 0){
				$this->db->where('username', $data['username']);
				$this->db->update('master_information_health', $data);
				return true;
			}else{
				$this->db->insert('master_information_health', $data);
				return true;
			}
		}
		
		function get_userClubData($username){
			$sql = "Select * From master_information_club where username = '$username'";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				return $result;
			}else{
				return false;
			}
		}
		
		function updateClub($data){
			$sql_select = "select * from master_information_club where username = '$data[username]'";
			$query_select = $this->db->query($sql_select);
			if($query_select->num_rows() > 0){
				$this->db->where('username', $data['username']);
				$this->db->update('master_information_club', $data);
				return true;
			}else{
				$this->db->insert('master_information_club', $data);
				return true;
			}
		}
		
		function updateApparel($data){
			$sql_select = "select * from master_information_apparel where username = '$data[username]'";
			$query_select = $this->db->query($sql_select);
			if($query_select->num_rows() > 0){
				$this->db->where('username', $data['username']);
				$this->db->update('master_information_apparel', $data);
				return true;
			}else{
				$this->db->insert('master_information_apparel', $data);
				return true;
			}
		}
	}
?>