<?php
	class M_wellness extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function getWellness($atlet,$month,$year){
			$sql = " SELECT * FROM v_master_kondisi WHERE username = '$atlet'"
				 . " AND MONTH(created_dttm) = '$month'"
				 . " AND YEAR(created_dttm) = '$year'"
				 . " order by created_dttm desc";
			
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				
				$result = $query->result();
				
				return $result;
			}
			else
			{
				return false;
			}
		}
		
		function get_autocomplete($search_data,$group_id)
		{
			$sql_atlet = " SELECT b.wellness_date,b.value_wellness,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
					   . " FROM master_role AS a"
					   . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
					   . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
					   . " LEFT JOIN master_role as d on d.role_id = b.role_id"
					   . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
					   . " WHERE b.name like '%$search_data%'"
					   . " AND a.group_id = '$group_id' AND b.role_type = 'ATL'";
			$query_atlet = $this->db->query($sql_atlet);
			if($query_atlet -> num_rows() > 0){
				$result = $query_atlet->result();
				return $result;
			}else{
				return false;
			}
		}
		
		function get_data_diri_atlet($id){
			echo $sql = " SELECT a.*, b.group_id"
				 . " FROM master_user_pelatih AS a"
				 . " LEFT JOIN master_role AS b ON b.role_id = a.master_role_id"
				 . " WHERE a.master_user_username = '$id'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				print_r($query);
			}
		}
		
		function getNilai(){
			$sql = "Select * From master_kondisi_value";
			
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				
				$result = $query->result();
				
				return $result;
			}
			else
			{
				return false;
			}
		}
		
		function saveWellness($data,$total){
			$now	= date("Y-m-d");
			$insert = $this->db->insert('master_kondisi', $data);
			$sql = "UPDATE users SET value_wellness = '$total',wellness_date = '$now' WHERE username = '$data[username]'";
			$query = $this->db->query($sql);
			
			if($insert){
				return true;
			}else{
				return false;
			}
		}
		
		function id_kondisi(){
			$tr = "KOND_";
			$year = date("Y");
			$month = date("m");
			$day = date("d");
			$sql = "SELECT left(a.master_kondisi_id,5) as tr, mid(a.master_kondisi_id,6,4) as fyear," 
				 . " mid(a.master_kondisi_id,10,2) as fmonth, mid(a.master_kondisi_id,12,2) as fday,"
				 . " right(a.master_kondisi_id,4) as fno FROM master_kondisi AS a"
				 . " where left(a.master_kondisi_id,5) = '$tr' and mid(a.master_kondisi_id,6,4) = '$year'"
				 . " and mid(a.master_kondisi_id,10,2) = '$month' and mid(a.master_kondisi_id,12,2)= '$day'"
				 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				 
			$result = $this->db->query($sql);	
				
			if($result->num_rows($result) > 0) {
				$row = $result->row();
				$tr = $row->tr;
				$fyear = $row->fyear;
				$fmonth = $row->fmonth;
				$fday = $row->fday;
				$fno = $row->fno;
				$fno++;
			} else {
				$tr = $tr;
				$fyear = $year;
				$fmonth = $month;
				$fday = $day;
				$fno = 0;
				$fno++;
			}
			if (strlen($fno)==1){
				$strfno = "000".$fno;
			} else if (strlen($fno)==2){
				$strfno = "00".$fno;
			} else if (strlen($fno)==3){
				$strfno = "0".$fno;
			} else if (strlen($fno)==4){
				$strfno = $fno;
			}
			
			$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

			return $id_goal;
		}
	}
?>