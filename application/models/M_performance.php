<?php
	class M_performance extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function getKPFAtletID($id_performance){
			$sql = "select a.jenis_performance FROM master_performance as a where a.id_performance = '$id_performance'";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				$result = $query->result();
				// foreach ($result as $key) {
					// $jenis = $key->jenis_performance;
					// if($jenis == 'Physical'){
						// $sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 // . " benchmark as benchmark, goal as goal, current as current, option"
								 // . " From master_performance_detail where id_performance = '$id_performance'";
						// $query_phy = $this->db->query($sql_phy);
						// if($query_phy -> num_rows() > 0){
							// $result_per = $query_phy->result();
						// }
					// }else if($jenis == 'Technical'){
						// $sql_per = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 // . " benchmark as benchmark, goal as goal, current as current, option"
								 // . " From master_performance_detail where id_performance = '$id_performance'";
						// $query_per = $this->db->query($sql_per);
						// if($query_per -> num_rows() > 0){
							// $result_per = $query_per->result();
						// }
					// }else if($jenis == 'Psichology'){
						// $sql_per = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 // . " benchmark as benchmark, goal as goal, current as current, option"
								 // . " From master_performance_detail where id_performance = '$id_performance'";
						// $query_per = $this->db->query($sql_per);
						// if($query_per -> num_rows() > 0){
							// $result_per = $query_per->result();
						// }
					// }
				// }
				$sql_per = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
						 . " benchmark as benchmark, goal as goal, current as current, type"
						 . " From master_performance_detail where id_performance = '$id_performance'";
				$query_per = $this->db->query($sql_per);
				if($query_per -> num_rows() > 0){
					$result_per = $query_per->result();
				}
				return array($result, $result_per);
			}else{
				return false;
			}
		}
		
		function get_autocomplete($search_data,$name,$role_id)
		{
			$sql = " SELECT a.group_id"
				 . " FROM master_role AS a"
				 . " LEFT JOIN users AS b ON b.role_id = a.role_id"
				 . " WHERE b.role_id = '$role_id'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				$row = $query->row();
				$group_id = $row->group_id;
				
				$sql_atlet = " SELECT a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
						   . " FROM master_role AS a"
						   . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
						   . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
						   . " LEFT JOIN master_role as d on d.role_id = b.role_id"
						   . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
						   . " WHERE b.name like '%$search_data%'"
						   . " AND a.group_id = '$group_id' AND b.role_type = 'ATL'";
				return $query_atlet = $this->db->query($sql_atlet);
			}else{
				return false;
			}
		}
		
		function delete($id,$jenis){
			$sql = "DELETE a,b FROM master_performance as a"
				. " LEFT JOIN master_performance_detail as b on b.id_performance = a.id_performance"
				. " WHERE a.id_performance = '$id'";
			$query = $this->db->query($sql);
			
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		function get_data_diri_atlet($id){
			echo $sql = " SELECT a.*, b.group_id"
				 . " FROM master_user_pelatih AS a"
				 . " LEFT JOIN master_role AS b ON b.role_id = a.master_role_id"
				 . " WHERE a.master_user_username = '$id'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				print_r($query);
			}
		}
		
		function getKPFAtlet($atlet){
			$sql = " select a.*, b.role_id, c.master_atlet_nomor_event, b.name, b.gambar, e.master_group_name"
				 . " FROM master_performance as a"
				 . " LEFT JOIN users as b on b.username = a.id_user_atlet"
				 . " LEFT JOIN master_information_personal as c on c.master_atlet_username = a.id_user_atlet"
				 . " LEFT JOIN master_role as d on d.role_id = b.role_id"
				 . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
				 . " where id_user_atlet='$atlet' order by a.id_performance desc";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}

		function get_all_data_atl($id_user){
			$sql = " select a.*, b.role_id, c.master_atlet_nomor_event, b.name, b.gambar, e.master_group_name FROM master_performance as a"
				 . " LEFT JOIN users as b on b.username = a.id_user_atlet"
				 . " LEFT JOIN master_information_personal as c on c.master_atlet_username = a.id_user_atlet"
				 . " LEFT JOIN master_role as d on d.role_id = b.role_id"
				 . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
				 . " where a.id_user_atlet = '$id_user' order by a.id_performance desc";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}

		function get_all_data_atlet($id_performance){
			$sql = "select a.* FROM master_performance as a where a.id_performance = '$id_performance'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				$result = $query->result();
				foreach ($result as $key) {
					$jenis = $key->jenis_performance;
					if($jenis == 'Physical'){
						$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 . " benchmark as benchmark, goal as goal, current as current"
								 . " From master_performance_detail where id_performance = '$id_performance'";
						$query_phy = $this->db->query($sql_phy);
						if($query_phy -> num_rows() > 0){
							$result_per = $query_phy->result();
						}
					}else if($jenis == 'Technical'){
						$sql_per = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 . " benchmark as benchmark, goal as goal, current as current"
								 . " From master_performance_detail where id_performance = '$id_performance'";
						$query_per = $this->db->query($sql_per);
						if($query_per -> num_rows() > 0){
							$result_per = $query_per->result();
						}
					}else if($jenis == 'Psichology'){
						$sql_per = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
								 . " benchmark as benchmark, goal as goal, current as current"
								 . " From master_performance_detail where id_performance = '$id_performance'";
						$query_per = $this->db->query($sql_per);
						if($query_per -> num_rows() > 0){
							$result_per = $query_per->result();
						}
					}
				}
				return array($result, $result_per);
			}else{
				return false;
			}
		}
		
		function saveNewperformance($data){
			list($id_performance,$username,$atlet,$current_dttm,$komponen,$goal,$current,$jenis,$option) = $data;
			$values = array();

			for($i = 1; $i <= count($komponen); $i++){
				$values[] = "('','$id_performance','{$komponen[$i]}','{$goal[$i]}','{$current[$i]}','$option[$i]')";
			}

			$values_string = implode(',',$values);
			$sql_phy = "insert into master_performance_detail (id_performance_detail,id_performance,komponen,goal,current,type) values $values_string";
			$query_phy = $this->db->query($sql_phy);
			if ($query_phy) {
				return true;
			} else {
			   return false;
			}
		}
		
		function save_performance($data){
			$id_performance = $this->id_performance();

			$komponens 	= $data['komponen'];
			$messo 		= $data['messo'];
			$goal 		= $data['goal'];
			$current 	= $data['current'];
			$option		= $data['option'];
			$now 		= date('Y-m-d H:i:s');
			$arrAtlet	= $data['arrAtlet'];
			
			for($h=0;$h<count($arrAtlet);$h++){
				$id_performance = $this->id_performance();
				$values = array();
				
				$sql_add = " insert into master_performance (id_performance,id_user_atlet,jenis_performance,created_user_id,created_dttm,catatan,messo)"
						 . " values ('$id_performance','{$arrAtlet[$h]}','$data[jenis]','$data[username]','$now','$data[catatan]','$messo')";
				$query_add = $this->db->query($sql_add);

				for($i = 1; $i <= count($komponens); $i++){
					//$values[] = "('','$id_performance','{$komponens[$i]}','{$goal[$i]}','{$current[$i]}','{$option[$i]}')";
					$this->db->query("insert into master_performance_detail (id_performance_detail,id_performance,komponen,goal,current,type) values (NULL, '$id_performance','$komponens[$i]','$goal[$i]','$current[$i]','$option[$i]')");
				}

				/*
				$values_string = implode(',',$values);

				$sql_add = " insert into master_performance (id_performance,id_user_atlet,jenis_performance,created_user_id,created_dttm,catatan,messo)"
						 . " values ('$id_performance','{$arrAtlet[$h]}','$data[jenis]','$data[username]','$now','$data[catatan]','$messo')";
				$query_add = $this->db->query($sql_add);
				
				//$this->db->query("insert into a_text(id,ktext) values (NULL, '$values[]')");
				
			
				if($query_add){				
					$sql_phy = "insert into master_performance_detail (id_performance_detail,id_performance,komponen,goal,current,option) values $values_string";
					$query_phy = $this->db->query($sql_phy);
				}
				*/
				
				
			}
			
			return true;
		}
		
		function id_performance(){
			$tr = "PERF_";
			$year = date("Y");
			$month = date("m");
			$day = date("d");
			$sql = "SELECT left(a.id_performance,5) as tr, mid(a.id_performance,6,4) as fyear," 
				 . " mid(a.id_performance,10,2) as fmonth, mid(a.id_performance,12,2) as fday,"
				 . " right(a.id_performance,4) as fno FROM master_performance AS a"
				 . " where left(a.id_performance,5) = '$tr' and mid(a.id_performance,6,4) = '$year'"
				 . " and mid(a.id_performance,10,2) = '$month' and mid(a.id_performance,12,2)= '$day'"
				 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				 
			$result = $this->db->query($sql);	
				
			if($result->num_rows($result) > 0) {
				$row = $result->row();
				$tr = $row->tr;
				$fyear = $row->fyear;
				$fmonth = $row->fmonth;
				$fday = $row->fday;
				$fno = $row->fno;
				$fno++;
			} else {
				$tr = $tr;
				$fyear = $year;
				$fmonth = $month;
				$fday = $day;
				$fno = 0;
				$fno++;
			}
			if (strlen($fno)==1){
				$strfno = "000".$fno;
			} else if (strlen($fno)==2){
				$strfno = "00".$fno;
			} else if (strlen($fno)==3){
				$strfno = "0".$fno;
			} else if (strlen($fno)==4){
				$strfno = $fno;
			}
			
			$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

			return $id_goal;
		}
	}
?>