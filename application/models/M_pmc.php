<?php
	class M_pmc extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function searchAtlet($name,$group_id)
		{
			$sql_atlet = " SELECT b.value_wellness,b.wellness_date,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
			   . " FROM master_role AS a"
			   . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
			   . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
			   . " LEFT JOIN master_role as d on d.role_id = b.role_id"
			   . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
			   . " WHERE b.name like '%$name%'"
			   . " AND a.group_id = '$group_id' AND b.role_type = 'ATL'";
			$query_atlet = $this->db->query($sql_atlet);
			
			if($query_atlet->num_rows()>0){
				return $query_atlet->result();
			}else{
				return false;
			}
		}
		
		function getPmcYear($year,$atlet){
			$sql = " SELECT a.distribute_monotony, a.distribute_athlete, b.*"
				 . " FROM `mdlmonotony_distribute` as a"
				 . " LEFT JOIN mdlmonotony_detail as b on b.detail_reference = a.distribute_monotony"
				 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.detail_date) = '$year'";
				 
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getPmcMonth($year,$month,$atlet){
			$sql = " SELECT a.distribute_monotony, a.distribute_athlete, b.*"
				 . " FROM `mdlmonotony_distribute` as a"
				 . " LEFT JOIN mdlmonotony_detail as b on b.detail_reference = a.distribute_monotony"
				 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.detail_date) = '$year' AND MONTH(b.detail_date) = '$month' AND distribute_active ='1'";
				 
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getMonotony($atlet,$year,$month){
			if($month == "all"){
				$sql = " SELECT a.*, b.mdlmonotony_date FROM mdlmonotony_distribute as a"
					 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
					 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
					 . " AND b.mdlmonotony_active = '0' ORDER by b.mdlmonotony_date DESC";
			}else{
				$sql = " SELECT a.*, b.mdlmonotony_date FROM mdlmonotony_distribute as a"
					 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
					 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
					 . " AND MONTH(b.mdlmonotony_date) = '$month'"
					 . " AND b.mdlmonotony_active = '0' ORDER by b.mdlmonotony_date DESC";
			}
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getPmcData($monotony_id){
			$sql = " SELECT c.* FROM mdlmonotony as a"
				. " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.mdlmonotony_id"
				. " LEFT JOIN mdlmonotony_detail as c on c.detail_reference = a.mdlmonotony_id"
				// . " LEFT JOIN master_pmc as d on d.detail_id_pmc = c.detail_id"
				. " WHERE b.distribute_id = '$monotony_id'";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function getPmcMicro($dttm,$atlet){
			$detail = " SELECT a.*,e.intensity_name,f.intensity_name as actual_name FROM mdlmonotony_detail as a"
				 . " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.detail_reference "
				 . " LEFT JOIN mdlmonotony_intensity as e on e.intensity_id = a.detail_intensity"
				 . " LEFT JOIN mdlmonotony_intensity as f on f.intensity_id = a.detail_actual"
				 . " WHERE a.detail_date = '$dttm' "
				 . " AND b.distribute_athlete = '$atlet'"
				 . " ORDER BY a.detail_date ASC";
			$det_res = $this->db->query($detail);
			if($det_res->num_rows()>0){
				$det_res = $det_res->result();
			}else{
				$det_res = false;
			}
			
			$sql = " SELECT a.detail_id, a.detail_reference,a.detail_date FROM mdlmonotony_detail as a"
				 . " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.detail_reference "
				 . " WHERE a.detail_date = '$dttm' "
				 . " AND b.distribute_athlete = '$atlet'"
				 . " GROUP BY a.detail_date"
				 . " ORDER BY a.detail_date";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				$result = $query->result();
			}else{
				$result = false;
			}
			
			return array($result,$det_res);
		}
		
		function getPmcWeek($monotony_id){
			$sql = " SELECT f.intensity_name as actual_name,e.intensity_name,c.*,b.*,c.detail_id as did FROM mdlmonotony as a"
				. " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.mdlmonotony_id"
				. " LEFT JOIN mdlmonotony_detail as c on c.detail_reference = a.mdlmonotony_id"
				// . " LEFT JOIN master_pmc as d on d.detail_id = c.detail_id"
				. " LEFT JOIN mdlmonotony_intensity as e on e.intensity_id = c.detail_intensity"
				. " LEFT JOIN mdlmonotony_intensity as f on f.intensity_id = c.detail_actual"
				. " WHERE b.distribute_id = '$monotony_id' ORDER BY c.detail_date ASC";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function setPmcDetail($kegiatan,$detail_id,$monotony_id,$username,$atlet){		
			$pmc_id = $this->pmc_id();	
			$pmc = " INSERT INTO master_pmc (pmc_id,monotony_id,detail_id,created_user_id,atlet_id,"
				 . " detail_kegiatan,created_dttm) values "
				 . " ('$pmc_id','$monotony_id','$detail_id','$username','$atlet','$kegiatan',now())";
			$pmc_q = $this->db->query($pmc);
			if($pmc_q) {
				return true;
			} else {
			   return false;
			}
		}
		
		function save_pmc($data){
			$pmc_id = $this->pmc_id();
			$kegiatan = $data['kegiatan'];
			// print_r($kegiatan);
			$sql = " INSERT INTO master_pmc (pmc_id,detail_id_pmc,created_user_id,atlet_id,created_dttm)"
				 . " values ('$pmc_id','$data[detail_id]','$data[pelatih]','$data[atlet]',now())";
			$query = $this->db->query($sql);
			
			$pmc = "INSERT INTO master_pmc_detail (pmc_id,detail_kegiatan,detail_dttm) values ('$pmc_id','$kegiatan',now())";
			$pmc_q = $this->db->query($pmc);
			if ($pmc_q) {
				return true;
			} else {
			   return false;
			}
		}
		
		function update_pmc_detail($kegiatan,$pmc_id){			
			$pmc = "INSERT INTO master_pmc_detail (pmc_id,detail_kegiatan,detail_dttm) values ('$pmc_id','$kegiatan',now())";
			$pmc_q = $this->db->query($pmc);
			if ($pmc_q) {
				return true;
			} else {
			   return false;
			}
		}
		
		function update_detail($data){
			$kegiatan = $data['kegiatan'];
			$id		  = $data['id'];
			
			$values = array();

			// for($i = 1; $i = count($kegiatan); $i++){
			$pmc = "UPDATE master_pmc_detail SET  detail_kegiatan = '$kegiatan' WHERE pmc_id = '$data[pmc_id]' AND id = '$id'";
			$pmc_q = $this->db->query($pmc);
			
			if ($pmc_q) {
				
			} else {
			   return false;
			}
			// }
			return true;
		}
		
		function setActual($did,$monotony_id,$actual){
			$sql = "UPDATE mdlmonotony_detail SET detail_actual = '$actual' WHERE detail_id = '$did' AND detail_reference = '$monotony_id'";
			$query = $this->db->query($sql);
			
			return true;
		}
		
		function getMonotonyWeek($monotony_id){
			$sql = " SELECT f.intensity_name as actual_name,e.intensity_name,c.*,b.*,c.detail_id as did FROM mdlmonotony as a"
				. " LEFT JOIN mdlmonotony_distribute as b on b.distribute_monotony = a.mdlmonotony_id"
				. " LEFT JOIN mdlmonotony_detail as c on c.detail_reference = a.mdlmonotony_id"
				// . " LEFT JOIN master_pmc as d on d.detail_id = c.detail_id"
				. " LEFT JOIN mdlmonotony_intensity as e on e.intensity_id = c.detail_intensity"
				. " LEFT JOIN mdlmonotony_intensity as f on f.intensity_id = c.detail_actual"
				. " WHERE b.distribute_id = '$monotony_id' ORDER BY c.detail_date ASC";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
				
		function pmc_id(){
			$tr = "PMCA_";
			$year = date("Y");
			$month = date("m");
			$day = date("d");
			$sql = "SELECT left(a.pmc_id,5) as tr, mid(a.pmc_id,6,4) as fyear," 
				 . " mid(a.pmc_id,10,2) as fmonth, mid(a.pmc_id,12,2) as fday,"
				 . " right(a.pmc_id,4) as fno FROM master_pmc AS a"
				 . " where left(a.pmc_id,5) = '$tr' and mid(a.pmc_id,6,4) = '$year'"
				 . " and mid(a.pmc_id,10,2) = '$month' and mid(a.pmc_id,12,2)= '$day'"
				 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				 
			$result = $this->db->query($sql);	
				
			if($result->num_rows($result) > 0) {
				$row = $result->row();
				$tr = $row->tr;
				$fyear = $row->fyear;
				$fmonth = $row->fmonth;
				$fday = $row->fday;
				$fno = $row->fno;
				$fno++;
			} else {
				$tr = $tr;
				$fyear = $year;
				$fmonth = $month;
				$fday = $day;
				$fno = 0;
				$fno++;
			}
			if (strlen($fno)==1){
				$strfno = "000".$fno;
			} else if (strlen($fno)==2){
				$strfno = "00".$fno;
			} else if (strlen($fno)==3){
				$strfno = "0".$fno;
			} else if (strlen($fno)==4){
				$strfno = $fno;
			}
			
			$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

			return $id_goal;
		}
	}
?>