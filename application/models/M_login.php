<?php	
	class M_login extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
            $this->load->database();
		}
				
		function login($username,$password)
		{
			$query = $this->db->query("SELECT a.*,b.* FROM v_users as a"
			 	 . " LEFT JOIN master_role as b on b.role_id = a.role_id"
				 . " WHERE a.username = '$username'");
			
			if($query->num_rows() > 0){
				$row 	= $query->row();
				$salt	= $row->salt;
				$enc_p	= $row->encrypted_password;
				$code	= $row->license_code;
				$hash 	= $this->checkhashSSHA($salt, $password);
				
				// if($code == "ADMP"){
					if ($enc_p == $hash) {
						// user authentication details are correct
						$result = $query->result();
						return $result;
					}else{
						return "wrong_password";
					}
				// }else{
					// echo "not_allowed";
					// return;
				// }
			}
			else
			{
				return "error";
			}
		}

		function checkhashSSHA($salt, $password) {

			$hash = base64_encode(sha1($password . $salt, true) . $salt);

			return $hash;
		}
		
		function createUser($data) {
			list($name,$username,$licence_code,$device_id,$device_name,$password,$province,$birth,$gender) = $data;
			
			$sql 	= "SELECT username from users WHERE username = '$username'";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0) {
				echo "exist";
				return;
			}
			
			$sql = "SELECT license_code from users WHERE license_code = '$licence_code'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				echo "licence_exist";
				return;
			}
			
			$sql = "SELECT master_licence_code from master_licence WHERE master_licence_code = '$licence_code' and master_licence_is_activate = '2'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				echo "licence_suspen";
				return;
			}
			
			$result = "SELECT master_licence_code from master_licence WHERE master_licence_code = '$licence_code'";
			$no_of_rows = $this->db->query($result);
			if ($no_of_rows->num_rows() == 0) {
				echo "licence_not";
				return;
			}
			
			$password_md5 = md5($password);
			$uuid = uniqid('', true);
			$hash = $this->hashSSHA($password);
			$encrypted_password = $hash["encrypted"]; // encrypted password
			$salt = $hash["salt"]; // salt
			$birthDttm = date("Y-m-d", strtotime($birth));
			
			$sql = "SELECT master_licence_type, master_licence_user_groupcode, master_licence_group FROM master_licence WHERE master_licence_code = '$licence_code'";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				$key	= $query->row();
				$master_licence_type = $key->master_licence_type;
				$master_licence_user_groupcode = $key->master_licence_user_groupcode;
				$groupcode = $key->master_licence_group;
				
			}

				
				
			if($master_licence_user_groupcode == 'ALL'){
				$this->db->query("INSERT INTO user_access_alias(username,role_id,role_type) VALUES ('$username','RL_ALL_ACCESS','ALL')");
			}
				
			if($master_licence_user_groupcode == 'HPD'){
				$this->db->query("INSERT INTO user_access_alias(username,role_id,role_type) VALUES ('$username','$master_licence_type','HPD')");
				$this->db->query("INSERT INTO master_group_sc(username,groupcode) VALUES ('$username','$groupcode')");
			}
			
			$this->db->query("UPDATE master_licence SET master_licence_is_activate = '1' WHERE master_licence_code = '$licence_code'");
				 
			$result = $this->db->query("INSERT INTO users(unique_id, name, username, encrypted_password, salt, created_at,updated_at,license_code,"
				 . " registered_device_id,registered_device_name,gambar,login_device_id,login_device_name, role_id, role_type) VALUES "
				 . " ('$uuid', '$name', '$username', '$encrypted_password', '$salt', NOW(),NOW(),'$licence_code','$device_id',"
				 . " '$device_id','http://unj.iamperform.com/assets/icons/icon_profile.png',"
				 . "'$device_id','$device_id','$master_licence_type','$master_licence_user_groupcode')");
				   // check for successful store
			if($result) {
				
				$this->db->query("INSERT INTO master_information_personal (master_atlet_username, master_atlet_nomor_event, master_atlet_nama, master_atlet_address, master_atlet_telp, master_atlet_handphone, master_atlet_email, master_atlet_tempat_lahir, master_atlet_tanggal_lahir, master_atlet_agama, master_atlet_tinggi_badan, master_atlet_golongan_darah, master_atlet_jenis_kelamin, master_atlet_pendidikan, master_atlet_pekerjaan, master_atlet_status, nomor_ktp, nomor_passport, npwp, url_image_ktp, url_image_passport, url_image_npwp, provinsi_id) 
							VALUES ('$username', '', '', '', '', '', '', '', '$birthDttm', '', '', '', '$gender', '', '', '', '', '', '', '', '', '', '$province')");
				echo "sukses";
			} else {
				echo "failure";
			}
		}
		
		function hashSSHA($password) {
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10);
			$encrypted = base64_encode(sha1($password . $salt, true) . $salt);
			$hash = array("salt" => $salt, "encrypted" => $encrypted);
			return $hash;
		}
	}
?>