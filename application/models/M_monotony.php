<?php
	class M_monotony extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function saveMonotony($args,$user_id,$atlet){
			for($k=0; $k<count($atlet); $k++) {
				$monotony_id = $this->uniqekey();
				list($target,$StartDay,$phase,$PostSession,$PostScale,$PostVolume)=$args;
				
				$query	= $this->db->query(" SELECT * FROM mdlmonotony_distribute WHERE distribute_monotony "
						. " IN ( SELECT MAX(distribute_monotony)FROM mdlmonotony_distribute WHERE distribute_athlete = '{$atlet[$k]}')");
				
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyLastWeek[$k] = $row->distribute_monotony;
					
					$cek = $this->db->query("SELECT SUM(detail_intensity*detail_volume) as dailyMonotony FROM mdlmonotony_detail WHERE detail_reference = '{$monotonyLastWeek[$k]}'");
					
					if($cek->num_rows()>0){
						$key	= $cek->result();
						$tmpM	= "";
						foreach($key as $row){
							$dailyMonotony = $row->dailyMonotony;
						}
					}
				}else{
					$dailyMonotony = 0;
				}
				
				$dttm = $StartDay." ".date("H:i:s");
				
				$sql = " INSERT INTO mdlmonotony (mdlmonotony_id,mdlmonotony_user,mdlmonotony_target"
					 . " ,mdlmonotony_last,mdlmonotony_detail,mdlmonotony_date,phase) values ('$monotony_id','$user_id','$target'"
					 . " ,'$dailyMonotony','$monotony_id','$dttm','$phase')";
				$query = $this->db->query($sql);
				
				$sql1 = " INSERT INTO mdlmonotony_distribute (distribute_id,distribute_monotony,distribute_athlete"
					 . " ,distribute_creator,distribute_active)"
					 . " values ('$monotony_id','$monotony_id','{$atlet[$k]}','$user_id','1')";
				$query1 = $this->db->query($sql1);
				
				for($i=0; $i<count($PostSession); $i++) {
						
					$w 		= date('w', 		strtotime($StartDay. ' +'.($i).' day'));
					$Date 	= date('Y-m-d', 	strtotime($StartDay. ' +'.($i).' day'));
						
					for($j=0; $j<count($PostSession[$i]); $j++) {
						$detail_id = $this->uniqekey();			
						$sqldetail = " INSERT INTO mdlmonotony_detail (detail_id,detail_reference,detail_day,"
									." detail_date,detail_session,detail_intensity,detail_volume) "
									." values ('$detail_id','$monotony_id','$w','$Date','{$PostSession[$i][$j]}',"
									." '{$PostScale[$i][$j]}','{$PostVolume[$i][$j]}')";
						$qDetail = $this->db->query($sqldetail);
					}
				}
			}
			
			if($qDetail){
				return true;
			}else{
				return false;
			}
		}
		
		// function saveMonotony($args,$user_id,$atlet){
			// $monotony_id = $this->uniqekey();
			// list($target,$lastweek,$StartDay,$desc,$phase,$PostSession,$PostScale,$PostVolume)=$args;
			
			// $dttm = $StartDay." ".date("H:i:s");
			
			// $sql = " INSERT INTO mdlmonotony (mdlmonotony_id,mdlmonotony_user,mdlmonotony_target,mdlmonotony_description"
				 // . " ,mdlmonotony_last,mdlmonotony_detail,mdlmonotony_date) values ('$monotony_id','$user_id','$target'"
				 // . " ,'$desc','$lastweek','$monotony_id','$dttm')";
			// $query = $this->db->query($sql);
			
			// $sql1 = " INSERT INTO mdlmonotony_distribute (distribute_id,distribute_monotony,distribute_athlete"
				 // . " ,distribute_creator,distribute_active)"
				 // . " values ('$monotony_id','$monotony_id','$atlet','$user_id','1')";
			// $query1 = $this->db->query($sql1);
			
			// for($i=0; $i<count($PostSession); $i++) {
					
				// $w 		= date('w', 		strtotime($StartDay. ' +'.($i).' day'));
				// $Date 	= date('Y-m-d', 	strtotime($StartDay. ' +'.($i).' day'));
					
				// for($j=0; $j<count($PostSession[$i]); $j++) {
					// $detail_id = $this->uniqekey();			
					// $sqldetail = " INSERT INTO mdlmonotony_detail (detail_id,detail_reference,detail_day,"
								// ." detail_date,detail_session,detail_intensity,detail_volume) "
								// ." values ('$detail_id','$monotony_id','$w','$Date','{$PostSession[$i][$j]}',"
								// ." '{$PostScale[$i][$j]}','{$PostVolume[$i][$j]}')";
					// $qDetail = $this->db->query($sqldetail);
				// }
			// }
			// return true;
		// }
		
		function uniqekey() {
			$micro = microtime();
			$date  = date("YmdHis");
			$micro = explode(" ", $micro);
			$ext   = substr($micro[0], 2, 4);
			return $date.$ext;
		}
		
		function getMonotony($atlet,$year,$month){
			if($month == "all"){
				$sql = " SELECT a.*, b.mdlmonotony_date,b.mdlmonotony_user,b.phase FROM mdlmonotony_distribute as a"
				 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
				 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
				 . " AND b.mdlmonotony_active = '0' ORDER BY b.mdlmonotony_date DESC";
			}else{
				$sql = " SELECT a.*, b.mdlmonotony_date,b.mdlmonotony_user,b.phase FROM mdlmonotony_distribute as a"
				 . " LEFT JOIN mdlmonotony as b on b.mdlmonotony_id = a.distribute_monotony"
				 . " WHERE a.distribute_athlete = '$atlet' AND YEAR(b.mdlmonotony_date) = '$year'"
				 . " AND MONTH(b.mdlmonotony_date) = '$month'"
				 . " AND b.mdlmonotony_active = '0' ORDER BY b.mdlmonotony_date DESC";
			}
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getGrafikDay($monotony_id){
			$sql = " SELECT detail_date, SUM(detail_intensity * detail_volume) as trainingLoad,"
					 . " SUM(detail_intensity) as detail_intensity, SUM(detail_actual) as detail_actual"
					 . " FROM `mdlmonotony_detail` WHERE detail_reference = '$monotony_id'"
				   . " GROUP BY detail_date";
// 			$sql = " SELECT detail_date, SUM(detail_intensity) as detail_intensity, "
// 				 . " SUM(detail_actual) as detail_actual, SUM(detail_volume) as detail_volume "
// 				 . " FROM `mdlmonotony_detail` WHERE detail_reference = '$monotony_id' GROUP BY detail_date";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getMonotonyID($monotony_id){
			$sql = " SELECT * FROM `mdlmonotony_detail` WHERE detail_reference = '$monotony_id';";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		
		function getMonotonyTarget($monotony_id){
			$sql = " SELECT mdlmonotony_target, mdlmonotony_description, mdlmonotony_last FROM `mdlmonotony"
				  ."`WHERE mdlmonotony_id = '$monotony_id';";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){
				$row = $query->row();
				$target = $row->mdlmonotony_target;
				$desc	= $row->mdlmonotony_description;
				$last	= $row->mdlmonotony_last;
				
				return array($target,$desc,$last);
			}else{
				return false;
			}
		}
		
		function get_autocomplete($name,$group_id)
		{
			$sql_atlet = " SELECT b.value_wellness,b.wellness_date,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
			   . " FROM master_role AS a"
			   . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
			   . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
			   . " LEFT JOIN master_role as d on d.role_id = b.role_id"
			   . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
			   . " WHERE b.name like '%$name%'"
			   . " AND a.group_id = '$group_id' AND b.role_type = 'ATL'";
			$query_atlet = $this->db->query($sql_atlet);
			
			if($query_atlet->num_rows()>0){
				return $query_atlet->result();
			}else{
				return false;
			}
		}
	
		function STDEV($sample) {
			if(is_array($sample)){
				$mean = array_sum($sample) / count($sample);
				foreach($sample as $key => $num) $devs[$key] = pow($num - $mean, 2);
				$stdev	= sqrt(array_sum($devs) / (count($devs) - 1));
				return round($stdev);
			}else{
				return NULL;
			}//END IF
		}

		function AVERAGE($sample) {
			if(is_array($sample)){
				$mean	= array_sum($sample) / count($sample);
				
				return round($mean);
			}else{
				return NULL;
			}//END IF
		}

		function SUM($sample) {
			if(is_array($sample)){
				return array_sum($sample);
			}else{
				return NULL;
			}//END IF
		}

		function ABS($number) {
			return abs($number);
		}
	}
?>