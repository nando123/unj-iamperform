<?php
	class M_member extends CI_Model {
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		function getGroup($username){
			$query = $this->db->query("SELECT a.*,b.* FROM v_users as a"
			 	 . " LEFT JOIN master_role as b on b.role_id = a.role_id"
				 . " WHERE a.username = '$username'");
			
			if($query->num_rows() > 0){
				foreach($query->result() as $row){
					$role_id = $row->role_id;
					$role_type = $row->role_type;
				}
			}
			
			$sql = "SELECT *, master_group_id as groupcode FROM v_master_group WHERE username = '$username' ORDER BY master_group_name ASC";
			
			// if($role_type == "SATLAK" OR $role_type == "PRIMA" OR $role_type == "RCV" OR $role_type == "MLP"){
				// $sql = " select *, master_group_id as groupcode from master_group "
					 // . " where master_group_category in ('GW200','GW300','GW400') "
					 // . " ORDER BY master_group_name";
			// }else{
				// $sql = " SELECT a.groupcode, b.master_group_name, b.master_group_logo FROM v_master_group_sc as a"
					 // . " LEFT JOIN master_group as b on b.master_group_id = a.groupcode"
					 // . " WHERE a.username = '$username'";
			// }
			
			// echo $sql;
			// return;
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}				
		}
		
		function getMember($group_id){				
			$sql_atlet = "SELECT *, nomor_event as master_atlet_nomor_event FROM v_member_atlet WHERE master_licence_group = '$group_id' ORDER BY name ASC";
			// $sql_atlet = " SELECT b.wellness_date,b.value_wellness,b.role_type as role_member,a.role_id, b.name, b.username, c.master_atlet_nomor_event, b.name, e.master_group_name, b.gambar"
					   // . " FROM master_role AS a"
					   // . " LEFT JOIN users AS b ON a.role_id IN (b.role_id)"
					   // . " LEFT JOIN master_information_personal as c on c.master_atlet_username = b.username"
					   // . " LEFT JOIN master_role as d on d.role_id = b.role_id"
					   // . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
					   // . " WHERE a.group_id = '$group_id' AND b.role_type IN ('ATL')"
					   // . " ORDER BY b.name asc";
			$query_atlet = $this->db->query($sql_atlet);
			if($query_atlet -> num_rows() > 0){
				$result = $query_atlet->result();
				return $result;
			}else{
				return false;
			}
		}
		
		function getKPFphysical($atlet){
			$sql = " select a.*, b.role_id, c.master_atlet_nomor_event, b.name, b.gambar, e.master_group_name"
				 . " FROM master_performance as a LEFT JOIN users as b on b.username = a.id_user_atlet"
				 . " LEFT JOIN master_information_personal as c on c.master_atlet_username = a.id_user_atlet"
				 . " LEFT JOIN master_role as d on d.role_id = b.role_id "
				 . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
				 . " where id_user_atlet='$atlet' AND a.jenis_performance = 'Physical' order by a.id_performance desc LIMIT 1";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function getKPFtechnical($atlet){
			$sql = " select a.*, b.role_id, c.master_atlet_nomor_event, b.name, b.gambar, e.master_group_name"
				 . " FROM master_performance as a LEFT JOIN users as b on b.username = a.id_user_atlet"
				 . " LEFT JOIN master_information_personal as c on c.master_atlet_username = a.id_user_atlet"
				 . " LEFT JOIN master_role as d on d.role_id = b.role_id "
				 . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
				 . " where id_user_atlet='$atlet' AND a.jenis_performance = 'Technical' order by a.id_performance desc LIMIT 1";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
		
		function getKPFpsichology($atlet){
			$sql = " select a.*, b.role_id, c.master_atlet_nomor_event, b.name, b.gambar, e.master_group_name"
				 . " FROM master_performance as a LEFT JOIN users as b on b.username = a.id_user_atlet"
				 . " LEFT JOIN master_information_personal as c on c.master_atlet_username = a.id_user_atlet"
				 . " LEFT JOIN master_role as d on d.role_id = b.role_id "
				 . " LEFT JOIN master_group as e on e.master_group_id = d.group_id"
				 . " where id_user_atlet='$atlet' AND a.jenis_performance = 'Psichology' order by a.id_performance desc LIMIT 1";
			$query = $this->db->query($sql);
			if($query -> num_rows() > 0){

				$result = $query->result();

				return $result;
				
			}else{
				return false;
			}
		}
	}
?>