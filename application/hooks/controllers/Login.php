<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_login');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function form(){		
		if($this->session->userdata("login")){
			redirect("home");
		}
		$this->load->view("login/form");
	}
	
	function validation(){
		$username = $_POST["username"];
		$password = $_POST["password"];
        $result = $this->m_login->login($username,$password);
		
		if($result == "wrong_password"){
			echo $result;
			return;
		}
		if($result == "not_allowed"){
			echo $result;
			return;
		}
		if($result == "error"){
			echo $result;
			return;
		}
		
		if($result){
			foreach($result as $row){
				$name = $row->name;
				$username = $row->username;
				$gambar = $row->gambar;
				$role_id = $row->role_id;
				$role_type = $row->role_type;
				$group_id = $row->group_id;
				
				$data = array(
					'name' 		=> $name,
					'username' 	=> $username,
					'gambar' 	=> $gambar,
					'role_id' 	=> $role_id,
					'role_type' => $role_type,
					'group_id' 	=> $group_id
				);
				
				$this->session->set_userdata('login',$data);
			}
			
			// print_r($data);
			// echo $group_id;
			// return;
			
			redirect("home");
		}
	}
	
	public function logout(){
        $this->session->unset_userdata('login');
        redirect('login/form','refresh');
    }
}