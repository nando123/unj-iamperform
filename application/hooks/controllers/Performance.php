<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Performance extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_performance');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		
		if($role_type == "CHC"){
			redirect("performance/set_atlet");
		}
		if($role_type == "ATL"){
			redirect("performance/data/".$username);
		}
		if($role_type == "KSC"){
			redirect("performance/setGroup");
		}
	}
	
	function set_performance(){
    	$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$atlet		= $this->session->userdata("atlet");
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["atlet"] = $atlet;
         
		$data["page"] = "performance/setPerformance";
		$this->load->view('layout',$data);
    }
	
	function formPerformance(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$atlet		= $this->session->userdata("atlet");
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["atlet"] = $atlet;
		
		$get = $this->uri->uri_to_assoc(1);
		$atlet = $get['atlet'];			
		$get = $this->uri->uri_to_assoc(1);
		$set = $get['data'];
		$data['set'] = $set;
		
		$data["page"] = "performance/formPerformance";
		$this->load->view('layout',$data);
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
				
		$this->load->model('m_home');
		$data['group'] = $this->m_home->getGroup($username);
		
		$data["page"] = "performance/setGroup";
		$this->load->view("layout",$data);
	}
	
	function set_atlet($group_id = NULL){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		// $group_id	= $session["group_id"];
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$data['atlet'] = $this->m_performance->getAtlet($group_id);
		
		$data["page"] = "performance/set_atlet";
		$this->load->view("layout",$data);
	}
	
	function data($atlet){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$this->session->set_userdata("atlet",$atlet);
		
		$data['kpf'] 	= $this->m_performance->getKPFAtlet($atlet);
		$data['page']	= 'performance/performance';
		$this->load->view('layout',$data);
	}
	
	function formPerformanceAdd($set,$id_performance){
		if($this->session->userdata('login'))
        {
			$current_dttm = date('Y-m-d H:i:s');
			$atlet 		= $this->session->userdata('atlet');
			$session 	= $this->session->userdata("login");
			$name		= $session["name"];
			$username	= $session["username"];
			$gambar		= $session["gambar"];
			$role_type	= $session["role_type"];
			
			$data["gambar"] 	= $gambar;
			$data["name"] 		= $name;
			$data["username"] 	= $username;
			$data["role_type"] 	= $role_type;
			
			$data['set'] 	= $set;
			$data['atlet'] 	= $atlet;
			$data["dttm"] 	= $current_dttm;
			$data['id_performance'] = $id_performance;
			$data["page"] 	= "performance/formPerformanceAdd";
			$this->load->view('layout',$data);
        }else{
            redirect('login','refresh');   
        }
	}
	
	function saveNewPerformance(){
		if($this->session->userdata('login'))
        {
			$atlet 		= $this->session->userdata('atlet');
			$session 	= $this->session->userdata("login");
			$name		= $session["name"];
			$username	= $session["username"];
			$gambar		= $session["gambar"];
			$role_type	= $session["role_type"];
		}else{
            redirect('login','refresh');   
		}
		$komponen 	= $_POST['input_name'];
		$value 		= $_POST['input_nilai'];
		$goal 		= $_POST['input_goal'];
		$current 	= $_POST['input_current'];
		$jenis 		= $_POST['jenis'];
		$id_performance = $_POST['id_performance'];
		$current_dttm = date('Y-m-d H:i:s');
		
		$data = array($id_performance,$username,$atlet,$current_dttm,$komponen,$value,$goal,$current,$jenis);
		$save = $this->m_performance->saveNewperformance($data);
		if($save){
			redirect('performance/data/'.$atlet.'','refresh');
		}else{
			echo "gagal";
		}
	}
	
	function viewTable(){
		$session = $this->session->userdata('login');
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		$atlet = $this->session->userdata("atlet");
		
		$id_performance = $_POST["id_performance"];
		$sql = "select a.* FROM master_performance as a where a.id_performance = '$id_performance'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$row = $query->row();
			$date = $row->created_dttm;
			$jenis = $row->jenis_performance;
		}
		if($role_type == "SCH"){
			$btnadd = "<a data-backdrop='static' data-toggle='modal' href='".base_url()."index.php/atlet/add_detail_performance/$jenis/".$id_performance."'><span class='btn btn-brand'>Tambah</span></a>";
		}else{
			$btnadd = "";
		}
		$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
			 . " benchmark as benchmark, goal as goal, current as current"
			 . " From master_performance_detail where id_performance = '$id_performance'";
		$query_phy = $this->db->query($sql_phy);
		if($query_phy -> num_rows() > 0){
			$result_phy = $query_phy->result();
			if($result_phy > 0){
				$data = "";
				foreach ($result_phy as $item) {
					$hasil = ($item->current/$item->benchmark)*100;
					$result = number_format($hasil, 2, '.', ' ');
					$d_id = $item->id;
					if($role_type == "CHC" AND date("Y-m-d", strtotime($date)) == date("Y-m-d")){
						$btnedit = "<a data-toggle='modal' href='#ui_modal_".$d_id."'>$item->komponen</a>";
					}else{
						$btnedit = "$item->komponen";
					}
					
					$data .="<tr><td>$btnedit</td>"
							."<td>$item->benchmark</td><td>$item->goal</td><td>$item->current</td></tr>";
							
					$modal ='<p><strong>Komponen</strong> <input name="komponen" class="form-control" value="'.$item->komponen.'"/></p>'
						   .'<p><strong>Benchmark</strong> <input type="number" step="any" name="benchmark" class="form-control" value="'.$item->benchmark.'"/></p>'
						   .'<p><strong>Goal</strong> <input type="number" step="any" name="goal" class="form-control" value="'.$item->goal.'"/></p>'
						   .'<p><strong>Achievment</strong> <input type="number" step="any" name="current" class="form-control" value="'.$item->current.'"/></p>';
						   
					$data .='<div aria-hidden="true" class="modal fade" id="ui_modal_'.$d_id.'" role="dialog" tabindex="-1">
									<div class="modal-dialog">
									<form method="post" action="'.base_url().'index.php/atlet/edit_performance">
										<div class="modal-content">
											<div class="modal-heading">
												<a class="modal-close" data-dismiss="modal">×</a>
											</div>
											<div class="modal-inner">
												<div id="itemlist">
													'.$modal.'
												</div>
												<input name="id_performance_detail" type="hidden" value="'.$d_id.'"/>
												<input name="atlet" type="hidden" value="'.$atlet.'"/>
											</div>
											<div class="modal-footer">
												<p class="text-right"><button class="btn btn-flat btn-brand waves-attach" type="submit">Update</button>
												<span class="btn btn-flat btn-red waves-attach" data-dismiss="modal">Cancel</span></p>
											</div>
										</div>
									</form>
									</div>
								</div>';
				}
				$data .="<tr><td colspan='4'>$btnadd</td></tr></tbody></table></div>"
					    ."<br>";
		
				$data .= '
				<div aria-hidden="true" class="modal modal-va-middle fade" id="delete_'.$id_performance.'" role="dialog" tabindex="-1">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-inner">
								<p class="h5 margin-top-sm text-black-hint">Delete ?</p>
							</div>
							<div class="modal-footer">
								<p class="text-right">
									<a class="btn btn-flat btn-brand waves-attach" href="'.base_url().'index.php/home/delete/'.$id_performance.'/'.$jenis.'"">CONFIRM</a>
									<a class="btn btn-flat btn-red waves-attach" data-dismiss="modal">CANCEL</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				';
			}
		}
		
		$table = '
				<div id="grafik_'.$id_performance.'">
					<table class="striped">
						<thead>
							<tr>
								<th class="indigo white-text" data-field="id">Komponen</th>
								<th class="indigo white-text" data-field="name">Messo</th>
								<th class="indigo white-text" data-field="price">Current</th>
								<th class="indigo white-text" data-field="price">Benchmark</th>
							</tr>
						</thead>
						<tbody>
						'.$data.'
						</tbody>
					</table>
				</div>
			';
		
		echo $table;
		// echo "test";
	}
	
	function viewGrafik(){
		$id_performance = $this->input->post("id_performance");
		
		$perf = $this->m_performance->getKPFAtletID($id_performance);
		
		if($perf){
			list($first,$second)=$perf;
			$result_current = "";
			$result_goal 	= "";
			$result_bench	= "";
			foreach ($first as $key) {
				$jenis = $key->jenis_performance;
				$atlet = $key->id_user_atlet;
				$catatan = $key->catatan;

				foreach ($second as $item) {
					$komponen = $item->komponen;
					$benchmark = $item->benchmark;
					$goal = $item->goal;
					$current = $item->current;
					$hasil_current = ($current/$benchmark)*100;
					$hasil_goal = ($goal/$benchmark)*100;
					$hasil_benc = ($benchmark/$benchmark)*100;
					$result_current = number_format($hasil_current, 2, '.', ' ');
					$result_goal = number_format($hasil_goal, 2, '.', ' ');
					$result_bench = number_format($hasil_benc, 2, '.', ' ');
					// $result_current .= number_format($hasil_current, 2, '.', ' ');
					// $result_goal .= number_format($hasil_goal, 2, '.', ' ');
					// $result_bench .= number_format($hasil_benc, 2, '.', ' ');
					
					// if(count($hasil_current)) {
						// $result_current .= ",";
					// }
					// if(count($hasil_goal)) {
						// $result_goal .= ",";
					// }
					// if(count($benchmark)) {
						// $result_bench .= ",";
					// }
		
					$arrkomponen[] = $komponen;
					$arrcurrent[] = (int)$result_current;
					$arrgoal[] = (int)$result_goal;
					$arrbench[] = (int)$result_bench;
				}
					
				// $arrcurrent = array(9,10,11);
				// $arrgoal = array(7,8,10);
				// $arrbench = array(7,8,10);
				
				$data = array('categories'=>$arrkomponen, 'current'=>$arrcurrent, 'goal'=>$arrgoal, 'benc'=>$arrbench);
				$json = json_encode($data);
				echo $json;
			}
		}
	}
	
	public function save_performance() {
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$atlet		= $this->session->userdata("atlet");	
		
		$nama_komponen 	= $_POST['input_name'];
		$nilai_komponen = $_POST['input_nilai'];
		$nilai_goal = $_POST['input_goal'];
		$nilai_current = $_POST['input_current'];
		$jenis = $_POST['jenis'];
		$catatan = $_POST['catatan'];
		
		$data = array(
			'username' => $username,
			'atlet' => $atlet,
			'dttm' => $current_dttm,
			'komponen' => $nama_komponen,
			'value' => $nilai_komponen,
			'goal' => $nilai_goal,
			'current' => $nilai_current,
			'jenis' => $jenis,
			'catatan' => $catatan
		);
		
		$save = $this->m_performance->save_performance($data);
		if($save){
			redirect('performance/data/'.$atlet.'','refresh');
		}else{
			redirect('performance/data/'.$atlet.'','refresh');
		}
    }
}