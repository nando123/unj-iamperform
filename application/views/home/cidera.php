<?php
	if($cidera){
		$list = "";
		foreach($cidera as $row){
			$atlet_id	= $row->username_atlet;
			$atlet_nm	= $row->name;
			$cidera		= $row->cidera;
			$group_nm	= $row->master_group_name;
			$wellness	= $row->value_wellness;
			$atlet_pic	= $row->gambar;
			
			if($wellness > 0){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}
			
			$list .= '
				<li class="collection-item avatar">
					<img src="'.$atlet_pic.'" alt="" class="circle">
					<span class="title">'.$atlet_nm.'</span>
					<p>'.$group_nm.'</p>
					<p>Cidera : '.$cidera.'</p>
					<a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 35px" class="mdi-action-favorite"></i></a>
				</li>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="col s12 m8 l9" id="divlist">
			<ul class="collection">
				<?php echo $list ?>
			</ul>
		</div><br><br><br><br>
	</div>
	<!--end container-->
</section>