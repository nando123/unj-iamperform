<!-- START CONTENT -->
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m4 12">
			<div id="profile-card" class="card">
				<div class="card-content">
					<div id="imgContainer">
						<form enctype="multipart/form-data" action="<?php echo base_url()?>index.php/account/uploadProfile" method="post" name="image_upload_form" id="image_upload_form">
						<div id="imgArea"><img src="<?php echo $gambar ?>">
							<div class="progressBar">
								<div class="bar"></div>
								<div class="percent">0%</div>
							</div>
							<div id="imgChange" style="background:#000"><span>Click Here To Change Photo</span>
								<input type="file" accept="image/*" name="image_upload_file" id="image_upload_file">
							</div>
						</div>
						</form>
					</div>
					<span class="card-title grey-text text-darken-4"><?php echo $name ?></span>
					<a class="btn-floating cyan activator btn-move-down waves-effect waves-light darken-2 right">
						<i class="mdi-editor-mode-edit"></i>
					</a>
					<p><i class="mdi-action-assignment-ind"></i> <?php echo $username ?></p>
					<p><i class="mdi-action-account-child"></i> <?php echo $group_name ?></p>
					<p><i class="mdi-communication-contacts"></i> <?php echo $no_event ?></p>
					<p><span class="btn-flat cyan white-text" onClick="changePassword()">Change Password</span></p>
					<p id="formpassword"></p>
				</div>
				<div class="card-reveal">
					<p><span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i></span></p>
					<div class="row margin">
						<div class="input-field col s12">
							<p for="username" >Full Name </p>
							<i class="mdi-action-account-circle prefix"></i>
							<input id="nameupdate" name="nameupdate" type="text" value="<?php echo $name ?>">
						</div>
					</div>
					<div class="row margin">
						<span class="btn-flat cyan white-text" onClick="updateName()">Save</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>

<script>
	function updateName(){
		var name = $("#nameupdate").val();
		// alert(name);
		// return;
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/account/updateName',
			data : {name:name},
			// dataType: "json",
			success: function(data){
				window.location.reload();
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function changePassword(){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/account/formpassword',
			// dataType: "json",
			success: function(data){
				$("#formpassword").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
</script>