<?php 
	$event = "";
	$alamat = "";
	$telp = "";
	$handphone = "";
	$email = "";
	$tempat_lahir = "";
	$tgl_lahir = "";
	$agama = "";
	$tinggi_badan = "";
	$golongan_darah = "";
	$pendidikan = "";
	$pekerjaan = "";
	$status = "";
	$jenis_kelamin = "";
	$optstatus = "";
	$optkelamin = "";
	$kaos = "";
	$jaket = "";
	$celana = "";
	$sepatu = "";
	$nomor_ktp = "";
	$npwp = "";
	if($personal){
		foreach($personal as $personal){
			$event = $personal->master_atlet_nomor_event;
			$alamat = $personal->master_atlet_address;
			$telp = $personal->master_atlet_telp;
			$handphone = $personal->master_atlet_handphone;
			$email = $personal->master_atlet_email;
			$tempat_lahir = $personal->master_atlet_tempat_lahir;
			$tgl_lahir = $personal->master_atlet_tanggal_lahir;
			$agama = $personal->master_atlet_agama;
			$tinggi_badan = $personal->master_atlet_tinggi_badan;
			$golongan_darah = $personal->master_atlet_golongan_darah;
			$pendidikan = $personal->master_atlet_pendidikan;
			$pekerjaan = $personal->master_atlet_pekerjaan;
			$status = $personal->master_atlet_status;
			$jenis_kelamin = $personal->master_atlet_jenis_kelamin;
			$kaos = $personal->kaos;
			$jaket = $personal->jaket;
			$celana = $personal->celana;
			$sepatu = $personal->sepatu;
			$nomor_ktp 	= $personal->nomor_ktp;
			$npwp 	= $personal->npwp;
		}
	}
		
	if($jenis_kelamin == "laki"){
		$optkelamin = 
			'<p>Jenis Kelamin</p>
			<select name="jenis_kelamin">
			  <option value="laki" selected>Laki Laki</option>
			  <option value="perempuan">Perempuan</option>
			</select>';
	}else if($jenis_kelamin == "perempuan"){
		$optkelamin = 
			'<p>Jenis Kelamin</p>
			<select name="jenis_kelamin">
			  <option value="laki">Laki Laki</option>
			  <option value="perempuan" selected>Perempuan</option>
			</select>';
	}else{
		$optkelamin = 
			'<p>Jenis Kelamin</p>
			<select name="jenis_kelamin">
			  <option>-- Pilih --</option>
			  <option value="laki">Laki Laki</option>
			  <option value="perempuan">Perempuan</option>
			</select>';
	}
	
	
	if($status == "menikah"){
		$optstatus = 
			'<p>Status</p>
			<select name="status">
			  <option value="menikah" selected >Menikah</option>
			  <option value="single">Belum Menikah</option>
			</select>';
	}else if($status == "single"){
		$optstatus = 
			'<p>Status</p>
			<select name="status">
			  <option value="menikah" >Menikah</option>
			  <option value="single" selected >Belum Menikah</option>
			</select>';
	}else{
		$optstatus = 
			'<p>Status</p>
			<select name="status">
			  <option>-- Pilih --</option>
			  <option value="menikah">Menikah</option>
			  <option value="single">Belum Menikah</option>
			</select>';
	}
?>
<?php 
	$orangtua = "";
	$pasangan = "";
	$anak	= "";
	$nomordll	= "";
	if($keluarga){
		foreach($keluarga as $keluarga){ 
			$orangtua = $keluarga->master_atlet_keluarga_orangtua;
			$pasangan = $keluarga->master_atlet_keluarga_pasangan;
			$anak	  = $keluarga->master_atlet_keluarga_anak;
			$nomordll = $keluarga->master_atlet_keluarga_nomor_lain;
		}
	}
?>
<?php 
	$cedera = "";
	$alergi = "";
	$lemak  = "";
	if($health){
		foreach($health as $health){ 
			$cedera = $health->cedera;
			$alergi = $health->alergi;
			$lemak  = $health->lemak;
		}
	}
?>
<?php 
	$nama_club = "";
	$alamat_club = "";
	$email_club  = "";
	$prestasi  = "";
	if($club){
		foreach($club as $club){ 
			$nama_club		= $club->club;
			$alamat_club 	= $club->alamat_club;
			$email_club 	= $club->email_club;
			$prestasi 		= $club->prestasi;
		}
	}
?>
<!-- START CONTENT -->
<section id="content">
	<!--start container-->
	<div class="container">
		<div id="preselecting-tab" class="section">
            <h4 class="header">Profile</h4>
			<div class="row">
			  <div class="col s12 m8 l9">
				<div class="row">
					<div class="col s12">
						<ul class="tabs tab-demo-active z-depth-1 cyan">
						  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#basic">Basic</a>
						  </li>
						  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#family">Family</a>
						  </li>
						  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#health">Health</a>
						  </li>
						  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#pb">PB</a>
						  </li>
						  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#apparel">Apparel</a>
						  </li>
						</ul>
					</div>
					<div class="col s12">
						<div id="basic" class="col s12  lighten-4">
							<div class="col s12 m8 l9">
								<div class="row">
									<form class="col s12" method="post" action="<?php echo base_url();?>index.php/profile/updatePersonal">
									  <div class="row">
										<div class="input-field col s12 m6">
										  <p for="icon_prefix3">Nama Lengkap</p>
										  <input disabled name="icon_prefix3" type="text" class="validate" value="<?php echo $name ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Cabang Olahraga</p>
										  <input disabled type="text" class="validate" value="<?php echo $group_name ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Nomor Event</p>
										  <input name="nomor_event" type="text" class="validate" value="<?php echo $event ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Alamat</p>
										  <input name="alamat" type="text" class="validate"  value="<?php echo $alamat ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">No KTP</p>
										  <input name="nomor_ktp" type="text" class="validate"  value="<?php echo $nomor_ktp ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">No NPWP</p>
										  <input name="npwp" type="text" class="validate"  value="<?php echo $npwp ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Telpon</p>
										  <input name="telp" type="tel" class="validate" value="<?php echo $telp ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Handphone</p>
										  <input name="handphone" type="tel" class="validate" value="<?php echo $handphone ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Email</p>
										  <input name="email" type="text" class="validate" value="<?php echo $email ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Tempat Lahir</p>
										  <input name="tempat_lahir" type="text" class="validate"  value="<?php echo $tempat_lahir ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Tanggal Lahir</p>
										  <input name="tgl_lahir" type="text" class="validate"  value="<?php echo $tgl_lahir ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Agama</p>
										  <input name="agama" type="text" class="validate"  value="<?php echo $agama ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Tinggi Badan</p>
										  <input name="tinggi_badan" type="number" class="validate"  value="<?php echo $tinggi_badan ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Golongan Darah</p>
										  <input name="golongan_darah" type="text" class="validate" value="<?php echo $golongan_darah ?>">
										</div>
										<div class="input-field col s12 m6">
										  <?php echo $optkelamin?>
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Pendidikan</p>
										  <input name="pendidikan" type="text" class="validate" value="<?php echo $pendidikan ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Pekerjaan</p>
										  <input name="pekerjaan" type="text" class="validate"  value="<?php echo $pekerjaan ?>">
										</div>
										<div class="input-field col s12 m6">
										  <?php echo $optstatus ?>
										</div>
									  </div>
									  <p><button class="btn cyan">Update</button></p>
									</form>
								</div>
							</div>
						</div>
						<div id="family" class="col s12  lighten-4">
							<div class="col s12 m8 l9">
								<div class="row">
									<form class="col s12" method="post" action="<?php echo base_url();?>index.php/profile/updateFamily">
									  <div class="row">
										<div class="input-field col s12 m6">
										  <p for="icon_prefix3">Nama Orang Tua</p>
										  <input name="orangtua" type="text" class="validate" value="<?php echo $orangtua ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Nama Suami/Istri</p>
										  <input name="pasangan" type="text" class="validate" value="<?php echo $pasangan ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Nama Anak</p>
										  <input name="anak" type="text" class="validate" value="<?php echo $anak ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Nama / Nomor kontak yang bisa dihubungi apabila terjadi sesuatu :</p>
										  <input name="nomor_lain" type="text" class="validate" value="<?php echo $nomordll ?>">
										</div>
									  </div>
									  <p><button class="btn cyan">Update</button></p>
									</form>
								</div>
							</div>
						</div>
						<div id="health" class="col s12  lighten-4">
							<div class="col s12 m8 l9">
								<div class="row">
									<form class="col s12" method="post" action="<?php echo base_url();?>index.php/profile/updateHealth">
									  <div class="row">
										<div class="input-field col s12 m6">
										  <p for="icon_prefix3">Cedera Yang Pernah Dialami</p>
										  <input name="cedera" type="text" class="validate" value="<?php echo $cedera ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Alergi</p>
										  <input name="alergi" type="text" class="validate" value="<?php echo $alergi ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Presentasi Lemak Tubuh</p>
										  <input name="lemak" type="text" class="validate" value="<?php echo $lemak ?>">
										</div>
									  </div>
									  <p><button class="btn cyan">Update</button></p>
									</form>
								</div>
							</div>
						</div>
						<div id="pb" class="col s12  lighten-4">
							<div class="col s12 m8 l9">
								<div class="row">
									<form class="col s12" method="post" action="<?php echo base_url();?>index.php/profile/updateClub">
									  <div class="row">
										<div class="input-field col s12 m6">
										  <p for="icon_prefix3">PB/PP</p>
										  <input name="club" type="text" class="validate" value="<?php echo $nama_club ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Alamat PB/PP</p>
										  <input name="alamat_club" type="text" class="validate" value="<?php echo $alamat_club ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Email PB/PP</p>
										  <input name="email_club" type="text" class="validate" value="<?php echo $email_club ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Prestasi Terakhir</p>
										  <input name="prestasi" type="text" class="validate" value="<?php echo $prestasi ?>">
										</div>
									  </div>
									  <p><button class="btn cyan">Update</button></p>
									</form>
								</div>
							</div>
						</div>
						<div id="apparel" class="col s12  lighten-4">
							<div class="col s12 m8 l9">
								<div class="row">
									<form class="col s12" method="post" action="<?php echo base_url();?>index.php/profile/updateApparel">
									  <div class="row">
										<div class="input-field col s12 m6">
										  <p for="icon_prefix3">Ukuran Kaos</p>
										  <input name="kaos" type="text" class="validate" value="<?php echo $kaos ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Ukuran Jaket</p>
										  <input name="jaket" type="text" class="validate" value="<?php echo $jaket ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Ukuran Celana</p>
										  <input name="celana" type="text" class="validate" value="<?php echo $celana ?>">
										</div>
										<div class="input-field col s12 m6">
										  <p for="icon_telephone">Ukuran Sepatu</p>
										  <input name="sepatu" type="text" class="validate" value="<?php echo $sepatu ?>">
										</div>
									  </div>
									  <p><button class="btn cyan">Update</button></p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>