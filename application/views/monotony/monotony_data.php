<?php 
	$data 		= "";
	
	$dayList = array(
		'Sun' => 'Minggu',
		'Mon' => 'Senin',
		'Tue' => 'Selasa',
		'Wed' => 'Rabu',
		'Thu' => 'Kamis',
		'Fri' => 'Jumat',
		'Sat' => 'Sabtu'
	);
	
	if($table){
		$tmpdata = 0;
		$count = count($table);
		$Details = $cell["value"];
		foreach($table as $num => $row){
			$mdldate[$num] 		= $row->detail_date;
			$sessType[$num] 	= $row->detail_session;
			$detIntensity[$num]	= $row->detail_intensity;
			$detVolume[$num]	= $row->detail_volume;
			$day[$num] 			= date("D",strtotime($mdldate[$num]));
			$hari				= $day[$num];
			$dayHari			= $dayList[$hari];
			$totalVolume[$num] = $detIntensity[$num]*$detVolume[$num];
			
			if( isset($Details[$mdldate[$num]]) ) {
				$TrainingLoad = '<td rowspan="'.$Details[$mdldate[$num]]['count'].'">
				  '.$Details[$mdldate[$num]]['trainingday'].'</td>';
				unset($Details[$mdldate[$num]]);
			}else {
				$TrainingLoad = "";
			}
			
			$RIGHT = ($num==0)?'[TrainingMonotoryLeft][TrainingMonotoryRight][TrainingStrainLeft][TrainingStrainRight]':'';
			$data .="<tr><td>$dayHari - ".($num+1)."</td><td>".date("d M Y", strtotime($mdldate[$num]))."</td>"
				   ."<td>$sessType[$num]</td><td class='text-center'>$detIntensity[$num]</td>"
				   ."<td class='text-center'>$detVolume[$num]</td>"
				   ."<td class='text-center'>$totalVolume[$num]</td>".$TrainingLoad."".$RIGHT;
		}
		$TrainingMonotory['left']  = '<td class="text-center" rowspan="'.$count.'">'.$variation.'</td>';
	    $TrainingMonotory['right'] = '<td class="text-center" rowspan="'.$count.'">'.$target.'</td>';
		$TrainingStrain['left']    = '<td class="text-center" rowspan="'.$count.'">'.$desc.'</td>';
	    $TrainingStrain['right']   = '<td class="text-center" rowspan="'.$count.'">'.$load.'</td>';
		$data = str_replace("[TrainingMonotoryLeft]",    $TrainingMonotory['left']  , $data);
		$data = str_replace("[TrainingMonotoryRight]",   $TrainingMonotory['right'] , $data);
		$data = str_replace("[TrainingStrainLeft]",      $TrainingStrain['left']    , $data);
		$data = str_replace("[TrainingStrainRight]",     $TrainingStrain['right']   , $data);
		
		$data .= '<tr><td colspan="3" rowspan="3"></td>'
				.'<td rowspan="3"></td>'
				.'<td class="text-center">Weakly TL</td>'
				.'<td class="text-center" style="font-weight: bold;">'.$total.'</td>'
				.'<td colspan="2" class="text-center">Last Week : '.number_format($last, 2, ".", "").'</td>
			 </tr>
			 <tr>
				<td class="text-center">Av daily load</td>
				<td class="text-center" style="font-weight: bold;">'.$average.'</td>
				<td colspan="2" rowspan="2" class="numeric text-center" style="font-weight: bold;"></td>
			 </tr>
			 <tr>
				<td class="numeric text-center">SD</td>
				<td class="numeric text-center" style="font-weight: bold;">'.$stdev.'</td>
			 </tr>
		';
	}
?> 
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Training Load</p>
						</div>
					</div>
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showGrafik('<?php echo $monotony_id?>')" class="btn-flat blue white-text">Grafik Sessi</a> &nbsp&nbsp
						<a href="#" onClick="showGrafikDay('<?php echo $monotony_id?>')" class="btn-flat blue white-text">Grafik Hari</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataMonotony">
                  <table class="responsive-table striped">
					<thead>
						<tr>
							<td class="indigo white-text">Hari</td>
							<td class="indigo white-text">TGL</td>
							<td class="indigo white-text">SESSION</td>
							<td class="indigo white-text">INTENSITAS</td>
							<td class="indigo white-text">VOLUME</td>
							<td class="indigo white-text" colspan="2">TRAINING LOAD</td>
							<td class="indigo white-text" colspan="2">TRAINING MONOTORY</td>
							<td class="indigo white-text" colspan="2">TRAINING STRAIN</td>
						</tr>
					</thead>
					<tbody>
						 <tr>
							<td class="text-center">Sesi latihan</td>
							<td class="text-center">Week starting</td>
							<td class="text-center">Session Type</td>
							<td class="text-center">Effort scale 1 to 10 - RPE</td>
							<td class="text-center">Duration of actual session workout in minutes</td>
							<td class="text-center" colspan="2">Intensity X volume</td>
							<td class="text-center" >Daily Variation</td>
							<td class="text-center" >Target 1.5</td>
							<td class="text-center" >Total Impact on Athlete</td>
							<td class="text-center" >Load X Monotony</td>
						 </tr>
						 <?php echo $data ?>
					</tbody>
                  </table>
                </div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>
<script>
	function showGrafik(monotony_id){
		
		$("#DataMonotony").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		var optionsSession = {
			chart: {
				renderTo: 'DataMonotony',
			},
			plotOptions: {
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
			},
			title: {
				text: 'Training Load Per Session Chart'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: [
				{
					min:0,
					max:10,
				},
				{
					min:0,
					max:1000,
				},
			],
			legend: {
				enabled: true,
			},
			series: [
				{
					yAxis:0,
					type: 'column',
					name: 'Goal',
					data: [{}],
					color:"#009688"
				},
				{
					yAxis:0,
					type: 'column',
					name: 'Actual',
					data: [{}],
					color:"#F44336"
				},
				{
					yAxis:1,
					type: 'spline',
					name: 'Training Load',
					data: [{}],
					color:"#76FF03"
				},
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/viewGrafik',
			data : {monotony_id:monotony_id},
			dataType: "json",
			success: function(data){
				// $("#DataMonotony").html(data);
				// alert(data);
				// return;
				optionsSession.xAxis.categories = data.categories;
				optionsSession.series[2].data = data.load;
				// options.series[1].data = data.score;
				optionsSession.series[0].data = data.rpe;
				optionsSession.series[1].data = data.actual;
				var chart = new Highcharts.Chart(optionsSession);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showGrafikDay(monotony_id){
		
		$("#DataMonotony").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		var options = {
			chart: {
				renderTo: 'DataMonotony',
			},
			plotOptions: {
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
			},
			title: {
				text: 'Training Load Per Day Chart'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: true,
				},
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: [
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				},
				{
					min: 0,
					max: 20,
					allowDecimals: false,
					title: {
						text: ''
					}
				}
			],
			legend: {
				enabled: true,
			},
			series: [
				{
					type: 'spline',
					name: 'Training Load',
					data: [{}],
				},
				{
					type: 'column',
					name: 'Goal',
					data: [{}],
					yAxis:1,
					color:"#009688",
				},
				{
					type: 'column',
					name: 'Actual',
					data: [{}],
					yAxis:1,
					color:"#F44336",
				},
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/viewGrafikDay',
			data : {monotony_id:monotony_id},
			dataType: "json",
			success: function(data){
				// $("#DataMonotony").html(data);
				// alert(data);
				// return;
				options.xAxis.categories = data.categories;
				options.series[0].data = data.load;
				options.series[1].data = data.rpe;
				options.series[2].data = data.actual;
				var chart = new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
</script>