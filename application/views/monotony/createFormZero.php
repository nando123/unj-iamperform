<?php
	if($atlet){
		$list = "";
		foreach($atlet as $row){
			$name 		= $row->name;
			$gambar		= $row->gambar;
			$group		= $row->master_group_name;
			$event		= $row->master_atlet_nomor_event;
			$atlet		= $row->username;
			$wellness	= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			
			$query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
									. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
			if($query->num_rows()>0){
				$row 	= $query->row();
				$cidera = $row->cidera;
				if($cidera == ""){
					$cidera = "-";
				}
			}else{
				$cidera	= "-";
			}
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}
			
			$list .= '
				<div class="col s12 m4 10">
				<p>
				  <input type="checkbox" class="filled-in" id="atlet_'.$atlet.'" name="atlet[]" value="'.$atlet.'"/>
				  <label for="atlet_'.$atlet.'">'.$name.'</label>
				  <a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 25px" class="mdi-action-favorite"></i></a>
				</p><hr>
				</div>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
	
	
	$now		= date("Y-m-d");
	$tmpNow		= explode('-', $now);
	$bln		= $tmpNow[1]; //mengambil array $tgl[1] yang isinya 03
	$thn		= $tmpNow[0]; //mengambil array $tgl[0] yang isinya 2015
	$ref_date	= strtotime( "$now" ); //strtotime ini mengubah varchar menjadi format time
	$nowWeek	= date( 'W', $ref_date );
	
	$slcWeek = '<select class="form-control" id="startDate">';
	if($selectWeek){
		foreach($selectWeek as $row){
			$weekID		= $row->uid;
			$week 		= $row->weekly;
			$start_date	= $row->start_date;
			$end_date	= $row->end_date;
			$year		= $row->year;
			$start		= date("d M Y", strtotime($start_date));
			$end		= date("d M Y", strtotime($end_date));
			
			if($nowWeek == $week){$slc="selected";}else{$slc="";}
			
			$slcWeek .= '<option '.$slc.' value="'.$weekID.'">'.$start.' S/d '.$end.'</option>';
		}
	}
	$slcWeek .= "</select>";
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="row">
			<div class="col s12 m12 20">
				<div class="card-panel" id="DataWellness">
					<p class="card-title">Pilih Tanggal Training Load</p>
					<?php echo $slcWeek ?>
					<p class="card-title">Pilih Atlit</p>
					<div class="row">
						<div class="col s12 m4 10">
							<p>
								<input type="checkbox" onClick="toggle(this)" id="all"/>
								<label for="all">Select All</label>
							</p><hr>
						</div>
						<?php echo $list ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>
<?php if($role_type == 'KSC' OR $role_type == "CHC"){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button class="btn indigo white-text" onClick="nextStep()">
	  Next
	</button>
</div>
<?php } ?>

<script>
	function nextStep(){
		var startDate	= $("#startDate").val();
		var atlet = new Array();
			$.each($("input[name='atlet[]']:checked"), function() {
			atlet.push($(this).val());
		});
		
		if(atlet == ""){
			Materialize.toast('Silahkan Pilih Atlet', 4000);
			return;
		}
		
		$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url()?>index.php/monotony/setAtletMonotony',
			data	: {atlet:atlet,startDate:startDate},
			success: function(data){
				if(data == 'sukses'){
					window.location.href='<?php echo base_url()?>index.php/monotony/createStepOne';
				}else{
					$("#DataWellness").html("Terjadi Kesalahan Silahkan Coba Lagi");
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				$("#DataWellness").html("Terjadi Kesalahan Silahkan Coba Lagi");
				// alert(xhr.responseText);
			}
		})
	}
	
	function toggle(pilih) {
		checkboxes = document.getElementsByName('atlet[]');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = pilih.checked;
		}
    }
</script>