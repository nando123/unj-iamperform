<?php 
	$ret ="";
	if($monotony){
		foreach ($monotony as $key) {
			$date 			= $key->mdlmonotony_date;
			$user_id		= $key->mdlmonotony_user;
			$monotony_id 	= $key->distribute_monotony;
			$phase 	= $key->phase;
			$vdt = date("Y-m-d",strtotime($date));
			$date = date("d M Y H:i:s",strtotime($date));
			
			$sql	= $this->db->query("SELECT name FROM users WHERE username = '$user_id'");
			if($sql->num_rows()>0){
				$row		= $sql->row();
				$created_nm	= $row->name;
			}else{
				$created_nm = "";
			}
			
			$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
			$query	= $this->db->query($sql);
			if($query->num_rows()>0){
				$row 	= $query->row();
				$startMonotony 	= $row->startMonotony;
				$endMonotony 	= $row->endMonotony;
				$start			= date("d M Y",strtotime($startMonotony));
				$end			= date("d M Y",strtotime($endMonotony));
			}else{
				$start	= "";
				$end	= "";
			}
			
			if($role_type == "KSC"){
				$btnDelete	= "<span class='mdi-action-delete red-text waves-effect waves-light' onClick='showDelete(\"$monotony_id\")'></span>";
			}else{
				$btnDelete = "";
			}
			
			$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' group by detail_reference");
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyPerDay = $row->monotonyPerDay;
			}else{
				$monotonyPerDay	= 0;
			}
			
			$ret .= "
				<tr id='data_$monotony_id'>
					<tr>
						<td class='indigo white-text'>Tanggal</td>
						<td class='indigo white-text'>Total</td>
						<td class='indigo white-text'></td>
					</tr>
					<tr>
						<td style='font-weight:bold'>
							<a href='".base_url()."index.php/monotony/dataMonotony/$monotony_id''>$start - $end</a>
						</td>
						<td style='font-weight:bold'>
							$monotonyPerDay
						</td>
						<td>$btnDelete</td>
					</tr>
					<tr>
						<td colspan='4'>Dibuat Pada $date</td>
					</tr>
					<tr>
						<td colspan='4'>
							$phase </br>
							Dibuat Oleh $created_nm
						</td>
					</tr>
				</tr>
			";
			
			// $ret .= "
				// <tr id='data_$monotony_id'>
					// <td style='font-weight:bold'>
						// <a class='btn-flat btn-brand' href='".base_url()."index.php/monotony/dataMonotony/$monotony_id''>
							// $date
						// </a>
					// </td>
					// <td><span class='mdi-action-delete red-text' onClick='showDelete(\"$monotony_id\")'></span></td>
				// </tr>
			// ";
		}
	}else{
		$ret .= "<tr><td>Data Kosong</td></tr>";
	}
	
	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $pic?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title white-text" style="text-shadow: 0px 0px 0px #ffffff;"><b><?php echo $atlet_name?></b></h5>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;">Training Load</p>
						</div>
					</div>
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showGrafikWeek()" class="btn-flat blue white-text"><span class="mdi-action-trending-up"></span> Grafik</a> &nbsp&nbsp
						<a href="#" onClick="showFilter()" class="btn-flat blue white-text"><i class="mdi-content-sort"></i> Filter</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="filter">
            <div class="col s12 m12 l12">
				<div class="card-panel">
                    <div class="row">
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Bulan</p>
							<select id="month">
								<option value='all'>Semua Bulan</option>
								<?php
									$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
									for($bulan=1; $bulan<=12; $bulan++){
										if($month == $bulan){
											$select = "selected";
										}else{
											$select = "";
										}
										if($bulan<=9) {
											echo "<option $select value='0$bulan'>$bln[$bulan]</option>"; 
										}else{ 
											echo "<option $select value='$bulan'>$bln[$bulan]</option>"; 
										}
									}
								?>
							</select>
						</div>
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Tahun</p>
							<select id="year">
								<?php
								$mulai= date('Y') - 50;
								for($i = $mulai;$i<$mulai + 100;$i++){
									$sel = $i == $year ? ' selected="selected"' : '';
									echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
								}
								?>
							</select>
						</div>
                    
						<div class="input-field col s12 m2">
							<button class="btn btn-flat blue white-text" onClick="showWellness()">Filter</button>
						</div>
                    </div>
				</div>
            </div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">
                  <table class="striped">
					<thead><tr><th style='font-size:12pt;text-align:center' colspan='4'>Weekly Training Load <?php echo $year ?></th></tr></thead>
					<?php echo $ret ?>
				  </table>
				</div>
			  </div>
			</div>
			<div id="modalData"></div>
		</div><br><br><br><br><br><br>
	</div>
</section>
<?php if($role_type == 'KSC' OR $role_type == 'CHC'){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/monotony/createStepZero/<?php echo $group_id?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>

<script>
	function showDelete(monotony_id){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/showDeleteConfirm',
			data : {monotony_id:monotony_id},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function closeModal(d_id){
		var modal = document.getElementById('myModal_'+d_id);
		modal.style.display = "none";
	}
	
	function deleteMonotony(monotony_id){
		var modal = document.getElementById('myModal_'+monotony_id);
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/deleteMonotony',
			data : {monotony_id:monotony_id},
			success: function(data){
				if(data == "sukses"){
					modal.style.display = "none";
					$("#data_"+monotony_id).remove();
					Materialize.toast('Berhasil Menghaspus Training Load', 10000);
					window.location.reload();
					return;
				}else{
					modal.style.display = "none";
					Materialize.toast('Gagal Menghaspus Training Load', 10000);
					return;
					// window.location.reload();
				};
				
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}

	function showFilter() {
        $("#filter").toggle(); 
    }
	
	function showWellness(){
		$("#DataWellness").html("Loading ....");
		var year 	= $("#year").val();
		var month 	= $("#month").val();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/showFilter',
			data : {year:year,month:month},
			success: function(data){
				$("#DataWellness").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showGrafikWeek(){
		var year 	= $("#year").val();
		var month 	= $("#month").val();
		
		$("#DataWellness").html("Loading ....");
		
		var options = {
			chart: {
				renderTo: 'DataWellness',
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				}
			},
			title: {
				text: 'Training Load Volume Chart Weekly'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -45,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: [
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				},
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				}
			],
			legend: {
				enabled: true,
			},
			series: [
				{
					type: 'spline',
					name: 'Training Load',
					data: [{}],
					color: "#d50000",
				},
				{
					type: 'column',
					name: 'Training Load',
					data: [{}],
					color:"#009688",
				},
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/monotony/viewGrafikWeek',
			data : {year:year,month:month},
			dataType: "json",
			success: function(data){
				// $("#DataWellness").html(data);
				// alert(data);
				// return;
				options.xAxis.categories = data.categories;
				options.series[0].data = data.volume;
				options.series[1].data = data.volume;
				var chart = new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
</script>