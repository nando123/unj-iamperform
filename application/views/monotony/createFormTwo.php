 <?php
	 $table = "";
	 $tmpDate = 1;
	 for($i=0; $i<count($data['date']); $i++) {
		$COUNT 	= $data['date'][$i]['COUNT'];
		$DAY   	= $data['date'][$i]['DAY'];
		$DATE 	= $data['date'][$i]['DATE'];
		$count = 1;
		for($j=0; $j<$COUNT; $j++) {
			if($tmpDate<>$DATE){
				$table .= '<tr><th class="indigo white-text">'.$DAY.', '.date("d M Y",strtotime($DATE)).'</th></tr>';
				$tmpDate = $DATE;
			}
			$table .= '<tr><td class="cyan white-text"><b>Sessi - '.$count.'</b></td></tr>';
			$table .= '<tr><td>'
					. '
					<div class="row">
						<div class="input-field col s12" style="margin-bottom:-50px">
							<input type="text" name="'.str_replace("%idx%", $i, $form['session']).'" required>
							<label for="first_name">Session Type</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<p for="first_name">Effort scale 1 to 10 - RPE</p>
							<select name="'.str_replace("%idx%", $i, $form['scale']).'" required>
								<option value="0"	>0 - Not Set</option>
								<option value="1"	>1 - rest/very very easy</option>
								<option value="2"	>2 - very easy</option>
								<option value="3"	>3 - easy</option>
								<option value="4"	>4 - medium</option>
								<option value="5"	>5 - med - hard</option>
								<option value="6"	>6 - hard</option>
								<option value="7"	>7 - hard - very hard</option>
								<option value="8"	>8 - very hard</option>
								<option value="9"	>9 - very very hard</option>
								<option value="10"	>10 - extremly hard</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input name="'.str_replace("%idx%", $i, $form['volume']).'" type="number" required>
							<label for="first_name">Duration Volume in minutes</label>
						</div>
					</div>
					'
					. '</td></tr>';
			$count++;
		}
	 }
?>
<div class="container">
	<div class="section">
		<div class="col s12 m12 l6">
			<div class="card-panel">
				<h4 class="header2">Form Training Load</h4>
				<div class="row">
					<!--<form class="col s12" method="post" action="index.php/monotony/saveMonotony">-->
					<form class="col s12" id="formMonotony">
						<div class="row">
							<div class="input-field col s12 m12">
								<table class="table bordered hoverable">
									<tbody>
										<tr>
											<td>
												<div class="row">
													<div class="input-field col s12">
														<input type="text" name="phase" required/>
														<label for="first_name">Phase</label>
													</div>
												</div>
											</td>
										</tr>
										<?php echo $table ?>
										<tr>
											<td>
												<div class="row">
													<div class="input-field col s12">
														<input type="text" name="goal" required value="1.5"/>
														<label for="first_name">Target</label>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
                        </div>
						<div class="row">
							<div class="input-field col s12">
								<button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
									<i class="mdi-content-send right"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#formMonotony").submit(function(e) {
		// alert("test");
		// return;

		
		var url = "<?php echo base_url()?>index.php/monotony/saveMonotony"; // the script where you handle the form input.
		// var formMonotony	= $("#formMonotony").serialize();
		// return

		$.ajax({
			type: "POST",
			url: url,
			data: $("#formMonotony").serialize(), // serializes the form's elements.
			success: function(data){
				// alert(data);
				// return;
				if(data == "sukses"){
					window.alert('Berhasil Menyimpan Training Load')
					window.location.href='setGroup';
					return;
				}else{
					Materialize.toast('Gagal Menyimpan Training Load', 4000);
					return;
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				Materialize.toast('Gagal Menyimpan Training Load', 4000);
				return;
			}
		 });

		e.preventDefault(); // avoid to execute the actual submit of the form.
		
	});
</script>