<form method="post" action="<?php echo base_url()?>index.php/monotony/saveMonotony">
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">
                  <table class="responsive-table striped">
					<thead>
						<tr>
							<td class="blue white-text">Hari</td>
							<td class="blue white-text">TGL</td>
							<td class="blue white-text">SESSION</td>
							<td class="blue white-text">INTENSITAS</td>
							<td class="blue white-text">VOLUME</td>
						</tr>
					</thead>
					<tbody>
						 <tr>
							<td class="text-center">Sesi latihan</td>
							<td class="text-center">Week starting</td>
							<td class="text-center">Session Type</td>
							<td class="text-center">Effort scale 1 to 10 - RPE</td>
							<td class="text-center">Duration of actual session workout in minutes</td>
						 </tr>
						<?php
							 $count = 1;
							 for($i=0; $i<count($data['date']); $i++) {
								$COUNT 	= $data['date'][$i]['COUNT'];
								$DAY   	= $data['date'][$i]['DAY'];
								$DATE 	= $data['date'][$i]['DATE'];
								for($j=0; $j<$COUNT; $j++) {
									echo '
									<tr>
										<td>'.$DAY.'-'.$count.'</td>
										<td>'.$DATE.'</td>
										<td>
											<input style="margin-top:-50px;margin-bottom:-30px" type="text" name="'.str_replace("%idx%", $i, $form['session']).'"/>
										</td>
										<td class="numeric text-center">
											<select style="margin-bottom:-30px" name="'.str_replace("%idx%", $i, $form['scale']).'">
												<option value="0"	>0 - Not Set</option>
												<option value="1"	>1 - rest/very very easy</option>
												<option value="2"	>2 - very easy</option>
												<option value="3"	>3 - easy</option>
												<option value="4"	>4 - medium</option>
												<option value="5"	>5 - med - hard</option>
												<option value="6"	>6 - hard</option>
												<option value="7"	>7 - hard - very hard</option>
												<option value="8"	>8 - very hard</option>
												<option value="9"	>9 - very very hard</option>
												<option value="10"	>10 - extremly hard</option>
											 </select>
										</td>
										<td class="numeric text-center">
											<input style="margin-top:-50px;width: 100px;" name="'.str_replace("%idx%", $i, $form['volume']).'" type="number"/>
										</td>
									</tr>
									';
									$count++;

								}//END FOR 1
							 }//END FOR 2
						?>
					</tbody>
                  </table>
                </div>
              </div>
            </div>
		</div>		
		<div class="row">
			<div class="col s12 m12 19">
                <div class="card-panel">
					<div class="row">
                        <div class="input-field col s6 m2">
                          <input id="name3" type="text" name="phase">
							<input  name="<?php echo $form['start']; ?>" 	type="hidden" value="<?php echo $data['start']; ?>"/>
                          <label for="first_name">Phase</label>
                        </div>
                        <div class="input-field col s6 m2">
                          <input id="name3" type="text" name="lastweek">
                          <label for="first_name">Last Week</label>
                        </div>
                        <div class=" col s6 m2">
                          <label>Target</label>
                          <input type="text" name="goal" required value="1.5">
                        </div>
                        <div class="input-field col  s6 m2">
                          <label>Total Impact On Athlete</label>
                          <input type="text" name="description" >
                        </div>
                        <div class="input-field col  s6 m2">
							<button class="btn-flat blue white-text">Simpan</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>
</form>