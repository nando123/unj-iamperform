<section id="content">
	<div class="container">
		<div class="row">
            <div class="col s12 m12 l12">
              <div class="card-panel">
                <div class="row">
                  <form class="col s12" action="<?php echo base_url()?>index.php/monotony/createMonotonyTwo" method="post">
                    <div class="row">                       
						<div class="input-field col s6">
							<p style="margin:0px">Tanggal</p>
							<input required type="date" class="datepicker" name="<?php echo $form['start'];?>" value="<?php echo date('Y-m-d')?>">
						</div>
						<div class="input-field col s6">
							<button class="btn cyan waves-effect waves-light right" type="submit" name="action">Lanjut
							<i class="mdi-content-send right"></i>
							</button>
                        </div>
                    </div>
                    <div class="row">
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 1</p>
							<input id="day" type="text" name="<?php echo $form['day1'];?>" value="2">
						</div>
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 2</p>
							<input id="day" type="text" name="<?php echo $form['day2'];?>" value="2">
						</div>
                    </div>
                    <div class="row">
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 3</p>
							<input id="day" type="text" name="<?php echo $form['day3'];?>" value="2">
						</div>
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 4</p>
							<input id="day" type="text" name="<?php echo $form['day4'];?>" value="2">
						</div>
                    </div>
                    <div class="row">
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 5</p>
							<input id="day" type="text" name="<?php echo $form['day5'];?>" value="2">
						</div>
						<div class="input-field col s6">
							<p style="margin:0px">Hari Ke 6</p>
							<input id="day" type="text" name="<?php echo $form['day6'];?>" value="2">
						</div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
	</div>
</section>