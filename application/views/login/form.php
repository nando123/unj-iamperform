<?php
	if(isset($_SESSION["errormsg"])) {
		$error = $_SESSION["errormsg"];
		session_unset($_SESSION["errormsg"]);
	} else {
		$error = "";
	}
?>
<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 2.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout03/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Aug 2015 16:06:28 GMT -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
 
 <title>UNJ - IAM PERFORM</title>

  <!-- Favicons-->
  <link rel="icon" href="<?php echo base_url()?>assets/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="images/favicon/favicon-32x32.png">
  <!-- For Windows Phone -->

	<link rel="manifest" href="<?php echo base_url()?>assets/manifest.json">
	<meta name="theme-color" content="#000">
	
  <!-- CORE CSS-->
  
  <link href="<?php echo base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="<?php echo base_url()?>assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS style Horizontal Nav (Layout 03)-->    
    <link href="<?php echo base_url()?>assets/css/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="<?php echo base_url()?>assets/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  
	<style type="text/css">
		.text-line span {padding:10px;background:#fff}
		.text-line.right {text-align:right}
		.text-line.center {text-align:center}
		.text-line {background-color:#DFDFDF;height:2px;vertical-align:middle;line-height:1px}
	</style>


  
</head>

<body class="black">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->


  <div id="login-page" class="row">
		<?php echo $error ?>
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form" id="form" method="post" action="<?php echo base_url()?>index.php/login/validation">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="<?php echo base_url()?>assets/images/logo_amas_web.png" alt="" class="responsive-img">
            <!--<p class="center login-form-text"></p>-->
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <p for="username" >Username</p>
            <i class="mdi-social-person-outline prefix"></i>
            <input id="username" name="username" type="text" required="" >
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <p for="password">Password</p>
            <i class="mdi-action-lock-outline prefix"></i>
            <input name="password" type="password" required="" >
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <button class="btn waves-effect waves-light red col s12">Login</button>
          </div>
        </div>
        <div class="row">
          <p style="text-align:center" class="text-line center"><span>OR</span></p>
          <div class="input-field col s12">
            <a href="<?php echo base_url()?>index.php/register/form" class="btn col s12 orange white-text">Register Now</a>
          </div>
          <!--<div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="page-forgot-password.html">Forgot password ?</a></p>
          </div>   -->       
        </div>

      </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/materialize.js"></script>
  <!--prism-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins.js"></script>

</body>


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout03/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Aug 2015 16:06:31 GMT -->
</html>
<script>
	function(){
		$("#username").focus();
	}
</script>
<script>
  /* jshint ignore:start */
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-53563471-1', 'auto');
  ga('send', 'pageview');
  /* jshint ignore:end */
</script>