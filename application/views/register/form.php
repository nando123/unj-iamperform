<?php
	$query	= $this->db->query("SELECT * FROM master_provinsi ORDER BY provinsi_id ASC");
	if($query->num_rows()>0){
		$opt = "";
		foreach($query->result() as $row){
			$provinsi_id	= $row->provinsi_id;
			$provinsi_nm	= $row->provinsi_nama;
			$opt	.= "<option value='$provinsi_id'>$provinsi_nm</option>"; 
		}
	}else{
		$opt = "<option>No Option Available</option>";
	}
?>
<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 2.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout03/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Aug 2015 16:06:28 GMT -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <title>UNJ - IAM PERFORM</title>

  <!-- Favicons-->
  <link rel="icon" href="<?php echo base_url()?>assets/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<meta name="theme-color" content="#000">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="<?php echo base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-material-datetimepicker.css" />
    <!-- Custome CSS-->    
    <link href="<?php echo base_url()?>assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS style Horizontal Nav (Layout 03)-->    
    <link href="<?php echo base_url()?>assets/css/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="<?php echo base_url()?>assets/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  
</head>

<body class="black">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->



  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
		<div class="login-form" id="form">
			<div class="row">
			  <div class="input-field col s12 center">
				<h4>Register</h4>
				<p>Integrated Athlete Monitoring</p>
			  </div>
			</div>				
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-social-person-outline prefix"></i>
				<input id="username" type="text">
				<label for="username" class="center-align">Username</label>
			  </div>
			</div>
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-action-account-circle prefix"></i>
				<input id="name" type="text">
				<label for="name" class="center-align">Name</label>
			  </div>
			</div>
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-action-event prefix"></i>
				<input type="date" name="birth" id="date">
			  </div>
			</div>
			<div class="row margin">
			  <div class="col s12">
				<p>Gender</p>
				<p>
				  <input id="gender1" name="gender" type="radio" value="male" />
				  <label for="gender1">Male</label>
				  &nbsp
				  <input id="gender2" name="gender" type="radio" value="female" />
				  <label for="gender2">Female</label>
				</p>
			  </div>
			</div>
			<div class="row margin">
			  <div class="col s12">
				<p>Provinsi</p>
				<p>
				 <select id="provinsi">
					<?php echo $opt ?>
				 </select>
				</p>
			  </div>
			</div>
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-communication-vpn-key prefix"></i>
				<input id="licence" type="text">
				<label for="licence">Licence Code</label>
			  </div>
			</div>
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-action-lock-outline prefix"></i>
				<input id="password" type="password">
				<label for="password">Password</label>
			  </div>
			</div>
			<div class="row margin">
			  <div class="input-field col s12">
				<i class="mdi-action-lock-outline prefix"></i>
				<input id="repassword" type="password">
				<label for="repassword">Re-Type Password</label>
			  </div>
			</div>
			<div id="notsame" class="center input-field col s12"></div>
			<div class="row">
			  <div class="input-field col s12">
				<span class="btn orange waves-effect waves-light col s12" onClick="register()">Register Now</span>
			  </div>
			  <div class="input-field col s12">
				<p class="margin center medium-small sign-up">Already have an account? <a href="<?php echo base_url()?>index.php/login/form">Login</a></p>
			  </div>
			</div>
		</div>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/materialize.js"></script>
  <!--prism-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-material-datetimepicker.js"></script>

</body>


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout03/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Aug 2015 16:06:31 GMT -->
</html>
<script>
	$('#date').bootstrapMaterialDatePicker
	({
		time: false,
		clearButton: true
	});
</script>
<script>
	$('document').ready(function(){alert('check');});
	function register(){
		var username 	= $("#username").val();
		var name 		= $("#name").val();
		var licence 	= $("#licence").val();
		var password 	= $("#password").val();
		var repassword	= $("#repassword").val();
		var provinsi	= $("#provinsi").val();
		var gender		= $('input[name=gender]:checked').val();
		var birth		= $("#date").val();
		
		// alert(provinsi+' '+gender+' '+birth);
		// return;
		
		if(username == ''){$("#username").focus();return;}
		if(name == ''){$("#name").focus();return;}
		if(birth == ''){$("#date").focus();return;}
		if(gender == ''){$("#gender1").focus();return;}
		if(provinsi == ''){$("#provinsi").focus();return;}
		if(licence == ''){$("#licence").focus();return;}
		if(password == ''){$("#password").focus();return;}
		if(password != repassword){$("#notsame").html('<span class="btn-flat red col s12 white-text">Password Not Match</span>');$("#repassword").focus();return;}
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/register/createUser',
			data : {username:username,name:name,licence:licence,password:password,provinsi:provinsi,gender:gender,birth:birth},
			// dataType: "json",
			success: function(data){
				if(data == "exist"){
					$("#notsame").html('<span class="btn-flat red col s12 white-text">Username Already Taken</span>');
					$("#username").focus();
					return;
				}
				if(data == "licence_not"){
					$("#notsame").html('<span class="btn-flat red col s12 white-text">Licence Code Expired</span>');
					$("#licence").focus();
					return;
				}
				if(data == "licence_exist"){
					$("#notsame").html('<span class="btn-flat red col s12 white-text">Licence Code Not Found</span>');
					$("#licence").focus();
					return;
				}
				if(data == "licence_suspen"){
					$("#notsame").html('<span class="btn-flat red col s12 white-text">Licence Code Was Suspend By System</span>');
					$("#licence").focus();
					return;
				}
				if(data == "failure"){
					$("#notsame").html('<span class="btn-flat red col s12 white-text">Failed. Call IT Support</span>');
					$("#licence").focus();
					return;
				}
				if(data == "sukses"){
					window.location.href="<?php echo base_url()?>index.php/login/form";
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
</script>
