<?php 	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
	$ret = '<table class="striped"><thead>
						<tr><th colspan="3" style="font-size:12pt;text-align:center">Weekly Recovery Management</th></tr>
						<tr><th style="font-size:10pt;text-align:center">Tanggal</th>
						<th style="font-size:10pt;text-align:center">Recovery Point</th><th style="font-size:10pt;text-align:center">Recovery Plan</th></tr>
					</thead><tbody>';
	$retTotal = "";
	$modal = "";
	if($recovery){
		$tmpdttm = 0;
		$ttlMonotony = 0;
		foreach ($recovery as $key) {
			$detail_id 	= $key->detail_id;
			$date 		= $key->detail_date;
			$intensiti 	= $key->detail_intensity;
			$volume		= $key->detail_volume;
			$ttl 		= $intensiti*$volume;
			$ttlMonotony = $ttlMonotony+$ttl;
			
			$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' AND detail_date = '$date' group by detail_date");
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyPerDay = $row->monotonyPerDay;
			}else{
				$monotonyPerDay	= 0;
			}
			
			$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerWeek FROM `mdlmonotony_detail` "
									 . " WHERE detail_reference = '$monotony_id' ");
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyPerWeek = $row->monotonyPerWeek;
			}else{
				$monotonyPerWeek	= 0;
			}
			
			if($group_cat == "GW400"){		
				if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
					$target = 25;
				}else if($monotonyPerWeek <= 5000 OR $monotonyPerWeek < 6500){
					$target = 35;
				}else{
					$target = 40;
				}			
			}else{		
				if($monotonyPerWeek <= 2000 OR $monotonyPerWeek < 3500){
					$target = 25;
				}else if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
					$target = 35;
				}else{
					$target = 40;
				}
			}
			
			$plan =	($target/$monotonyPerWeek)*$monotonyPerDay;
			$recoveryPlan = number_format($plan);
			
			$tmp_dttm = explode(" ", $date);
			$dtmm1 = $tmp_dttm[0];
			$dttm = date('d M Y',strtotime($dtmm1));
			$day = date('D', strtotime($dtmm1));
			$rid = date('dmY', strtotime($date));
			
			$sql	= " SELECT SUM(b.recovery_point) as recovery_point FROM `master_recovery` as a"
					. " LEFT JOIN master_recovery_point as b on b.point_id = a.point_id"
					. " WHERE a.atlet_id = '$atlet'"
					. " AND a.recovery_dttm = '$dtmm1'";
			$query	= $this->db->query($sql);
			if($query->num_rows()>0){
				$row	= $query->row();
				$recovery_point	= $row->recovery_point;
				
				if($recovery_point == ""){
					$recovery_point = 0;
				}				
			}else{
				$recovery_point = 0;
			}

			$arrRecovery[$dtmm1]	= $recovery_point;
			
			$dayList = array(
				'Sun' => 'Minggu',
				'Mon' => 'Senin',
				'Tue' => 'Selasa',
				'Wed' => 'Rabu',
				'Thu' => 'Kamis',
				'Fri' => 'Jumat',
				'Sat' => 'Sabtu'
			);

			$btnRecovery = '<span id="button_'.$dtmm1.'" class="waves-effect waves-light" onClick="viewPlan(\''.$dtmm1.'\',\''.$atlet.'\')">'
							 . ''.$recoveryPlan.' Pts</span>';
			
			if($tmpdttm<>$date){
				$ret .= "
					<tr>
						<td style='font-size:7pt;text-align:left;font-weight:bold' width='50%'><a class='btn-flat btn-brand' href='".base_url()."index.php/recovery/recoveryDetail/$date/$monotony_id'>$dayList[$day], $dttm</a></td>
						<td style='font-weight:bold;text-align:center'>$recovery_point Pts</td>
						<td style='font-weight:bold;text-align:center'>$btnRecovery</td>
					</tr>
				";
				$tmpdttm = $date;
			}
		}
		
		if($group_cat == "GW400"){		
			if($ttlMonotony <= 3500 OR $ttlMonotony < 5000){
				$CM = "#388E3C";
				$target = 25;
			}else if($ttlMonotony <= 5000 OR $ttlMonotony < 6500){
				$CM = "#FF9800";
				$target = 35;
			}else{
				$CM = "#D50000";
				$target = 40;
			}			
		}else{		
			if($ttlMonotony <= 2000 OR $ttlMonotony < 3500){
				$CM = "#388E3C";
				$target = 25;
			}else if($ttlMonotony <= 3500 OR $ttlMonotony < 5000){
				$CM = "#FF9800";
				$target = 35;
			}else{
				$CM = "#D50000";
				$target = 40;
			}
		}
			
		// $sql	= " SELECT SUM(b.recovery_point) as ttlRecover FROM `master_recovery` as a"
				// . " LEFT JOIN master_recovery_point as b on b.point_id = a.point_id"
				// . " WHERE a.monotony_id = '$monotony_id'";
		// $query	= $this->db->query($sql);
		// if($query->num_rows()>0){
			// $row	= $query->row();
			// $ttlRecover	= $row->ttlRecover;
			// if($ttlRecover == ""){
				// $ttlRecover = 0;
			// }	
		// }else{
			// $ttlRecover = 0;
		// }
		
		$ttlRecover	= array_sum($arrRecovery);
		$remains = $target - $ttlRecover;
		
		// print_r($arrRecovery);
		
		
		$retTotal .="<tr><td class='btn-flat btn-brand'><b>Total Recovery</b></td><td><b>$ttlRecover Pts</b></td></tr>";
		$retTotal .="<tr><td class='btn-flat btn-brand'><b>Target Recovery</b></td><td><b>$target Pts</b></td></tr>";
		$retTotal .="<tr><td class='btn-flat btn-brand'><b>Remains Recovery</b></td><td><b>$remains Pts</b></td></tr>";
		$retTotal .="<tr><td class='btn-flat btn-brand'><b>Total Weekly Training Load</b></td><td style='background: $CM'><b>$ttlMonotony</b></td></tr>";
		
	}else{
		$ret .= "<tr><td>Data Kosong</td></tr>";
		$retTotal .= "";
		$btn = "";
	}
	$ret .='</tbody></table>';
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">
								<?php echo $group_name ?> -
								<?php echo $no_event ?>
							</p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Recovery Management</p>
						</div>
					</div>
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showGrafik('<?php echo $monotony_id ?>')" class="btn-flat blue white-text"><i class="mdi-av-equalizer"></i> Grafik Recovery</a>
						<a href="#" onClick="window.location.reload()" class="btn-flat blue white-text">Data</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
				<div class="row">
					<div class="col s12 m12 16" id="DataWellness">
						<?php echo $ret ?>
					</div>
				</div>
				<br>
				<hr>
				<div class="row">
					<div class="col s12 m12 16" id="DataWellness">
						<table class="striped">
							<tbody>
								<?php echo $retTotal ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div id="modalPlan"></div>
		</div>
		<?php echo $modal?>
	</div>
	<!--end container-->
</section>

<?php if($role_type == "ATL" AND $atlet == $username){?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/recovery/createDay/">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>
<script>
	function viewPlan(dttm, atlet) {
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url();?>index.php/recovery/viewPlan',
			data: {
				dttm: dttm,
				atlet: atlet
			},
			// dataType: "json",
			success: function(data) {
				// alert(data);
				$("#modalPlan").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
	}

	function closeModal(dttm, recoveryData) {
		var modal = document.getElementById('myModal_' + dttm + recoveryData);
		modal.style.display = "none";
	}

	function showGrafik(monotony_id) {
		// alert(monotony_id);
		// return;
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");

		var options = {
			chart: {
				renderTo: 'DataWellness',
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				}
			},
			// colors: ["#c62828","#2e7d32","#2196f3"],
			title: {
				text: 'Recovery Chart'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: true,
				},
			},
			xAxis: {
				categories: [],
				crosshair: true
			},
			yAxis: [{
				allowDecimals: false,
				title: {
					text: ''
				}
			}, {
				min: 0,
				max: 20,
				allowDecimals: false,
				title: {
					text: ''
				}
			}, ],
			series: [{
				showInLegend: true,
				type: 'column',
				name: 'Recovery Point',
				data: [{}],
				color: "#3F51B5",
				yAxis: 1,
			}, {
				showInLegend: true,
				type: 'column',
				name: 'Recovery Plan Point',
				data: [{}],
				color: "#388E3C",
				yAxis: 1,
			}, {
				showInLegend: true,
				type: 'spline',
				name: 'Volume Training Load',
				data: [{}],
				color: "#ef5350",
			}, {
				showInLegend: true,
				type: 'spline',
				name: 'Soreness',
				data: [{}],
				color: "#1b5e20",
				yAxis: 1,
			}, {
				showInLegend: true,
				type: 'spline',
				name: 'Fatigue',
				data: [{}],
				color: "#FFD600",
				yAxis: 1,
			}, ],
		};

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url();?>index.php/recovery/viewGrafik',
			data: {
				monotony_id: monotony_id
			},
			dataType: "json",
			success: function(data) {
				// $("#DataWellness").html(data);
				// return
				options.xAxis.categories = data.categories;
				options.series[2].data = data.TrainingLoad;
				options.series[0].data = data.point;
				options.series[1].data = data.plan;
				options.series[3].data = data.soreness;
				options.series[4].data = data.fatigue;

				var chart = new Highcharts.Chart(options);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
	}
</script>