<?php
	$sql 	= "SELECT a.*, b.recovery_type, b.recovery_image as image FROM master_recovery_point as a"
			. " LEFT JOIN master_recovery_type as b on b.type_id = a.type_id";
	$query	= $this->db->query($sql);
	$input = "";
	if($query->num_rows()>0){
		$result = $query->result();
		$tmp = 0;
		$rowspan = 0;
		foreach($result as $key){
			$point_id		= $key->point_id;
			$recovery_nm	= $key->recovery_name;
			$point			= $key->recovery_point;
			$image			= $key->image;
			$type_id		= $key->type_id;
			$type_nm		= $key->recovery_type;
			
			$img = '';
			if($tmp<>$type_id){
				$input .= "<thead><tr><th colspan='3'><img width='100%' src='".base_url()."assets/images/".$image."'/></th></tr><tr><th colspan='3'><p class='text-black'>$type_nm</p></th></tr></thead>";
				// $input .= '
					// <ul id="task-card" class="collection with-header">
						// <li class="collection-item dismissable"><img width="100%" src="'.base_url().'assets/images/'.$image.'"/></li>
						// <li class="collection-item dismissable"><p>'.$type_nm.'</p></li>
					// </ul>
				// ';
				$tmp = $type_id;
			}
			$input.="<tbody><tr><td><input type='checkbox' id='task3_$point_id' name='point[]' value='$point_id'/>"
				  . "<label for='task3_$point_id'> $recovery_nm</label> <a class='secondary-content'>"
				  . "<span class='ultra-small'>$point Pts</span></a></td></tr></tbody>";
		}		
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div id="card-widgets" class="seaction">
			<div class="row">
                <div class="col s12 m4 l4">
					<p class="task-card-date">Post-Game Recovery Point Per Tanggal <b><?php echo date('d M Y')?></b></p>
					<div class="table">
						<table class="table">
							<?php echo $input?>
							<tr><td><button onClick="saveRecovery()" class="btn cyan">Simpan</button></td></tr>
						</table>
					</div>					
                </div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>

<script>
	function saveRecovery(date,monotony_id){
		var data = $('input:checkbox:checked').map(function() { return this.value; }).get();
				
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>index.php/recovery/saveRecoveryDay",
			data: {point:data},
			success: function(data) {
				// alert(data);
				Materialize.toast('Berhasil Menyimpan Recovery :)', 4000);
				window.history.back(-2);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
</script>