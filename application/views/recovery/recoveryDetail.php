<?php 	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
	$ret ="";
	if($recoveryDetail){
		$no = 1;
		foreach ($recoveryDetail as $key) {
			$recovery_nm	= $key->recovery_name;
			$recovery_point	= $key->recovery_point;
			$ret .="<tr><td>$no</td><td>$recovery_nm</td><td>$recovery_point Pts</td></tr>";
			
			$no++;
		}
	}else{
		$ret .= "<tr><td colspan='3'>Data Kosong</td></tr>";
		$btn = "";
	}
	
	$dttm = date('d M Y',strtotime($date));
	$day = date('D', strtotime($date));
	$month = date('m', strtotime($date));
	
	$dayList = array(
		'Sun' => 'Minggu',
		'Mon' => 'Senin',
		'Tue' => 'Selasa',
		'Wed' => 'Rabu',
		'Thu' => 'Kamis',
		'Fri' => 'Jumat',
		'Sat' => 'Sabtu'
	);
	
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Recovery Management</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">
                  <table class="table striped">
					<thead>
						<tr>
							<th colspan="3" style='font-size:12pt;text-align:center;border-bottom:1px solid #ddd'>Recovery <?php echo $dayList[$day]?>, <?php echo $dttm ?></th>
						</tr>
						<tr>
							<th>No</th><th>Recovery Activity</th><th>Point</th>
						</tr>
					</thead>                   
                    <tbody>
					<?php echo $ret ?>
					</tbody>
                  </table>
                </div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>
<br><br><br><br><br><br>
<?php if($role_type == "ATL" AND $atlet == $username){?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/recovery/createRecovery/<?php echo $date ?>/<?php echo $monotony_id ?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>