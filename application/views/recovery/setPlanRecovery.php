<?php
	if($monotonyList){
		$optMonotony = "";
		$tmpStart	= 1;
		// foreach($monotonyList as $k => $row){
			// $vdt	= $row->start;
			// $vdt2	= $row->end;
			
			foreach($monotonyList as $row){
				$atletID = $row->username;
				$sql	= " SELECT distribute_monotony FROM mdlmonotony_distribute"
						. " WHERE distribute_athlete = '$atletID'"
						. " ORDER BY distribute_monotony ASC";
				$query	= $this->db->query($sql);
				
				if($query->num_rows()>0){
					foreach($query->result() as $row){
						$monotonyID = $row->distribute_monotony;
						
						$sql = "SELECT MIN(a.detail_date) start, MAX(a.detail_date) as end, a.detail_reference 
							FROM mdlmonotony_detail as a 
							WHERE a.detail_reference = '$monotonyID'
							GROUP BY a.detail_reference
							ORDER BY a.detail_reference DESC";
						$query	= $this->db->query($sql);
						foreach($query->result() as $row){
							$vdt = $row->start;
							$vdt2 = $row->end;
							
							if($tmpStart<>$vdt){
								$optMonotony .=  "<option value='".$vdt."_".$vdt2."'>$vdt - $vdt2</option>";
								$tmpStart = $vdt;
							}
						}
					}
				}
			}
			
			// $optMonotony .=  "<option value='$vdt'>$vdt - $vdt2</option>";
		// }
	}
	
	if($atlet){
		$list = "";
		foreach($atlet as $row){
			$name 		= $row->name;
			$gambar		= $row->gambar;
			$group		= $row->master_group_name;
			$event		= $row->master_atlet_nomor_event;
			$atlet		= $row->username;
			$wellness	= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			
			$query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
									. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
			if($query->num_rows()>0){
				$row 	= $query->row();
				$cidera = $row->cidera;
				if($cidera == ""){
					$cidera = "-";
				}
			}else{
				$cidera	= "-";
			}
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}
			
			$list .= '
				<div class="col s12 m4 10">
				<p>
				  <input type="checkbox" class="filled-in" id="atlet_'.$atlet.'" name="atlet[]" value="'.$atlet.'"/>
				  <label for="atlet_'.$atlet.'">'.$name.'</label>
				  <a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 25px" class="mdi-action-favorite"></i></a>
				</p><hr>
				</div>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
?>

<div class="container">
	<div class="row">
		<div class="col s12 m12 20">
			<div class="card-panel" id="DataWellness">
				<div class="row">
					<div class="col s12 m6 10">
						<p for="start">Pilih Training Load</p>
						<select id="loadTraining" onChange="getMonotonyAtlet()">
							<option value="">-- Pilih Training Load --</option>
							<?php echo $optMonotony ?>
						</select>
					</div>
				</div>
				<label class="card-title">Pilih Atlit</label>
				<div class="row" id="atletOpt">
					<div class="col s12 m4 10">
						<p>
							<input type="checkbox" onClick="toggle(this)" id="all"/>
							<label for="all" class="black-text"><b>Select All</b></label>
						</p><hr>
					</div>
					<?php echo $list ?>
				</div>
				<br><br><br><br><br>
			</div>
		</div>
	</div>
</div>
<?php if($role_type == 'RCV'){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button class="btn indigo white-text" onClick="nextStep()">
	  Next
	</button>
</div>
<?php } ?>
<script>
	function toggle(pilih) {
		checkboxes = document.getElementsByName('atlet[]');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = pilih.checked;
		}
	}
	
	function getMonotonyAtlet(){
		$("#atletOpt").html("<div class='progress'><div class='indeterminate'></div></div>");
		var monotony_id = $( "#loadTraining option:selected" ).val();
		$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url()?>index.php/recovery/getRecoveryAtlet',
			data	: {monotony_id:monotony_id},
			success: function(data){
				// alert(data);
				// return;
				$("#atletOpt").html(data);
				return;
			},error: function(xhr, ajaxOptions, thrownError){
				Materialize.toast('Terjadi Kesalahan, Hubungi Petugas', 4000);
				return;
			}
		})
	}
	
	function nextStep(){
		var trainingLoad	= $( "#loadTraining option:selected" ).val();
		
		if(trainingLoad == ""){
			Materialize.toast('Anda Belum Memilih Training Load', 4000);
			return;
		}
		var atlet 	= new Array();
			$.each($("input[name='atlet[]']:checked"), function() {
			atlet.push($(this).val());
		});
		
		if(atlet == ""){
			Materialize.toast('Anda Belum Memilih Atlet', 4000);
			return;
		}
		
		$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url()?>index.php/recovery/setRecoveryPlan',
			data	: {atlet:atlet,trainingLoad:trainingLoad},
			success: function(data){
				// alert(data);
				// return;
				if(data == 'sukses'){
					window.location.href='<?php echo base_url()?>index.php/recovery/formRecoveryPlan';
				}else{
					Materialize.toast('Terjadi Kesalahan, Hubungi Petugas', 4000);
					return;
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				Materialize.toast('Terjadi Kesalahan, Hubungi Petugas', 4000);
				return;
			}
		})
	}
</script>