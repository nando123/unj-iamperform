<?php 
		$sql = " SELECT a.distribute_monotony,b.detail_date,b.detail_reference, "
			 . " SUM(b.detail_volume*b.detail_intensity) as monotonyPerDay "
			 . " FROM mdlmonotony_distribute as a"
			 . " LEFT JOIN mdlmonotony_detail as b on b.detail_reference = a.distribute_monotony"
			 . " WHERE a.distribute_athlete = '$username'"
			 . " AND b.detail_date >= '$vdt'"
			 . " AND b.detail_date <= '$vdt2'"
			 . " GROUP BY b.detail_reference"
			 . " ORDER BY b.detail_date DESC, monotonyPerDay DESC LIMIT 1";
		$query	= $this->db->query($sql);
		if($query->num_rows()>0){
			$row	= $query->row();
			$monotony_id = $row->distribute_monotony;
		}else{
			$monotony_id 	= "";
		}

	$daySet = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
	$form = "";
	for ($n=0; $n<$jmlHari; $n++) {
		$date = mktime(0,0,0,date("m",strtotime($vdt)),date("d",strtotime($vdt))+$n,date("Y",strtotime($vdt)));
		$date = date("Y-m-d", $date);
		
		$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
								 . " WHERE detail_reference = '$monotony_id' AND detail_date = '$date' group by detail_date");
		if($query->num_rows()>0){
			$row	= $query->row();
			$monotonyPerDay = $row->monotonyPerDay;
		}else{
			$monotonyPerDay	= 0;
		}
		
		$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerWeek FROM `mdlmonotony_detail` "
								 . " WHERE detail_reference = '$monotony_id' ");
		if($query->num_rows()>0){
			$row	= $query->row();
			$monotonyPerWeek = $row->monotonyPerWeek;
		}else{
			$monotonyPerWeek	= 0;
		}
		
		$query	= $this->db->query("SELECT master_group_category FROM `master_group` WHERE master_group_id = '$group_id'");
		if($query->num_rows()>0){
			$row = $query->row();
			$group_cat = $row->master_group_category;
		}
		
		if($group_cat == "GW400"){		
			if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
				$target = 25;
			}else if($monotonyPerWeek <= 5000 OR $monotonyPerWeek < 6500){
				$target = 35;
			}else{
				$target = 40;
			}			
		}else{		
			if($monotonyPerWeek <= 2000 OR $monotonyPerWeek < 3500){
				$target = 25;
			}else if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
				$target = 35;
			}else{
				$target = 40;
			}
		}
		
		$plan =	($target/$monotonyPerWeek)*$monotonyPerDay;
		$recoveryPlan = number_format($plan);
		
		
		$day  		= date("w",strtotime($date));
		$hari 		= $daySet[$day];
		$tanggal  	= date("d M Y",strtotime($date));
		$form	.="<tr><td>$hari, $tanggal</td><td>$recoveryPlan Point</td>"
				. "<td id='btn_$date' ><span class='waves-effect waves-light btn-floating indigo' onClick='showFormPlan(\"$date\")'><i class='mdi-content-add'></i></span></td></tr>";
	}
?>
<div class="container">
	<div class="row">
		<div class="col s12 m6 20">
			<div class="card-panel" id="DataWellness">
				<table class="striped">
					<thead>
						<tr><th colspan="3" style="font-size:12pt;text-align:center">Weekly Recovery Management</th></tr>
						<tr>
							<th style="font-size:10pt;text-align:left">Tanggal</th>
							<th style="font-size:10pt;text-align:left">Plan Point</th>
							<th style="font-size:10pt;text-align:center"></th>
						</tr>
					</thead>
					<tbody>
						<? echo $form?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="modalData"></div>
<script>
	function showFormPlan(date){		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/recovery/showFormPlan',
			data : {date:date},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	
	function closeModal(dttm){
		var modal = document.getElementById('myModal_'+dttm);
		modal.style.display = "none";
	}
	
	function savePlan(dttm){
		var data = $('input:checkbox:checked').map(function() { return this.value; }).get();
		var modal = document.getElementById('myModal_'+dttm);
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/recovery/savePlan',
			data : {dttm:dttm,data:data},
			success: function(data){
				// alert(data);
				if(data == "sukses"){
					$("#btn_"+dttm).html("<span class='waves-effect waves-light btn-floating green white-text' onClick='showFormPlan(\""+dttm+"\")'><i class='mdi-action-done'></i></span>");
					Materialize.toast('Berhasil Menyimpan Recovery Plan :)', 4000);
					modal.style.display = "none";
				}else{
					Materialize.toast('Gagal Menyimpan Recovery Plan :(', 4000);
				};
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
</script>