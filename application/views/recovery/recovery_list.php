<?php 
	$ret = '<tbody>';
	if($listRecovery){
		foreach ($listRecovery as $key) {
			$date 			= $key->mdlmonotony_date;
			$monotony_id 	= $key->distribute_monotony;
			$vdt = date("Y-m-d",strtotime($date));
			$date = date("d M Y H:i:s",strtotime($date));
			$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
			$query	= $this->db->query($sql);
			if($query->num_rows()>0){
				$row 	= $query->row();
				$startMonotony 	= $row->startMonotony;
				$endMonotony 	= $row->endMonotony;
				$start			= date("d M Y",strtotime($startMonotony));
				$end			= date("d M Y",strtotime($endMonotony));
			}else{
				$start	= "";
				$end	= "";
			}
			
			$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' group by detail_reference");
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyPerDay = $row->monotonyPerDay;
			}else{
				$monotonyPerDay	= 0;
			}
			
			$ret .= "
				<tr>
					<td style='font-weight:bold'>
						<a class='btn-flat btn-brand' href='".base_url()."index.php/recovery/recoveryData/$monotony_id''>".$start." - ".$end."</a>
					</td>
					<td style='font-weight:bold'>
						$monotonyPerDay
					</td>
				</tr>
			";
		}
	}else{
		$ret .= "<tr><td>Data Kosong</td></tr>";
	}
	$ret .= '</tbody></table>';
	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title white-text" style="text-shadow: 0px 0px 0px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;">Recovery Management</p>
						</div>
					</div>
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showFilter()" class="btn-flat blue white-text"><i class="mdi-content-sort"></i> Filter</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="filter">
            <div class="col s12 m12 l12">
				<div class="card-panel">
                    <div class="row">
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Tahun</p>
							<select id="year">
								<?php
								$mulai= date('Y') - 50;
								for($i = $mulai;$i<$mulai + 100;$i++){
									$sel = $i == $year ? ' selected="selected"' : '';
									echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
								}
								?>
							</select>
						</div>
            <div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Bulan</p>
							<select id="month">
								<option value="all">-- All --</option>
								<?php
									$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
									for($bulan=1; $bulan<=12; $bulan++){
										if($month == $bulan){
											$select = "selected";
										}else{
											$select = "";
										}
										if($bulan<=9) {
											echo "<option $select value='0$bulan'>$bln[$bulan]</option>"; 
										}else{ 
											echo "<option $select value='$bulan'>$bln[$bulan]</option>"; 
										}
									}
								?>
							</select>
						</div>      
						<div class="input-field col s12 m2">
							<button class="btn btn-flat blue white-text" onClick="showWellness()">Filter</button>
						</div>
                    </div>
				</div>
            </div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">
                  <table class="striped">
					<thead>
						<tr><th style="text-align:center" >Tanggal Training Load</th>
							<th>Total</th></tr>
					</thead>
					<?php echo $ret ?>
				  </table>
				</div>
			  </div>
			</div>
		</div>
	</div>
</section>
<br>
<br>
<br>
<br>
<br>
<br>
<?php if($role_type == "ATL" AND $atlet == $username){?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/recovery/createDay/">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>
<script>
	function showFilter() {
        $("#filter").toggle(); 
    }
	
	function showWellness(){
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		var year 	= $("#year").val();
		var month 	= $("#month").val();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/recovery/showFilter',
			data : {year:year,month:month},
			success: function(data){
				$("#DataWellness").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
</script>