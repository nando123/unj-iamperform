<section id="content">
	<div class="container">
		<div id="work-collections" class="seaction">
			<div class="row">
				<div class="col s12 m12 26">
					<ul id="projects-collection" class="collection">
						<?php if($role_type=="KSC"){?>
						<a href="<?php echo base_url();?>index.php/performance/formPerformance/data/physic/atlet/<?php echo $atlet;?>">
						<li class="collection-item avatar">
						  <i class="mdi-action-accessibility circle light-blue"></i>
						  <span class="collection-header">Physical</span>
						  <p>For Physical Profiling</p>
						  <a></a>
						</li>
						</a>
						<?php }?>
						<?php if($role_type=="CHC"){?>
						<a href="<?php echo base_url();?>index.php/performance/formPerformance/data/technical/atlet/<?php echo $atlet;?>">
						<li class="collection-item avatar">
						  <i class="mdi-maps-directions-walk circle light-blue"></i>
						  <span class="collection-header">Technical</span>
						  <p>For Technical Profiling</p>
						  <a></a>
						</li>
						</a>
						<?php } ?>
						<?php if($role_type=="PSY"){?>
						<a href="<?php echo base_url();?>index.php/performance/formPerformance/data/mental/atlet/<?php echo $atlet;?>">
						<li class="collection-item avatar">
						  <i class="mdi-social-group circle light-blue"></i>
						  <span class="collection-header">Psicology</span>
						  <p>For Psicology Profiling</p>
						  <a></a>
						</li>
						</a>
						<?php } ?>
					</ul>
				</div>					
			</div>
		</div>
	</div>
	<!--end container-->
</section>