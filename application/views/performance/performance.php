<?php
	if($kpf){
		$data = "";
		foreach ($kpf as $key) {
			$id_performance = $key->id_performance;
			$no_event = $key->master_atlet_nomor_event;
			$group = $key->master_group_name;
			$jenis = $key->jenis_performance;
			$messo = $key->messo;
			$date = $key->created_dttm;
			$user_id = $key->created_user_id;
			$tmpdate = explode(" ", $date);
			$dttm = $tmpdate[0];
			$catatan = $key->catatan;
			$nama = $key->name;
			$gambar = $key->gambar;
			if($gambar){
				$img = "<img alt='$nama' src='$gambar'>";
			}else{
				$img = "";										
			}
			
			if($role_type == "KSC" AND $jenis == "Physical" OR $role_type == "CHC" AND $jenis == "Technical" OR $role_type =="PSY" AND $jenis == "Psichology"){
				$btnadd = "<a data-backdrop='static' data-toggle='modal' href='".base_url()."index.php/performance/formPerformanceAdd/$jenis/".$id_performance."'><i class='mdi-content-add-box'></i></a>";
			}else{
				$btnadd = "";
			}
			if($role_type == "KSC" AND $jenis == "Physical" OR $role_type == "CHC" AND $jenis == "Technical" OR $role_type =="PSY" AND $jenis == "Psichology"){
				$btndelete = "<span class='mdi-action-delete' onclick='confirmDelete(\"$id_performance\")'></span>";
			}else{
				$btndelete = "";
			}
			
			$query	= $this->db->query("SELECT name as user_nm FROM users WHERE username = '$user_id'");
			if($query->num_rows()>0){
				$row		= $query->row();
				$user_nm 	= $row->user_nm;
			}
			
			$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
				 . " benchmark as benchmark, goal as goal, current as current"
				 . " From master_performance_detail where id_performance = '$id_performance'";
			$query_phy = $this->db->query($sql_phy);
			$table = "";
			if($query_phy -> num_rows() > 0){
				$result_phy = $query_phy->result();
				if($result_phy > 0){
					foreach ($result_phy as $item) {
						$d_id = $item->id;
						$komponen = $item->komponen;
						// $messo = $item->benchmark;
						$current = $item->goal;
						$benchmark = $item->current;
						
						if ($benchmark <= 0) $benchmark = 1;
						if ($current <= 0) $current = 1;
						// if($role_type == "KSC" AND date("Y-m-d", strtotime($date)) == date("Y-m-d")){
							// $btnedit = "<span href='' onclick='showModal(\"$d_id\",\"$komponen\",\"$messo\",\"$current\",\"$benchmark\")'>$item->komponen</span>";
						// }else{
							$btnedit = "$item->komponen";
						// }
						// $hasil = ($current/$benchmark)*100;
						// $result = number_format($hasil, 2, '.', ' ');
						$table .= "<tr><td class='btn-info'><strong>$btnedit</strong></td><td>$item->current</td><td>$item->goal</td></tr>";
					}
				}
			}
			
			$data .= '
				<div class="col s12 m6 l6">
					<div class="card">
                      <div class="card-content">
						<table class="striped">
							<thead>
								<tr>
									<td colspan="2">'.$jenis.' <span class="text-indigo" id="btnGrafik_'.$id_performance.'">
									<span class="mdi-action-assessment" onclick="showGrafik(\''.$id_performance.'\')"></span></span></td>
									<td></td><td>'.$btndelete.'</td>
								</tr>
							</thead>
						</table>
						<div id="grafik_'.$id_performance.'">
							<table class="striped">
								<thead>
									<tr><th class="indigo white-text" colspan="3">Messo '.$messo.'</th></tr>
									<tr>
										<th class="indigo white-text" data-field="id">Komponen</th>
										<th class="indigo white-text" data-field="price">Benchmark</th>
										<th class="indigo white-text" data-field="price">Current</th>
									</tr>
								</thead>
								<tbody>
									'.$table.'
									<tr><td colspan="4">'.$btnadd.'</td></tr>
								</tbody>
							</table>
						</div>
                      </div>
                      <div class="card-action">
						<p>Dibuat Pada : '.date('d M Y H:i:s',strtotime($date)).' <br>Dibuat Oleh &nbsp: '.$user_nm.'</p>
						<p>Catatan : </p>
						<p>'.$catatan.'</p>
                      </div>
                    </div>
                </div>           
			';
		}
	}else{
		$data = "Data Kosong";
	}
	
	if($role_type == "KSC"){
		$type = "physic";
	}
	if($role_type == "CHC"){
		$type = "technical";
	}
	if($role_type == "PSY"){
		$type = "mental";
	}
?>
<!-- START CONTENT -->
<section id="content">
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title white-text" style="text-shadow: 0px 0px 0px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;">Performance Profiling</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="card-stats">
			<div class="row">
				<?php echo $data ?>
				<div id="modalData"></div>
			</div>
		</div>
	</div>
	<!--end container-->
</section>
<?php if($role_type == "KSC" OR $role_type == "CHC" OR $role_type == "PSY"){?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/performance/ChooseAtlet/data/<?php echo $type ?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>
<!-- END CONTENT -->
<script>
	function showModal(d_id,komponen,messo,current,benchmark){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/performance/showPopUp',
			data : {d_id:d_id,komponen:komponen,messo:messo,current:current,benchmark:benchmark},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function confirmDelete(id_performance){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/performance/showDeleteConfirm',
			data : {id_performance:id_performance},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function closeModal(d_id){
		var modal = document.getElementById('myModal_'+d_id);
		modal.style.display = "none";
	}
	
	function showGrafik(id_performance){
		// alert(id_performance);
		$("#grafik_"+id_performance).html("<div class='progress'><div class='indeterminate'></div></div>");
		$("#btnGrafik_"+id_performance).html("<span class='mdi-action-list' onclick='showTable(\""+id_performance+"\")'></span>");
		var options = {
			colors: ['#FF00B2', '#293696', '#FCFC00', '#DDDF00', '#24CBE5', '#64E572', 
			'#FF9655', '#FFF263', '#6AF9C4'],
			 
			chart: {
				polar: true,
				type: 'line',
				renderTo: 'grafik_'+id_performance,
			},

			title: {
				text: '',
				x: -80
			},

			pane: {
				size: '80%'
			},

			navigation: {
				buttonOptions: {
					align: "right",
					enabled: true,
				},
			},
			
			xAxis: {
				categories: [],
				tickmarkPlacement: 'on',
				lineWidth: 0
			},

	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },

	        tooltip: {
	            shared: true,
	            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	        },

	        // legend: {
	            // align: 'right',
	            // verticalAlign: 'top',
	            // y: 70,
	            // layout: 'vertical'
	        // },
			
			series: [{},{}]

			// series: [{
				// name: 'Achievment',
				// data: [10,10,10],
				// pointPlacement: 'on'
			// }, {
				// name: 'Goal',
				// data: [10,10,10],
				// pointPlacement: 'on'
			// },{
				// name: 'Messo',
				// data: [10,10,10],
				// pointPlacement: 'on'
			// }]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/performance/viewGrafik',
			data : {id_performance:id_performance},
			dataType: "json",
			success: function(data){
				options.xAxis.categories = data.categories;
				options.series[0].name = 'Benchmark';
				options.series[0].data = data.benchmark;
				options.series[1].name = 'Current';
				options.series[1].data = data.current;
				var chart = new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showTable(id_performance){
		$("#grafik_"+id_performance).html("<div class='progress'><div class='indeterminate'></div></div>");
		$("#btnGrafik_"+id_performance).html("<span class='mdi-action-assessment' onclick='showGrafik(\""+id_performance+"\")'></span>");
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/performance/viewTable',
			data : {id_performance:id_performance},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#grafik_"+id_performance).html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
</script>