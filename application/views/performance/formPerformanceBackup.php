<?php 
	
	if($this->session->userdata("errorMsg")){
		$errorMsg = $this->session->userdata("errorMsg");
	}else{
		$errorMsg = "";
	}

	if($set == 'physic'){ 
		$title = "Physical";
	}else if($set == 'mental'){
		$title = "Psichology";
	}else{
		$title = "Technical";
	}
	
	$optAtlet	= "";
	 if($atlet){
		foreach($atlet as $row){
			$atlet_id		= $row->username;
			$atlet_nm		= $row->name;
			
			$optAtlet 	.= '<option value ="'.$atlet_id.'">'.$atlet_nm.'</option>';
		}
	 }
?>
<section id="content">
	<div class="container">
		<div class="section">
		 <div id="basic-form" class="section">
			<div class="row">
				<div class="col s12 m12 l6">
					<?php echo $errorMsg?>
					<div class="card-panel">
						<h4 class="header2"><?php echo $title?></h4>
						<div class="row">
							<form class="col s12" method="post" action="<?php echo base_url();?>index.php/performance/save_performance">
								<div class="row">
									<div class="input-field col s12 m4">
										<select id="atlet" name="atlet" required>
											<option value=""> -- Pilih Atlit -- </option>
											<?php echo $optAtlet?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12 m4">
										<input type="hidden" name="jenis" value="<?php echo $title?>" />
										<input type="hidden" name="type" value="<?php echo $set?>" />
										<input type="number" name="messo" value="" placeholder="Messo Ke" required/>
									</div>
								</div>
								<div id="itemlist" class="row">
								  
								</div>
								<br>
								<div class="row">
									<span class="btn btn-block indigo waves-attach" onclick="additem(); return false">Tambah Form</span>
								</div>
								<div class="col-lg-12 col-lg-push-3 col-sm-12">
									<p>Catatan</p>
									<textarea name='catatan' style="width:100%;height:100px"></textarea><br><br>
									<button type="submit" class="btn indigo btn-block">Save</button><br>
								</div>
							</form>
						</div>
					</div>
                </div>			
			</div>
		</div>
	</div>
	<!--end container-->
</section>
<script>
	var i = 1;
	function additem() {
		//menentukan target append
		var itemlist = document.getElementById('itemlist');
		
		//membuat element
		var row = document.createElement('div');
		row.setAttribute('class','row');
		var komponen = document.createElement('div');
		komponen.setAttribute('class','input-field col s12 m12');
		// var goal_value = document.createElement('div');
		// goal_value.setAttribute('class','input-field col s12 m6');
		var bench_value = document.createElement('div');
		bench_value.setAttribute('class','input-field col s12 m6');
		var current_value = document.createElement('div');
		current_value.setAttribute('class','input-field col s12 m6');
		var aksi = document.createElement('div');
		aksi.setAttribute('class','input-field col s12 m12');

//                meng append element
		itemlist.appendChild(row);
		row.appendChild(komponen);
		// row.appendChild(goal_value);
		row.appendChild(current_value);
		row.appendChild(bench_value);
		row.appendChild(aksi);

//                membuat element input
		var input_name = document.createElement('input');
		input_name.setAttribute('name', 'input_name[' + i + ']');
		input_name.setAttribute('placeholder', 'Komponen Penilaian');
		input_name.setAttribute('type', 'text');
		input_name.setAttribute('required', '');

		// var input_nilai = document.createElement('input');
		// input_nilai.setAttribute('name', 'input_nilai[' + i + ']');
		// input_nilai.setAttribute('placeholder', 'Messo');
		// input_nilai.setAttribute('type', 'number');
		// input_nilai.setAttribute('step', 'any');
		// input_nilai.setAttribute('required', '');

		var input_current = document.createElement('input');
		input_current.setAttribute('name', 'input_current[' + i + ']');
		input_current.setAttribute('placeholder', 'Benchmark');
		input_current.setAttribute('type', 'number');
		input_current.setAttribute('step', 'any');
		input_current.setAttribute('required', '');

		var input_bench = document.createElement('input');
		input_bench.setAttribute('name', 'input_goal[' + i + ']');
		input_bench.setAttribute('placeholder', 'Current');
		input_bench.setAttribute('type', 'number');
		input_bench.setAttribute('step', 'any');
		input_bench.setAttribute('required', '');

		var hapus = document.createElement('div');
		hapus.setAttribute('class','form-group');

//                meng append element input
		aksi.appendChild(hapus);
		komponen.appendChild(input_name);
		// goal_value.appendChild(input_nilai);
		current_value.appendChild(input_current);
		bench_value.appendChild(input_bench);

		hapus.innerHTML = '<span class="btn btn-block btn-red">delete</span>';
//                membuat aksi delete element
		hapus.onclick = function () {
			row.parentNode.removeChild(row);
		};

		i++;
	}
</script>