<?php
	if($atlet){
		$list = "";
		foreach($atlet as $row){
			$name 	= $row->name;
			$gambar	= $row->gambar;
			$group	= $row->master_group_name;
			$event	= $row->master_atlet_nomor_event;
			$atlet	= $row->username;
			$wellness		= $row->value_wellness;
			$wellness	= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			
			$query = $this->db->query("SELECT * FROM master_performance WHERE id_performance IN "
									. "( SELECT MAX(id_performance) FROM master_performance WHERE id_user_atlet = '$atlet')");
			if($query->num_rows()>0){
				$row 	= $query->row();
				$messo = $row->messo;
				if($messo == ""){
					$messo = "-";
				}
			}else{
				$messo	= "-";
			}
			
			// $query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
						// . " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
			// if($query->num_rows()>0){
				// $row 	= $query->row();
				// $cidera = $row->cidera;
				// if($cidera == ""){
					// $cidera = "-";
				// }
			// }else{
				// $cidera	= "-";
			// }
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}

			$list .= '
				<a href="'.base_url().'index.php/performance/data/'.$atlet.'" class="content"><li class="collection-item avatar">
					<img src="'.$gambar.'" alt="" class="circle">
					<span class="title">'.$name.'</span>
					<p>'.$group.' | '.$event.' <br>Messo : '.$messo.'
					</p>
					<a href="#!" class="secondary-content"><i style="color: '.$btn.'" class="mdi-action-favorite"></i></a>
				</li></a>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
	
	
	if($role_type == "KSC"){
		$type = "physic";
	}
	if($role_type == "CHC"){
		$type = "technical";
	}
	if($role_type == "PSY"){
		$type = "mental";
	}
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="col s12 m8 l9">
			<ul class="collection">
				<?php echo $list ?>
			</ul>
		</div><br><br><br><br>
	</div>
	<!--end container-->
</section>
<?php if($role_type == "KSC" OR $role_type == "CHC" OR $role_type == "PSY"){?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/performance/ChooseAtlet/data/<?php echo $type ?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>
<!-- END CONTENT -->