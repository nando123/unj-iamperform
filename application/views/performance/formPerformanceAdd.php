<section id="content">
	<div class="container">
		<div class="section">
		 <div id="basic-form" class="section">
			<div class="row">
				<div class="col s12 m12 l6">
					<div class="card-panel">
						<h4 class="header2"><?php echo $set?></h4>
						<div class="row">
							<form class="col s12" method="post" action="<?php echo base_url();?>index.php/performance/saveNewPerformance">
								<div id="itemlist" class="row">
									<input type="hidden" name="jenis" value="<?php echo $set ?>" />
									<input type="hidden" name="id_performance" value="<?php echo $id_performance?>" />
								  
								</div>
								<br>
								<div class="row">
									<span class="btn btn-block indigo waves-attach" onclick="additem(); return false">Tambah Form</span>
								</div>
								<div class="col-lg-12 col-lg-push-3 col-sm-12"><br><br>
									<button type="submit" class="btn indigo btn-block">Save</button><br>
								</div>
							</form>
						</div>
					</div>
                </div>			
			</div>
		</div>
	</div>
	<!--end container-->
</section>
<script>
	var i = 1;
	function additem() {
		//menentukan target append
		var itemlist = document.getElementById('itemlist');
		
		//membuat element
		var row = document.createElement('div');
		row.setAttribute('class','row');
		var komponen = document.createElement('div');
		komponen.setAttribute('class','input-field col s12 m12');
		var bench_value = document.createElement('div');
		bench_value.setAttribute('class','input-field col s12 m6');
		var current_value = document.createElement('div');
		current_value.setAttribute('class','input-field col s12 m6');
		var aksi = document.createElement('div');
		aksi.setAttribute('class','input-field col s12 m12');
		var option = document.createElement('div');
		option.setAttribute('class','input-field col s6 m6');
		var option2 = document.createElement('div');
		option2.setAttribute('class','input-field col s6 m6');

//                meng append element
		itemlist.appendChild(row);
		row.appendChild(komponen);
		row.appendChild(current_value);
		row.appendChild(bench_value);
		row.appendChild(option);
		row.appendChild(option2);
		row.appendChild(aksi);

//                membuat element input
		var input_name = document.createElement('input');
		input_name.setAttribute('name', 'komponen['+i+']');
		input_name.setAttribute('id', 'komponen');
		input_name.setAttribute('placeholder', 'Komponen Penilaian');
		input_name.setAttribute('type', 'text');
		input_name.setAttribute('required', '');

		var input_current = document.createElement('input');
		input_current.setAttribute('name', 'benchmark['+i+']');
		input_current.setAttribute('id', 'benchmark');
		input_current.setAttribute('placeholder', 'Benchmark');
		input_current.setAttribute('type', 'number');
		input_current.setAttribute('step', 'any');
		input_current.setAttribute('required', '');

		var input_bench = document.createElement('input');
		input_bench.setAttribute('name', 'current['+i+']');
		input_bench.setAttribute('id', 'current');
		input_bench.setAttribute('placeholder', 'Current');
		input_bench.setAttribute('type', 'number');
		input_bench.setAttribute('step', 'any');
		input_bench.setAttribute('required', '');
			
		var checkbox = document.createElement('input');
		checkbox.checked	= true;
		checkbox.type = "radio";
		checkbox.name = "option["+i+"]";
		checkbox.value = "descending";
		checkbox.id = "descending"+i+"";
		
		var label = document.createElement('label')
		label.htmlFor = "descending"+i+"";
		label.appendChild(document.createTextNode('Descending'));
		
		var checkbox2 = document.createElement('input');
		checkbox2.type = "radio";
		checkbox2.name = "option["+i+"]";
		checkbox2.value = "ascending";
		checkbox2.id = "ascending"+i+"";
		
		var label2 = document.createElement('label')
		label2.htmlFor = "ascending"+i+"";
		label2.appendChild(document.createTextNode('Ascending'));

		var hapus = document.createElement('div');
		hapus.setAttribute('class','contianer');

//                meng append element input
		aksi.appendChild(hapus);
		komponen.appendChild(input_name);
		current_value.appendChild(input_current);
		bench_value.appendChild(input_bench);
		option.appendChild(checkbox);
		option.appendChild(label);
		option2.appendChild(checkbox2);
		option2.appendChild(label2);

		hapus.innerHTML = '<br><span class="btn btn-block btn-red">delete</span></br>';
//                membuat aksi delete element
		hapus.onclick = function () {
			row.parentNode.removeChild(row);
		};

		i++;
	}
</script>