<?php
	if($atlet){
		$list = "";
		foreach($atlet as $row){
			$name 		= $row->name;
			$gambar		= $row->gambar;
			$group		= $row->master_group_name;
			$event		= $row->master_atlet_nomor_event;
			$atlet		= $row->username;
			$wellness	= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			
			$query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
									. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
			if($query->num_rows()>0){
				$row 	= $query->row();
				$cidera = $row->cidera;
				if($cidera == ""){
					$cidera = "-";
				}
			}else{
				$cidera	= "-";
			}
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}
			
			$list .= '
				<div class="col s12 m4 10">
				<p>
				  <input type="checkbox" class="filled-in" id="atlet_'.$atlet.'" name="atlet[]" value="'.$atlet.'"/>
				  <label for="atlet_'.$atlet.'">'.$name.'</label>
				  <a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 25px" class="mdi-action-favorite"></i></a>
				</p><hr>
				</div>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
?>
<section id="content">
<div class="container">
	<div class="row">
		<div class="col s12 m12 20">
			<div class="card-panel" id="DataWellness">
				<label class="card-title">Pilih Atlit</label>
				<div class="row">
					<div class="col s12 m4 10">
						<p>
							<input type="checkbox" onClick="toggle(this)" id="all"/>
							<label for="all" class="black-text"><b>Select All</b></label>
						</p><hr>
					</div>
					<?php echo $list ?>
				</div>
				<br><br><br><br><br>
			</div>
		</div>
	</div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button class="btn indigo white-text" onClick="nextStep()">
	  Next
	</button>
</div>
</section>

<script>
	function toggle(pilih) {
		checkboxes = document.getElementsByName('atlet[]');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = pilih.checked;
		}
    }
	
	function nextStep(){
		var atlet 	= new Array();
			$.each($("input[name='atlet[]']:checked"), function() {
			atlet.push($(this).val());
		});
		
		if(atlet == ""){
			Materialize.toast('Anda Belum Memilih Atlet', 4000);
			return;
		}
		
		$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url()?>index.php/performance/setAtlet2Create',
			data	: {atlet:atlet},
			success: function(data){
				if(data == 'sukses'){
					window.location.href='<?php echo base_url()?>index.php/performance/formPerformance/data/<?php echo $set?>';
				}else{
					Materialize.toast('Terjadi Kesalahan, Hubungi Petugas', 4000);
					return;
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				Materialize.toast('Terjadi Kesalahan, Hubungi Petugas', 4000);
				return;
			}
		})
	}
</script>