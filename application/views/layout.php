<?php
	$query	= $this->db->query("SELECT * FROM v_cidera_atlet_today WHERE username = '$username'");
	if($query->num_rows()>0){
		$totalCidera = $query->num_rows();
	}else{
		$totalCidera = 0;
	}
	if(isset($role_name)){
		$role_name = $role_name;
	}else{
		$role_name = "";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>UNJ - IAM PERFORM</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url()?>assets/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets/images/favicon/favicon-32x32.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/images/favicon/favicon-32x32.png">
    <!-- For Windows Phone -->

	<link rel="manifest" href="<?php echo base_url()?>assets/manifest.json">
	<meta name="theme-color" content="#000">

    <!-- Custome CSS-->    
    <link href="<?php echo base_url()?>assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection"> 
    <!-- CORE CSS-->    
    <link href="<?php echo base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">   
    <!-- CSS style Horizontal Nav (Layout 03)-->    
    <link href="<?php echo base_url()?>assets/css/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url()?>assets/modal/modal.css" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="<?php echo base_url()?>assets/js/plugins/magnific-popup/magnific-popup.css" type="text/css" rel="stylesheet" media="screen,projection">
    


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url()?>assets/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url()?>assets/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- jQuery Library --> 
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.11.2.min.js"></script>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="black">
                <div class="nav-wrapper"> 
                    <ul class="left">
                      <li class="no-hover"><a href="#" data-activates="slide-out" class="menu-sidebar-collapse btn-floating btn-flat btn-medium waves-effect waves-light black hide-on-large-only"><i class="mdi-navigation-menu" ></i></a></li>
                      <li><h1 class="logo-wrapper"><a class="brand-logo darken-1"><img src="<?php echo base_url()?>assets/images/iam_perform.png" alt="IAM PERFORM"></a> <span class="logo-text">UNJ - IAM FERFORM</span></h1></li>
                    </ul>
                </div>
            </nav>

            <!-- HORIZONTL NAV START-->
             <nav id="horizontal-nav" class="white hide-on-med-and-down">
                <div class="nav-wrapper">                  
                  <ul id="nav-mobile" class="left hide-on-med-and-down">
                    <li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>" class="black-text">
                            <i class="mdi-action-home" style="font-size:30px"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/wellness" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/wellness_black.png"/></i>
                            <span>Wellness</span>
                        </a>
                    </li>
                    <li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/monotony" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/monotony_black.png"/></i>
                            <span>Training Load</span>
                        </a>
                    </li>
                    <li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/pmc" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/pmc_black.png"/></i>
                            <span>PMC</span>
                        </a>
                    </li>
                    <li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/mealplan" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/meal_black.png"/></i>
                            <span>Meal Plan</span>
                        </a>
                    </li>
					<li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/recovery" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/recovery_black.png"/></i>
                            <span>Recovery Management</span>
                        </a>
                    </li>
					<li>
                        <a style="margin-top:2px" href="<?php echo base_url()?>index.php/performance" class="black-text">
                            <i><img style="margin-top:5px" width="30px" src="<?php echo base_url()?>assets/icon/chart_black.png"/></i>
                            <span>Performance Profiling</span>
                        </a>
                    </li>
					<li>
                        <a href="#!" class="dropdown-menu black-text" data-activates="Chartsdropdown">
                            <i class="mdi-action-perm-contact-cal" style="font-size:30px"></i>
                            <span><?php echo $name ?> <i class="mdi-navigation-arrow-drop-down right"></i></span>
                        </a>
                    </li>
                    
                  </ul>
                </div>
			</nav>

                <!-- Chartsdropdown -->
                <ul id="Chartsdropdown" class="dropdown-content dropdown-horizontal-list">
                    <li><a href="<?php echo base_url()?>index.php/account" class="black-text">Account</span> </a></li>
                    <li><a href="<?php echo base_url()?>index.php/login/logout" class="black-text">Sign Out</span> </a></li>
                </ul>
            <!-- HORIZONTL NAV END-->

        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav hide-on-large-only">
                <ul id="slide-out" class="side-nav leftside-navigation ">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="<?php echo $gambar ?>" alt="" class="circle responsive-img valign profile-image">
                            </div>
                            <div class="col col s8 m8 l8">
                                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"><?php echo $name ?></a>
								<p class="user-roal"><?php echo $role_name?></p>
							</div>
                        </div>
                    </li>
                    <li class="bold">
						<a href="<?php echo base_url()?>" class="waves-effect waves-cyan"><i class="mdi-action-home"></i> Dashboard</a>
                    </li>
					<div class="divider"></div>
					<li class="no-padding">
					<ul class="collapsible collapsible-accordion">
					<li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-content-content-paste"></i> Modul</a>
                        <div class="collapsible-body">
                            <ul>
						<li>
							<a href="<?php echo base_url()?>index.php/wellness" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/wellness_black.png"/></i> Wellness</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>index.php/monotony" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/monotony_black.png"/></i> Training Load</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>index.php/pmc" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/pmc_black.png"/></i> PMC</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>index.php/mealplan" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/meal_black.png"/></i> Meal Plan</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>index.php/recovery" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/recovery_black.png"/></i> Recovery Management</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>index.php/performance" class="waves-effect"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/chart_black.png"/></i> Performance Profiling</a>
						</li>
							</ul>
						</div>
					</li>
					</ul>
					</li>
					<div class="divider"></div>
                    <li class="bold">
						<a href="<?php echo base_url()?>index.php/cidera"><i class="mdi-maps-local-hospital"></i> Cidera <span class="red badge white-text"><?php echo $totalCidera?> Atlet</span></a>
					</li>
					<div class="divider"></div>
                    <li class="bold">
						<a href="<?php echo base_url()?>index.php/member"><i class="mdi-social-group"></i> Group</a>
					</li>
					<div class="divider"></div>
					<li class="no-padding">
					<ul class="collapsible collapsible-accordion">
					<li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-image-brightness-7"></i> Personal Information</a>
                        <div class="collapsible-body">
                            <ul>
								<li class="bold">
									<a href="<?php echo base_url()?>index.php/profile"><i class="mdi-action-face-unlock"></i> Profile</a>
								</li>
								<li class="bold">
									<a href="<?php echo base_url()?>index.php/account"><i class="mdi-action-account-circle"></i> Account</a>
								</li>
							</ul>
						</div>
					</li>
					<div class="divider"></div>
					</ul>
					</li>
                    <li class="bold">
						<a href="<?php echo base_url()?>index.php/login/logout"><i class="mdi-navigation-arrow-back"></i> Sign Out</a>
					</li>
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only black"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <?php $this->load->view($page)?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->



    <!-- //////////////////////////////////////////////////////////////////////////// -->

   <!-- ================================================
    Scripts
    ================================================ -->
	
	 <!--Jquery-->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.11.2.min.js"></script>    
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.form.js"></script>      
    
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/materialize.js"></script>
	
	<!--prism-->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/prism.js"></script>
	
    <!--scrollbar-->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/sparkline/sparkline-script.js"></script>
		
    <!-- masonry -->
    <script src="<?php echo base_url()?>assets/js/plugins/masonry.pkgd.min.js"></script>
	
    <!-- imagesloaded -->
    <script src="<?php echo base_url()?>assets/js/plugins/imagesloaded.pkgd.min.js"></script>
	
    <!-- magnific-popup -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!--jvectormap-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jvectormap/vectormap-script.js"></script>

	<script src="<?php echo base_url();?>assets/radar/highcharts.js"></script>
	<script src="<?php echo base_url();?>assets/radar/highcharts-more.js"></script>
	<script src="<?php echo base_url();?>assets/radar/modules/exporting.js"></script>

    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-material-datetimepicker.js"></script>

<script>
	$("#filter").hide();
	$("#grafik").hide();
</script>

<script>
	$('#date').bootstrapMaterialDatePicker
	({
		time: false,
		clearButton: true
	});
</script>

<script>
$(document).on('change', '#image_upload_file', function () {
	var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

	$('#image_upload_form').ajaxForm({
		beforeSend: function() {
			progressBar.fadeIn();
			var percentVal = '0%';
			bar.width(percentVal)
			percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal)
			percent.html(percentVal);
		},
		success: function(html, statusText, xhr, $form) {	
			obj = $.parseJSON(html);
			if(obj.status){
				var percentVal = '100%';
				bar.width(percentVal)
				percent.html(percentVal);
				$("#imgArea>img").prop('src',obj.image_medium);
				window.location.reload();
			}else{
				alert(obj.error);
			}
		},
		complete: function(xhr) {
			progressBar.fadeOut();
		}	
	}).submit();
});
</script>
<script type="text/javascript">
      /*
       * Masonry container for Gallery page
       */
      var $containerGallery = $(".masonry-gallery-wrapper");
      $containerGallery.imagesLoaded(function() {
        $containerGallery.masonry({
            itemSelector: '.gallary-item img',
           columnWidth: '.gallary-sizer',
           isFitWidth: true
        });
      });

      //popup-gallery
      $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: true,    
        fixedContentPos: true,
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile mfp-no-margins mfp-with-zoom',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
          verticalFit: true,
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
          titleSrc: function(item) {
            return item.el.attr('title') + '<small>by IAM PERFORM</small>';
          },
        zoom: {
          enabled: true,
          duration: 300 // don't foget to change the duration also in CSS
        }
        }
      });
</script>
</body>


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout03/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Aug 2015 16:04:34 GMT -->
</html>