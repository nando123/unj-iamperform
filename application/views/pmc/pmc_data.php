<?php 	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
	
	$ret = "";
	if($pmcData){
		$tmpDate = 1;
		$ret .= '<table class="striped">';
		foreach($pmcData as $key){
			$detail_id			= $key->detail_id;
			$detail_date		= $key->detail_date;
			$detailSession		= $key->detail_session;
			$detailVolume		= $key->detail_volume;
			$detail_actual		= $key->detail_actual;
			$detailIntensity	= $key->detail_intensity;
			$detail_warmUpVolume	= $key->detail_warmUpVolume;
			$detail_warmUpActual	= $key->detail_warmUpActual;
			$detail_cooldownVolume	= $key->detail_cooldownVolume;
			$detail_cooldownActual	= $key->detail_cooldownActual;
			$detailVolumeTechnic		= $key->detail_volumeTechnic;
			$detail_actualTechnic		= $key->detail_actualTechnic;
			$detail_warmUpVolumeTechnic	= $key->detail_warmUpVolumeTechnic;
			$detail_warmUpActualTechnic	= $key->detail_warmUpActualTechnic;
			$detail_coolDownVolumeTechnic	= $key->detail_coolDownVolumeTechnic;
			$detail_coolDownActualTechnic	= $key->detail_coolDownActualTechnic;
			
			if($detailVolumeTechnic == "" OR $detailVolumeTechnic == 0){
				$detailVolumeTechnic = $detailVolume;
			}
			
			$daySet = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
			
			$day	= date("w", strtotime($detail_date));
			
			$hari		= $daySet[$day];
			$tangal	= date("d M Y", strtotime($detail_date));
			
			if($tmpDate<>$detail_date){
				$ret .='
					<thead><tr><th colspan="7" class="indigo white-text"><h6><b>'.$hari.', '.$tangal.'</b></h6></th></tr></thead>
				';
				$tmpDate = $detail_date;
			}
			
			$ret .='<tbody>'
					 . '<input type="hidden" name="detail_id[]" value="'.$detail_id.'"/>'
					 . '<tr><th colspan="7" class="red white-text"><h6><b>Session Name : '.$detailSession.'</b></h6></th></tr>'
					 // . '<tr><td colspan="7">'.$detailSession.'</td></tr>'
					 . '<tr><th colspan="2"><h6><b>Physic</b></h6></th><th colspan="2"><h6><b>Technic</b></h6></th></tr>'
					 . '<tr><td colspan="2"><b>Warm Up</b></td><td colspan="2"><b>Warm Up</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$detail_warmUpVolume.'</td><td>'.$detail_warmUpActual.'</td><td>'.$detail_warmUpVolumeTechnic.'</td><td>'.$detail_warmUpActualTechnic.'</td></tr>'
					 . '<tr><td colspan="2"><b>Primary Training</b></td><td colspan="2"><b>Primary Training</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$detailVolume.'</td><td>'.$detail_actual.'</td><td>'.$detailVolumeTechnic.'</td><td>'.$detail_actualTechnic.'</td></tr>'
					 . '<tr><td colspan="2"><b>Cool Down</b></td><td colspan="2"><b>Cool Down</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$detail_cooldownVolume.'</td><td>'.$detail_cooldownActual.'</td><td>'.$detail_coolDownVolumeTechnic.'</td><td>'.$detail_coolDownActualTechnic.'</td></tr>'
					 . '</tbody>';
				
		}
			$ret .="</table>";
	}else{
		$ret .="Data Tidak Ditemukan";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">PMC</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">       
				  <?php echo $ret ?>
                </div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>

<?php if($role_type == 'KSC' OR $role_type == "CHC"){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/pmc/setPMC/<?php echo $group_id?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>