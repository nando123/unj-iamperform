<?php
		
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
	$ret ="";
	if($monotonyWeek){
		$modal = "";
		$ttl_goal = "";
		$ttl_aktual = "";
		$tmp_dttm = 0;
		$table =  "<thead><tr>"
				. "<td>Sesi Latihan</td>"
				. "<td>Tanggal</td>"
				. "<td>Session Type</td>"
				. "<td>Time</td>"
				. "<td>RPE</td>"
				. "<td>Actual</td>"
				. "<td>TTL Goal</td>"
				. "<td>TTL Actual</td></tr></thead><tbody>";
		foreach ($monotonyWeek as $id => $key) {
			$did 		= $key->did;
			$dSession 	= $key->detail_session;
			$t_goal 	= $key->detail_volume;
			$goal 		= $key->detail_intensity;
			$actual 	= $key->detail_actual;
			$goal_nm 	= $key->intensity_name;
			$actual_nm 	= $key->actual_name;
			$created_user_id = $key->distribute_creator;
			$aid 		= $key->distribute_athlete;
			$date		= $key->detail_date;
			if($actual != ''){
				$actual = $actual;
			}else{
				$actual = 0;
				$actual_nm = "NOT SET";
			}
			
			
			
			$sql = "SELECT * FROM mdlmonotony_intensity order by intensity_id";
			$query = $this->db->query($sql);
			$result = $query->result();
			if($result){
				$opt = "<select class='form-control' id='slcAct_$did'>";
				foreach ($result as $key) {
					$iid = $key->intensity_id;
					$ket = $key->intensity_name;
					if($actual == $iid){
						$slc = "selected";
					}else{
						$slc = "";
					}
					$opt .= "<option $slc value='$iid'>$iid - $ket</option>";
				}
				$opt .="</select>";
			}else{
				$opt="";
			};
			
			if($role_type == "KSC"){
				$btnset = '<a class="waves-effect waves-light btn modal-trigger indigo white-text" href="#modal4_'.$did.'">'.$actual.' - '.$actual_nm.'</a>';
				// $btnset = "<span class='btn-flat buttonSetUpdate' onClick='setActual(\"$did\",\"$monotony_id\")' data-date='$date' ><i class='mdi-action-done'></i></span>";
			}else{
				$btnset = "<p>$actual - $actual_nm</p>";
			}
			
			$modal .= '
				<div id="modal4_'.$did.'" class="modal">
					<div class="modal-content" style="border:none">
						<p>Pilih Nilai Actual : </p>
						'.$opt.'
					</div>
					<div class="modal-footer indigo">
						<button href="#" class="waves-effect waves-red btn-flat modal-action modal-close white-text" onClick="setActual(\''.$did.'\',\''.$monotony_id.'\')">Save</button>
						<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close white-text">Cancel</a>
					</div>
                </div>
			';
			
			$dttm = date('d M Y',strtotime($date));
			$hari = date('d',strtotime($date));
			$day = date('D', strtotime($date));
			$month = date('m', strtotime($date));
			$year = date('Y', strtotime($date));
			$total_aktual[$dttm] = $t_goal * $actual;
			$total_goal[$id] = $t_goal * $goal;
			$ttl_goal += $total_goal[$id];
			$ttl_aktual += $total_aktual[$dttm];
			$dayList = array(
				'Sun' => 'Minggu',
				'Mon' => 'Senin',
				'Tue' => 'Selasa',
				'Wed' => 'Rabu',
				'Thu' => 'Kamis',
				'Fri' => 'Jumat',
				'Sat' => 'Sabtu'
			);
			
			$tgl = $hari;
			$bln = $month;
			$thn = $year;

			$jd = gregoriantojd( $bln, $tgl, $thn );
			$dw = jddayofweek( $jd ); //0 = Minggu
			$ref_date = strtotime( "$bln/$tgl/$thn" );
			//atau pake mktime(0,0,0,$bln,$tgl,$thn);
			
			$week_of_year = date( 'W', $ref_date );
			$week_of_month = $week_of_year - date( 'W', strtotime( "$bln/1/$thn" ) );
			
			$sql = "SELECT * FROM master_pmc WHERE monotony_id = '$monotony_id' AND detail_id = '$did' ORDER BY pmc_id ASC";
			$query = $this->db->query($sql);
			$result = $query->result();
			$data = "";
			if($result){
				foreach ($result as $key) {
					$d_activity = $key->detail_kegiatan;
					$pmc_id		= $key->pmc_id;
					if($role_type == "KSC"){
						$btnedit = "<a class='btn-flat' onclick='showEdit(\"$d_activity\",\"$pmc_id\")'>$d_activity</a>";
						$btndele = "<span class='btn-flat' onClick='confirmDelete(\"$pmc_id\")'><i class='mdi-action-delete'></i></span>";
					}else{
						$btnedit = "$d_activity";
						$btndele = "";
					}
					$data .= "
						<tr>
							<td class='white-text'>$btnedit</td>
							<td colspan='7'>$btndele</td>
						</tr>
					";
				}
			}
			if($role_type == "KSC"){
				$data .= '<tr id="formNew_'.$did.'"><td colspan="8">'
					   . '<span class="btn-flat" onclick="addForm(\''.$monotony_id.'\',\''.$did.'\')"><i class="mdi-content-add-circle"></i></span>'
					   . '</td></tr>';
			}
			
			$table .="<tr><td style='background:#ededed'>$dayList[$day] - ".($id+1)."</td><td>$dttm</td><td>$dSession</td>"
				   . "<td>$t_goal</td><td>$goal - $goal_nm</td><td>$btnset</td><td>$ttl_goal</td><td>$ttl_aktual</td></tr>"
				   . "$data";
		}
			$table .="</tbody>$modal";
	}else{
		$table = "
				<tbody>
					<tr>
						<td colspan='4'><h4 class='margin-top-no margin-bottom-no'>Data Kosong</h4></td>
					</tr>
				</tbody>
			";
	}
	
	$btnGrafik	= "<a href='#' class='btn white-text green' onClick='showGrafikPMC(\"$monotony_id\")'>Grafik PMC</a>";
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header indigo">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title"><?php echo $nama_user?></h5>
							<p class="flight-card-date"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date">PMC</p>
						</div>
						<div class="col s12 m12 l5 center-align">
							<p class="flight-card-date">
								<?php echo $btnGrafik ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card" id="DataMonotony">
			<div id="modalData"></div>
			<div id="responsive-table">
              <div class="container"> 
				<p class="content-sub-heading">MICRO <?php echo $week_of_year?></p>
                <div class="col s12 m12 16" id="DataWellness">
				<table class="responsive-table striped">
					<?php echo $table ?>
				</table>
				</div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>

<script>
	var i = 1;
	
	function addForm(monotony_id,did){
		var tr = $(document.createElement("tr")).attr("id", "rowNew"+i);
		
		tr.after().html("<td colspan='2'><input id='kegiatan_"+i+"' placeholder=' Komponen'/><span class='btn-flat' onclick='setKomponen(\""+i+"\",\""+monotony_id+"\",\""+did+"\")'><i class='mdi-content-send'></i></i></span> <span class='btn-flat' onclick='deleteForm()'><i class='mdi-navigation-cancel'></i></span></td>");
		
		tr.insertBefore("#formNew_"+did);
		i++;
	}
	
	function setKomponen(row,monotony_id,did){
		var kegiatan = $("#kegiatan_"+row).val();
		
		// alert(kegiatan);
		// return;
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/setPmcDetail',
			data : {kegiatan:kegiatan,monotony_id:monotony_id,did},
			success: function(data){
				if(data == "ERROR"){
					$("#kegiatan_"+row).focus;
				}else{
					$("#rowNew"+row).html("<td></td><td><a class='btn-flat' data-toggle='modal' href='#ui_modal_"+data+"'>"+kegiatan+"</a></td><td colspan='7'><a data-toggle='modal' href='#ui_delete_"+data+"'><i class='mdi-action-delete'></i></td>");
					
					window.location.reload()
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showEdit(d_activity,pmc_id){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showEdit',
			data : {pmc_id:pmc_id,d_activity:d_activity},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function confirmDelete(pmc_id){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/confirmDelete',
			data : {pmc_id:pmc_id},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function closeModal(pmc_id){
		var modal = document.getElementById('myModal_'+pmc_id);
		modal.style.display = "none";
	}
	
	function deleteForm(){
		i--;
		$("#rowNew"+i).remove();
	}
	
	function setActual(did,monotony_id){
		var actual	= $("#slcAct_"+did).val();
		
		$.ajax({
			type	: "POST",
			url		: "<?php echo base_url()?>index.php/pmc/setActual",
			data	: {did,did,monotony_id,monotony_id,actual,actual},
			success	: function(data){
				window.location.reload();
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	
	
	function showGrafikPMC(monotony_id){
		// alert(monotony_id);
		// return;
		$("#DataMonotony").html("Loading.........");
		
		var options = {
			chart: {
				renderTo: 'DataMonotony',
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				}
			},
			title: {
				text: 'PMC Chart RPE & Actual'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: [
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				},
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				}
			],
			legend: {
				enabled: true,
			},
			series: [
				{
					type: 'column',
					name: 'RPE',
					data: [{}],
					color: "#3F51B5",
					yAxis:0
				},
				{
					type: 'column',
					name: 'Actual',
					data: [{}],
					color: "#ef5350",
					yAxis:0
				}
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/viewGrafikDay',
			data : {monotony_id:monotony_id},
			dataType: "json",
			success: function(data){
				options.xAxis.categories = data.categories;
				options.series[0].data = data.rpe;
				options.series[1].data = data.actual;
				var chart = new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
</script>