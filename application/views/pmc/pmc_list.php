<?php 
	$ret ="";
	if($pmc){
		foreach ($pmc as $key) {
			$date 			= $key->mdlmonotony_date;
			$monotony_id 	= $key->distribute_monotony;
			$vdt = date("Y-m-d",strtotime($date));
			$date = date("d M Y H:i:s",strtotime($date));
			
			$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
			$query	= $this->db->query($sql);
			if($query->num_rows()>0){
				$row 	= $query->row();
				$startMonotony 	= $row->startMonotony;
				$endMonotony 	= $row->endMonotony;
				$start			= date("d M Y",strtotime($startMonotony));
				$end			= date("d M Y",strtotime($endMonotony));
			}else{
				$start	= "";
				$end	= "";
			}
			
			$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' group by detail_reference");
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyPerDay = $row->monotonyPerDay;
			}else{
				$monotonyPerDay	= 0;
			}
			
			$ret .= "
				<tr>
					<td style='font-weight:bold'><a class='btn-flat btn-brand' href='".base_url()."index.php/pmc/pmcData/$monotony_id''>$start - $end</a></td>
					<td style='font-weight:bold'>
						$monotonyPerDay
					</td>
				</tr>
			";
		}
	}else{
		$ret .= "<tr><td>Data Kosong</td></tr>";
	}
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title white-text" style="text-shadow: 0px 0px 0px #ffffff;"><b><?php echo $nama_user?></b></h5>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date white-text" style="text-shadow: 0px 0px 0px #ffffff;">PMC</p>
						</div>
					</div>
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showGrafikToogle()" class="btn-flat blue white-text"><span class="mdi-action-trending-up"></span> Grafik</a> &nbsp&nbsp
						<a href="#" onClick="showFilter()" class="btn-flat blue white-text"><i class="mdi-content-sort"></i> Filter</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="grafik">
            <div class="col s12 m12 l12">
				<div class="card-panel">
                    <div class="row">                    
						<div class="input-field col s12 m12">
							<button class="btn btn-flat blue white-text" onClick="showPMCMonth()">Month PMC</button>
						</div>                
						<div class="input-field col s6 m2">
							<button class="btn btn-flat blue white-text" onClick="showPMCYear()">Year PMC</button>
						</div>             
						<!--<div class="input-field col s12 m2">
							<button class="btn btn-flat blue white-text" onClick="showPMCYearWeek()">Year Week PMC</button>
						</div>-->
                    </div>
				</div>
            </div>
		</div>
		<div class="row" id="filter">
            <div class="col s12 m12 l12">
				<div class="card-panel">
                    <div class="row">
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Tahun</p>
							<select id="year">
								<?php
								$mulai= date('Y') - 50;
								for($i = $mulai;$i<$mulai + 100;$i++){
									$sel = $i == $year ? ' selected="selected"' : '';
									echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
								}
								?>
							</select>
						</div>
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Bulan</p>
							<select id="month">
								<option value="all">-- All --</option>
								<?php
									$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
									for($bulan=1; $bulan<=12; $bulan++){
										if($month == $bulan){
											$select = "selected";
										}else{
											$select = "";
										}
										if($bulan<=9) {
											echo "<option $select value='0$bulan'>$bln[$bulan]</option>"; 
										}else{ 
											echo "<option $select value='$bulan'>$bln[$bulan]</option>"; 
										}
									}
								?>
							</select>
						</div>
						<div class="input-field col s12 m2">
							<button class="btn btn-flat blue white-text" onClick="showPMC()">Filter</button>
						</div>
                    </div>
				</div>
            </div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
              <div class="row">
                <div class="col s12 m12 16" id="DataWellness">
                  <table class="striped">
						<thead>
							<tr><th style="text-align:center" >Tanggal Training Load</th>
							<th>Total</th></tr>
						</thead>
                      <?php echo $ret ?>
                  </table>
                </div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<?php if($role_type == 'KSC' OR $role_type == "CHC"){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a class="btn-floating btn-large red" href="<?php echo base_url();?>index.php/pmc/setPMC/<?php echo $group_id?>">
	  <i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
<?php } ?>

<script>
	function showFilter() {
        $("#filter").toggle(); 
    }
	function showGrafikToogle() {
        $("#grafik").toggle(); 
    }
	
	function showPMC(){
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		var month 	= $("#month").val();
		var year 	= $("#year").val();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showFilter',
			data : {month:month,year:year},
			success: function(data){
				$("#DataWellness").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showPMCYearWeek(){
		var year 	= $("#year").val();
		
		
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showPMCYearWeek',
			data : {year:year},
			dataType: "json",
			success: function(data){
				$("#DataWellness").html(data);
				// options.xAxis.categories = data.categories;
				// options.series[0].data = data.fatigue;
				// options.series[1].data = data.fitness;
				// options.series[2].data = data.tsb;
				
				// var chart 		= new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function showPMCYear(){
		var year 	= $("#year").val();
		
		
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		var options = {
			chart: {
				renderTo: 'DataWellness',
			},
			xAxis: {
				type: 'datetime',
				labels: {
					overflow: 'justify'
				}
			},
			yAxis: {
				minorGridLineWidth: 0,
				gridLineWidth: 0,
				alternateGridColor: null,
			},
			plotOptions: {
				spline: {
					lineWidth: 4,
					states: {
						hover: {
							lineWidth: 5
						}
					},
					marker: {
						enabled: false
					},
				}
			},
			exporting: { 
				enabled: false 
			},
			series: [
				{
					type: 'spline',
					name: 'Fatigue',
					data: [{}],
					color: "#f44336",
					yAxis:0
				},
				{
					type: 'spline',
					name: 'Fitness',
					data: [{}],
					color: "#0D47A1",
					yAxis:0
				},
				{
					type: 'spline',
					name: 'Form',
					data: [{}],
					color: "#FFEB3B",
					yAxis:0
				}
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showPMCYear',
			data : {year:year},
			dataType: "json",
			success: function(data){
				$("#DataWellness").html(data);
				options.xAxis.categories = data.categories;
				options.series[0].data = data.fatigue;
				options.series[1].data = data.fitness;
				options.series[2].data = data.tsb;
				
				var chart 		= new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function showPMCMonth(){
		var year 	= $("#year").val();
		var month 	= $("#month").val();
		
		
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		var options = {
			chart: {
				renderTo: 'DataWellness',
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			exporting: { 
				enabled: false 
			},
			series: [
				{
					type: 'spline',
					name: 'Fatigue',
					data: [{}],
					color: "#f44336",
					yAxis:0
				},
				{
					type: 'spline',
					name: 'Fitness',
					data: [{}],
					color: "#0D47A1",
					yAxis:0
				},
				{
					type: 'spline',
					name: 'Form',
					data: [{}],
					color: "#FFEB3B",
					yAxis:0
				}
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showPMCMonth',
			data : {year:year,month:month},
			dataType: "json",
			success: function(data){
				$("#DataWellness").html(data);
				options.xAxis.categories = data.categories;
				options.series[0].data = data.fatigue;
				options.series[1].data = data.fitness;
				options.series[2].data = data.tsb;
				
				var chart 		= new Highcharts.Chart(options);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
</script>