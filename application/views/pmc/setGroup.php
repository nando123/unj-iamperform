<?php
	if($group){
		$list = "";
		foreach($group as $row){
			$group_id 	= $row->groupcode;
			$group_nm	= $row->master_group_name;
			$group_ico	= $row->master_group_logo;
			$sql = " SELECT COUNT(username) as jml_atl FROM v_member_atlet WHERE master_licence_group = '$group_id'";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				$row = $query->row();
				$jmlAtlet = $row->jml_atl;
			}
			$sql1 = " SELECT COUNT(b.name) as jml_chc FROM master_role as a"
				 . " LEFT JOIN users as b on b.role_id = a.role_id"
				 . " WHERE a.group_id = '$group_id' AND a.role_type = 'CHC'";
			$query1 = $this->db->query($sql1);
			if($query1->num_rows()>0){
				$row = $query1->row();
				$jml_chc = $row->jml_chc;
			}
			$list .= '
				<a href="'.base_url().'index.php/pmc/set_atlet/'.$group_id.'" class="content"><li class="collection-item avatar">
					<img src="'.$group_ico.'" alt="" class="circle">
					<span class="title">'.$group_nm.'</span>
					<p>Jumlah Atlet : '.$jmlAtlet.'</p>
					<p>Jumlah Pelatih : '.$jml_chc.'</p>
					<a href="#!" class="secondary-content"></a>
				</li></a>
			';
		}
	}
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="col s12 m8 l9">
			<ul class="collection">
				<?php echo $list ?>
			</ul>
		  </div>
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->