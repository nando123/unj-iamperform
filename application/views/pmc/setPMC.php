<?php
	$optAtlet	= "";
	 if($atlet){
		foreach($atlet as $row){
			$atlet_id		= $row->username;
			$atlet_nm		= $row->name;
			
			$optAtlet 	.= '<option value ="'.$atlet_id.'">'.$atlet_nm.'</option>';
		}
	 }
?>
<div class="container">
	<div class="row">
		<div class="navbar-fixed">
			<div class="col s12 m12 16" style="position:fixed;z-index: 998;">
				<div id="buttonCard">
					<button class="btn-floating indigo left" onClick="hideCard()">
						<i class="mdi-hardware-keyboard-arrow-up"></i>
					</button>
				</div>
				<div id="buttonCard">
					<button class="btn-floating red right" onClick="back()">
						<i class="mdi-content-clear"></i>
					</button>
				</div>
				<div class="card-panel" id="cardOption">
					<div class="row">
						<div class="input-field col s12 m4">
							<p style="margin-top:-10px" class="header"><b>Pilih Atlit</p></b>
							<select id="atlet" onchange="pickAtlet()">
								<option value=""> -- Pilih Atlit -- </option>
								<?php echo $optAtlet?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m5">
							<p style="margin-top:-10px" class="header"><b>Pilih Tahun Training Load</p></b>
							<select id="year">
								<?php
								$mulai= date('Y') - 50;
								for($i = $mulai;$i<$mulai + 100;$i++){
									$sel = $i == $year ? ' selected="selected"' : '';
									echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
								}
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m4">
							<p style="margin-top:-10px" class="header"><b>Pilih Training Load</p></b>
							<select id="trainingLoad">
								
							</select>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m4">
							<button class="btn btn-flat indigo white-text" onClick="showPMC()">Show PMC</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="pmcData">
	</div>
</div>

<?php if($role_type == 'KSC'){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button class="btn-floating btn-large indigo" onClick="savePMC()">
		<i class="large mdi-content-save"></i>
	</button>
</div>
<?php } ?>

<?php if($role_type == 'CHC'){ ?>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button class="btn-floating btn-large indigo" onClick="savePmcTechnic()">
		<i class="large mdi-content-save"></i>
	</button>
</div>
<?php } ?>

<script>
	$('#atlet').on('change', function() {
		var atlet	= this.value;
		var year	= $("#year").val();
		
		// alert(atlet);
		// return;
		
		$("#trainingLoad").html("<option>loading....</option>");
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/getPMCAtlet',
			data : {atlet:atlet,year:year},
			success: function(data){
				$("#trainingLoad").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	});
	
	$('#year').on('change', function() {
		var atlet	= $("#atlet").val();
		var year	= this.value;
		
		$("#trainingLoad").html("<option>loading....</option>");
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/getPMCAtlet',
			data : {atlet:atlet,year:year},
			success: function(data){
				$("#trainingLoad").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	});
	
	function showPMC(){
		var monotonyID	= $("#trainingLoad").val();
		$("#pmcData").html("loading....");
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/getPMCData',
			data : {monotonyID:monotonyID},
			success: function(data){
				$("#pmcData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function savePmcTechnic(){
		var monotonyID	= $("#trainingLoad").val();
		
		var detail_id = $("input[name='detail_id[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumeTechnic = $("input[name='volumeTechnic[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumeWarmUpTechnic = $("input[name='volumeWarmUpTechnic[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumeCoolDownTechnic = $("input[name='volumeCoolDownTechnic[]']")
            .map(function(){return $(this).val();}).get();
			
		var actualTechnic = $('select[name="actualTechnic[]"] option:selected')
            .map(function(){return $(this).val();}).get();
			
		var actualWarmUpTechnic = $('select[name="actualWarmUpTechnic[]"] option:selected')
            .map(function(){return $(this).val();}).get();
			
		var actualCoolDownTechnic = $('select[name="actualCoolDownTechnic[]"] option:selected')
            .map(function(){return $(this).val();}).get();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/saveDataPmcTechnic',
			data : {monotonyID:monotonyID,detail_id:detail_id,volumeTechnic:volumeTechnic,volumeWarmUpTechnic:volumeWarmUpTechnic,volumeCoolDownTechnic:volumeCoolDownTechnic,actualTechnic:actualTechnic,actualWarmUpTechnic:actualWarmUpTechnic,actualCoolDownTechnic:actualCoolDownTechnic},
			success: function(data){
				if(data == "sukses"){
					Materialize.toast('Berhasil Menyimpan PMC :)', 4000);
				}else{
					Materialize.toast('Gagal Menyimpan PMC :(', 4000);
				};
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function savePMC(){
		var monotonyID	= $("#trainingLoad").val();
		
		var detail_id = $("input[name='detail_id[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumePhysic = $("input[name='volumePhysic[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumePhysicWarmUp = $("input[name='volumePhysicWarmUp[]']")
            .map(function(){return $(this).val();}).get();
			
		var volumePhysicCoolDown = $("input[name='volumePhysicCoolDown[]']")
            .map(function(){return $(this).val();}).get();
			
		var actualPhysic = $('select[name="actualPhysic[]"] option:selected')
            .map(function(){return $(this).val();}).get();
			
		var actualPhysicWarmUp = $('select[name="actualPhysicWarmUp[]"] option:selected')
            .map(function(){return $(this).val();}).get();
			
		var actualPhysicCoolDown = $('select[name="actualPhysicCoolDown[]"] option:selected')
            .map(function(){return $(this).val();}).get();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/saveDataPMC',
			data : {monotonyID:monotonyID,detail_id:detail_id,volumePhysic:volumePhysic,volumePhysicWarmUp:volumePhysicWarmUp,volumePhysicCoolDown:volumePhysicCoolDown,actualPhysic:actualPhysic,actualPhysicWarmUp:actualPhysicWarmUp,actualPhysicCoolDown:actualPhysicCoolDown},
			success: function(data){
				if(data == "sukses"){
					Materialize.toast('Berhasil Menyimpan PMC :)', 4000);
				}else{
					Materialize.toast('Gagal Menyimpan PMC :(', 4000);
				};
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function back(){
		window.history.back(-2);
	}
	
	function hideCard(){
		$("#cardOption").hide();
		$("#buttonCard").html('<button class="btn-floating indigo left" onClick="showCard()"><i class="mdi-hardware-keyboard-arrow-down"></i></button>');
	}
	function showCard(){
		$("#cardOption").show();
		$("#buttonCard").html('<button class="btn-floating indigo left" onClick="hideCard()"><i class="mdi-hardware-keyboard-arrow-up"></i></button>');
	}
</script>