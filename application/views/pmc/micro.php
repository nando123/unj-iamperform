<?php
		
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}				
	}else{
		$wellness = "#607D8B";
	}
	
	$no = 1;
	$tmp_dttm = 0;
	list($date_pmc,$data_pmc) = $pmc;
	if($date_pmc){
		$modal = "";
		$ret = "";
		$ttl_goal = "";
		$ttl_aktual = "";
		foreach ($date_pmc as $id => $key) {
			$optj 		= "Dynamic Warmup";
			$date	 	= $key->detail_date;
			$reference 	= $key->detail_reference;
			$dttm = date('d M Y',strtotime($date));
			$day = date('D', strtotime($date));
			$hari = date('d', strtotime($date));
			$month = date('m', strtotime($date));
			$year = date('Y', strtotime($date));
			$dayList = array(
				'Sun' => 'Minggu',
				'Mon' => 'Senin',
				'Tue' => 'Selasa',
				'Wed' => 'Rabu',
				'Thu' => 'Kamis',
				'Fri' => 'Jumat',
				'Sat' => 'Sabtu'
			);
			
			$tgl = $hari;
			$bln = $month;
			$thn = $year;

			$jd = gregoriantojd( $bln, $tgl, $thn );
			$dw = jddayofweek( $jd ); //0 = Minggu
			$ref_date = strtotime( "$bln/$tgl/$thn" );
			//atau pake mktime(0,0,0,$bln,$tgl,$thn);
			
			$week_of_year = date( 'W', $ref_date );
			$week_of_month = $week_of_year - date( 'W', strtotime( "$bln/1/$thn" ) );
			
			$sess = "";
			$ttl_goal = '';
			$ttl_aktual = '';
			if($data_pmc){
				foreach($data_pmc as $item){
					$detail_id 	= $item->detail_id;
					$goal 	   	= $item->detail_intensity;
					$actual	   	= $item->detail_actual;
					$goal_nm 	= $item->intensity_name;
					$actual_nm 	= $item->actual_name;
					$t_goal 	= $item->detail_volume;
					$total_goal = $t_goal * $goal;
					$ttl_goal += $total_goal;
					
					if($actual != ''){
						$actual = $actual;
					}else{
						$actual = 0;
						$actual_nm = "NOT SET";
					}
					
					$sql = " SELECT a.* FROM `master_pmc` as a"
						 . " WHERE a.detail_id = '$detail_id'";
					$query	= $this->db->query($sql);
					$pmc_data = '';
					if($query->num_rows()>0){
						foreach($query->result() as $p){
							$d_activity = $p->detail_kegiatan;
							$pmc_id		= $p->pmc_id;
							if($role_type == "KSC"){
								$btnedit = "<a class='btn-flat' onclick='showEdit(\"$d_activity\",\"$pmc_id\")'>$d_activity</a>";
								$btndele = "<span class='btn-flat' onClick='confirmDelete(\"$pmc_id\")'><i class='mdi-action-delete'></i></span>";
							}else{
								$btnedit = "$d_activity";
								$btndele = "";
							}
							$pmc_data .= "
								<tr>
									<td>$btnedit</td>
									<td colspan='7'>$btndele</td>
								</tr>
							";
						}
					}
					if($role_type == "KSC"){
						$pmc_data .= '<tr id="formNew_'.$detail_id.'"><td colspan="8">'
							   . '<span class="btn-flat" onclick="addForm(\''.$monotony_id.'\',\''.$detail_id.'\')"><i class="mdi-content-add-circle"></i></span>'
							   . '</td></tr>';
					}
					
					$sql = "SELECT * FROM mdlmonotony_intensity order by intensity_id";
					$query = $this->db->query($sql);
					$result = $query->result();
					if($result){
						$opt = "<select class='form-control' id='slcAct_$detail_id'>";
						foreach ($result as $key) {
							$iid = $key->intensity_id;
							$ket = $key->intensity_name;
							if($actual == $iid){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$opt .= "<option $slc value='$iid'>$iid - $ket</option>";
						}
						$opt .="</select>";
					}else{
						$opt="";
					};
					
					if($role_type == "KSC"){
						$btnset = '<a class="waves-effect waves-light btn modal-trigger indigo white-text" href="#modal4_'.$detail_id.'">'.$actual.' - '.$actual_nm.'</a>';
						// $btnset = "<span class='btn-flat buttonSetUpdate' onClick='setActual(\"$did\",\"$monotony_id\")' data-date='$date' ><i class='mdi-action-done'></i></span>";
					}else{
						$btnset = "<p>$actual - $actual_nm</p>";
					}
					
					$modal .= '
						<div id="modal4_'.$detail_id.'" class="modal">
							<div class="modal-content" style="border:none">
								<p>Pilih Nilai Actual : </p>
								'.$opt.'
							</div>
							<div class="modal-footer indigo">
								<button href="#" class="waves-effect waves-red btn-flat modal-action modal-close white-text" onClick="setActual(\''.$detail_id.'\',\''.$monotony_id.'\')">Save</button>
								<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close white-text">Cancel</a>
							</div>
						</div>
					';
					
					$total_aktual = $t_goal * $actual;
					$ttl_aktual += $total_aktual;
					$sess .="<tr><td style='background:#ededed'>$dayList[$day] - ".($id+1)."</td><td>$dttm</td>"
						  . "<td>$item->detail_session</td>"
						  . "<td>$t_goal</td><td>$goal - $goal_nm</td><td>$btnset</td>"
						  . "<td>$ttl_goal</td><td>$ttl_aktual</td></tr>$pmc_data";
				}
				// $sess .= "<tbody><tr><td class='btn-success'>GOAL</td><td colspan='4'>$ttl_goal</td></tr>"
					  // . "<tr><td class='btn-info'>ACTUAL</td><td colspan='4'>$ttl_aktual</td></tr></tbody>";
			}
			
			$ret .= "<thead><tr>"
				. "<td class='btn-brand' style='text-align: center' >Sesi Latihan</td>"
				. "<td class='btn-brand'>Tanggal</td>"
				. "<td class='btn-brand'>Session Type</td>"
				. "<td class='btn-brand'>Time</td>"
				. "<td class='btn-brand'>RPE</td>"
				. "<td class='btn-brand'>Actual</td>"
				. "<td class='btn-brand'>TTL Goal</td>"
				. "<td class='btn-brand'>TTL Actual</td></tr></thead><tbody>$sess</tbody>$modal";
		}
	}else{
		$ret = "
				<div class='col-sm-12 col-xs-12'>
					<table class='table table-hover'>
						<thead>
							<tr>
								<th colspan = '4' class='btn-danger'>Tidak Ada Data</th>
							</tr>
						</thead>
					</table>
				</div>
			";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header indigo">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<img style="border: 7px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title"><?php echo $nama_user?></h5>
							<p class="flight-card-date"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date">PMC</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="modalData"></div>
			<div id="responsive-table">
              <div class="container"> 
				<p class="content-sub-heading">MICRO <?php echo $week_of_year?></p>
                <div class="col s12 m12 16" id="DataWellness">
				<table class="responsive-table striped">
					<?php echo $ret ?>
				</table>
				</div>
              </div>
            </div>
		</div>
	</div>
	<!--end container-->
</section>

<script>
	var i = 1;
	
	function addForm(monotony_id,did){
		var tr = $(document.createElement("tr")).attr("id", "rowNew"+i);
		
		tr.after().html("<td colspan='2'><input id='kegiatan_"+i+"' placeholder=' Komponen'/><span class='btn-flat' onclick='setKomponen(\""+i+"\",\""+monotony_id+"\",\""+did+"\")'><i class='mdi-content-send'></i></i></span> <span class='btn-flat' onclick='deleteForm()'><i class='mdi-navigation-cancel'></i></span></td>");
		
		tr.insertBefore("#formNew_"+did);
		i++;
	}
	
	function setKomponen(row,monotony_id,did){
		var kegiatan = $("#kegiatan_"+row).val();
		
		// alert(kegiatan);
		// return;
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/setPmcDetail',
			data : {kegiatan:kegiatan,monotony_id:monotony_id,did},
			success: function(data){
				if(data == "ERROR"){
					$("#kegiatan_"+row).focus;
				}else{
					$("#rowNew"+row).html("<td></td><td><a class='btn-flat' data-toggle='modal' href='#ui_modal_"+data+"'>"+kegiatan+"</a></td><td colspan='7'><a data-toggle='modal' href='#ui_delete_"+data+"'><i class='mdi-action-delete'></i></td>");
					
					window.location.reload()
				}
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showEdit(d_activity,pmc_id){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/showEdit',
			data : {pmc_id:pmc_id,d_activity:d_activity},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function confirmDelete(pmc_id){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/pmc/confirmDelete',
			data : {pmc_id:pmc_id},
			// dataType: "json",
			success: function(data){
				// alert(data);
				$("#modalData").html(data);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
	function closeModal(pmc_id){
		var modal = document.getElementById('myModal_'+pmc_id);
		modal.style.display = "none";
	}
	
	function deleteForm(){
		i--;
		$("#rowNew"+i).remove();
	}
	
	function setActual(did,monotony_id){
		var actual	= $("#slcAct_"+did).val();
		
		$.ajax({
			type	: "POST",
			url		: "<?php echo base_url()?>index.php/pmc/setActual",
			data	: {did,did,monotony_id,monotony_id,actual,actual},
			success	: function(data){
				window.location.reload();
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
	
</script>