<?php
	if($atlet){
		$list = "";
		foreach($atlet as $row){
			$name 		= $row->name;
			$gambar		= $row->gambar;
			$group		= $row->master_group_name;
			$event		= $row->master_atlet_nomor_event;
			$atlet		= $row->username;
			$wellness	= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			
			$query = $this->db->query(" SELECT cidera,notes FROM master_kondisi WHERE username='$atlet'"
									. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
			if($query->num_rows()>0){
				$row 	= $query->row();
				$cidera = $row->cidera;
				$notes = $row->notes;
				if($cidera == ""){
					$cidera = "-";
				}
			}else{
				$cidera	= "-";
			}
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}
			
			$list .= '
				<a href="'.base_url().'index.php/wellness/data/'.$atlet.'" class="content">
				<li class="collection-item avatar">
					<img src="'.$gambar.'" alt="" class="circle">
					<span class="title">'.$name.'</span>
					<p>'.$group.' |  <i style="text-align:right">'.$event.'</i></p>
					<p>Cidera : '.$cidera.' -  <i style="text-align:right"> '.$notes.'</p>
					<a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 35px" class="mdi-action-favorite"></i></a>
				</li>
				</a>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
	}
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div class="container">
				<div class="row">
					<div class="row">
						<div class="input-field col s12">
							<input id="search" name="search" type="text" onkeyup="ajaxSearch();" class="validate">
							<input id="group_id" name="group_id" type="hidden" value="<?php echo $group_id?>" class="validate">
							<label for="first_name">Search</label>
						</div>
					</div>
				</div>
			</div>
		  </div>
		<div class="col s12 m8 l9" id="divlist">
			<ul class="collection">
				<?php echo $list ?>
			</ul>
		</div><br><br><br><br>
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->
<script type="text/javascript">
	function ajaxSearch() {
		$('#divlist').html("loading....");
		var input_data = $('#search').val();
		var group_id = $('#group_id').val();

		$.ajax({
			type: "POST",
            url: "<?php echo base_url()?>index.php/wellness/searchAtlet",
            data: {input_data:input_data,group_id:group_id},
            success: function(data) {
                  // return success
                  // if (data.length > 0) {
                      // $('#divlist').remove();
                      // $('#suggestions').show();
                      // $('#autoSuggestionsList').addClass('auto_list');
                $('#divlist').html(data);
                  // }
            },error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
            }
        });

	}
</script>