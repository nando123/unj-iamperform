<?php 
$lama_tidur 	= ""; 
$soreness 		= ""; 
$energi 		= ""; 
$kualitas_tidur = ""; 
$mood 			= ""; 
$stress			= ""; 
$mental			= ""; 
$jml_nutrisi	= ""; 
$kwt_nutrisi	= ""; 
$hydration		= ""; 

if($nilai){
	foreach ($nilai as $id=>$key) {
		$nilai = $key->nilai;
		$img = $key->ket_image;
		
		if($nilai == 1){
			$ketLamaTidur		= "< 4 Jam";
			$ketKwtTidur		= "Buruk";
			$ketStress			= "Sangat Stress";
			$ketFocus			= "Sangat Tidak Fokus";
			$ketSoreness		= "Sangat Soreness";
			$ketNutrisi			= "Selalu Lapar";
			$ketFatigue			= "Kelelahan";
			$ketKwtNutrisi		= "Sangat Buruk";
			$ketMood			= "Sangat Tidak Mood";
			$ketHidrasi			= "Sangat Buruk";
		}
		if($nilai == 2){
			$ketLamaTidur		= "5 Jam";
			$ketKwtTidur		= "Kurang Sekali";
			$ketStress			= "Stress";
			$ketFocus			= "Tidak Fokus";
			$ketSoreness		= "Soreness";
			$ketNutrisi			= "Tidak Puas";
			$ketFatigue			= "Tidak Berenergi";
			$ketKwtNutrisi		= "Tidak Baik";
			$ketMood			= "Tidak Mood";
			$ketHidrasi			= "Tidak Baik";
		}
		if($nilai == 3){
			$ketLamaTidur		= "6 Jam";
			$ketKwtTidur		= "Kurang";
			$ketStress			= "Sedikit Stress";
			$ketFocus			= "Sedikit Fokus";
			$ketSoreness		= "Ada Soreness";
			$ketNutrisi			= "Sedikit Puas";
			$ketFatigue			= "Kurang Berenergi";
			$ketKwtNutrisi		= "Kurang Baik";
			$ketMood			= "Kurang Mood";
			$ketHidrasi			= "Kurang Baik";
		}
		if($nilai == 4){
			$ketLamaTidur		= "7 Jam";
			$ketKwtTidur		= "Cukup Baik";
			$ketStress			= "Tidak Stress";
			$ketFocus			= "Fokus";
			$ketSoreness		= "Tidak Ada Soreness";
			$ketNutrisi			= "Puas";
			$ketFatigue			= "Berenergi";
			$ketKwtNutrisi		= "Baik";
			$ketMood			= "Mood Baik";
			$ketHidrasi			= "Baik";
		}
		if($nilai == 5){
			$ketLamaTidur		= "> 8 Jam";
			$ketKwtTidur		= "Sempurna";
			$ketStress			= "Tidak Stress Sama Sekali";
			$ketFocus			= "Sangat Fokus";
			$ketSoreness		= "Tidak Soreness Sama Sekali";
			$ketNutrisi			= "Sangat Memuaskan";
			$ketFatigue			= "Sangat Berenergi";
			$ketKwtNutrisi		= "Sangat Baik";
			$ketMood			= "Mood Sangat Baik";
			$ketHidrasi			= "Sangat Baik";
		}
		
		$lama_tidur .= "
			  <input required id='lama_tidur_$id' name='lama_tidur' type='radio' value='$nilai' />
			  <label for='lama_tidur_$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketLamaTidur</label>
		";
		
		$soreness .= "
			  <input required id='soreness$id' name='soreness' type='radio' value='$nilai' />
			  <label for='soreness$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketSoreness</label>
		";
		$energi .= "
			  <input required id='energi$id' name='energi' type='radio' value='$nilai' />
			  <label for='energi$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketFatigue</label>
		";
		$kualitas_tidur .= "
			  <input required id='kualitas_tidur$id' name='kualitas_tidur' type='radio' value='$nilai' />
			  <label for='kualitas_tidur$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketKwtTidur</label>
		";
		$mood .= "
			  <input required id='mood$id' name='mood' type='radio' value='$nilai' />
			  <label for='mood$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketMood</label>
		";
		$stress .= "
			  <input required id='stress$id' name='stress' type='radio' value='$nilai' />
			  <label for='stress$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketStress</label>
		";
		$mental .= "
			  <input required id='mental$id' name='mental' type='radio' value='$nilai' />
			  <label for='mental$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketFocus</label>
		";
		$jml_nutrisi .= "
			  <input required id='jml_nutrisi$id' name='jml_nutrisi' type='radio' value='$nilai' />
			  <label for='jml_nutrisi$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketNutrisi</label>
		";
		$kwt_nutrisi .= "
			  <input required id='kwt_nutrisi$id' name='kwt_nutrisi' type='radio' value='$nilai' />
			  <label for='kwt_nutrisi$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketKwtNutrisi</label>
		";
		$hydration .= "
			  <input required id='hydration$id' name='hydration' type='radio' value='$nilai' />
			  <label for='hydration$id'><img width='35' src='".base_url()."assets/icons/$img' /> &nbsp&nbsp $ketHidrasi</label>
		";
	}
}
?>
<section id="content">
	<form method="post" action="<?php echo base_url()?>index.php/wellness/saveWellness">
	<div class="container">
		<div id="card-widgets" class="seaction">
			<div class="row">
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Lama Tidur</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $lama_tidur?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Soreness</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $soreness?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Energi</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $energi?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Kualitas Tidur</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $kualitas_tidur?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Mood</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $mood?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Stress</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $stress?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Mental Focus</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $mental?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Jumlah Nutrisi</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $jml_nutrisi?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Kualitas Nutrisi</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $kwt_nutrisi?></p>
							</div>
						</li>
					</ul>
                </div>
                <div class="col s12 m6 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Hydration</p>
						</li>
						<li class="collection-item">							
							<div id="input-radio-buttons" class="section">
								<p><?php echo $hydration?></p>
							</div>
						</li>
					</ul>
                </div>
			</div>
			<div class="row">
                <div class="col s12 m12 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Berat Badan</p>
						</li>
						<li class="collection-item">	
							<input type="number" name="berat_badan" step="any" required/>
						</li>
					</ul>
				</div>
                <div class="col s12 m12 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Nadi Basa Per Menit</p>
						</li>
						<li class="collection-item">	
							<input type="number" name="rhr" max="99" required/>
						</li>
					</ul>
				</div>
				<div class="col s12 m12 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Cidera (Jika Tidak Ada Kosongkan)</p>
						</li>
						<li class="collection-item">	
							<input type="text" name="cidera" />
						</li>
					</ul>
				</div>
				<div class="col s12 m12 l4">
					<ul id="task-card" class="collection with-header">
						<li class="collection-header cyan">
							<p class="task-card-date">Notes</p>
						</li>
						<li class="collection-item">	
							<input type="text" name="notes" />
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
                <div class="col s12 m12 l4">
					<button class="btn blue btn-block" type="submit">Simpan</button>
				</div><br><br><br>
			</div>
		</div>
	</div>
	</form>
</section>