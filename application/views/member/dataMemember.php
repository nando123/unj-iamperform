<?php
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}	
	}else{
		$wellness = "#607D8B";
	}
	
	$dataPhysic = '';
	$jenisPhysic = '';
	$komponenPhysical = "";
	$result_current = "";
	$result_benchmark	= "";
	if($kpfPhysical){
		$dataPhysic .= '
			<div id="flight-card" class="card">
				<div class="card-header" id="kpfPhysical">
				
				</div>
			</div>
		';
		foreach ($kpfPhysical as $key) {
			$id_performance = $key->id_performance;
			$no_event = $key->master_atlet_nomor_event;
			$group = $key->master_group_name;
			$jenisPhysic .= $key->jenis_performance;
			$date = $key->created_dttm;
			$tmpdate = explode(" ", $date);
			$dttm = $tmpdate[0];
			$catatan = $key->catatan;
			$nama = $key->name;
			$gambar = $key->gambar;
			$messo = $key->messo;
			
			$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
				 . " benchmark as benchmark, goal as goal, current as current"
				 . " From master_performance_detail where id_performance = '$id_performance'";
			$query_phy = $this->db->query($sql_phy);
			if($query_phy -> num_rows() > 0){
				$result_phy = $query_phy->result();
				if($result_phy > 0){
					$table = "";
					foreach ($result_phy as $item) {
						$d_id = $item->id;
						$komponenPhysical .=  "'".$item->komponen;
						$current 	= $item->goal;
						$benchmark 	= $item->current;
						
						$hasil_benchmark 	= ($benchmark/$benchmark)*100;
						$hasil_current 		= ($current/$benchmark)*100;
						$result_current 	.= number_format($hasil_current, 2, '.', ' ');
						$result_benchmark 	.= number_format($hasil_benchmark, 2, '.', ' ');
						
						if(count($komponenPhysical)) {
							$komponenPhysical .= "',";
						}
						if(count($hasil_current)) {
							$result_current .= ",";
						}
						if(count($hasil_benchmark)) {
							$result_benchmark .= ",";
						}
					}
				}
			}
		}
	}
	
	$dataTechnical = '';
	$jenisTechnical = '';
	$komponenTechnical = "";
	$result_currentTechnical  = "";
	$result_benchTechnical 	= "";
	if($KPFtechnical){
		$dataTechnical .= '
			<div id="flight-card" class="card">
				<div class="card-header" id="kpfTechnical">
				
				</div>
			</div>
		';
		foreach ($KPFtechnical as $key) {
			$id_performance 	= $key->id_performance;
			$no_event 			= $key->master_atlet_nomor_event;
			$group 				= $key->master_group_name;
			$jenisTechnical 	.= $key->jenis_performance;
			$date 				= $key->created_dttm;
			$tmpdate 			= explode(" ", $date);
			$dttm 				= $tmpdate[0];
			$catatan 			= $key->catatan;
			$nama 				= $key->name;
			$gambar 			= $key->gambar;
			
			$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
				 . " benchmark as benchmark, goal as goal, current as current"
				 . " From master_performance_detail where id_performance = '$id_performance'";
			$query_phy = $this->db->query($sql_phy);
			if($query_phy -> num_rows() > 0){
				$result_phy = $query_phy->result();
				if($result_phy > 0){
					$table = "";
					foreach ($result_phy as $item) {
						$d_id = $item->id;
						$komponenTechnical .=  "'".$item->komponen;
						$current 	= $item->goal;
						$benchmark 	= $item->current;
						
						if ($benchmark <= 0) $benchmark = 1;
						if ($current <= 0) $current = 1;
						
						$hasil_benchmark	= ($benchmark/$benchmark)*100;
						$hasil_current 		= ($current/$benchmark)*100;
						$result_benchTechnical 		.= number_format($hasil_benchmark, 2, '.', ' ');
						$result_currentTechnical 	.= number_format($hasil_current, 2, '.', ' ');
						
						if(count($komponenTechnical)) {
							$komponenTechnical .= "',";
						}
						if(count($hasil_current)) {
							$result_currentTechnical .= ",";
						}
						if(count($hasil_benchmark)) {
							$result_benchTechnical .= ",";
						}
					}
				}
			}
		}
	}
	
	
	$dataPsichology = '';
	$jenisPsichology = '';
	$komponenPsichology = "";
	$result_currentPsichology  = "";
	$result_benchPsichology 	= "";
	if($KPFpsichology){
		$dataPsichology .= '
			<div id="flight-card" class="card">
				<div class="card-header" id="kpfPsichology">
				
				</div>
			</div>
		';
		foreach ($KPFpsichology as $key) {
			$id_performance = $key->id_performance;
			$no_event = $key->master_atlet_nomor_event;
			$group = $key->master_group_name;
			$jenisPsichology .= $key->jenis_performance;
			$date = $key->created_dttm;
			$tmpdate = explode(" ", $date);
			$dttm = $tmpdate[0];
			$catatan = $key->catatan;
			$nama = $key->name;
			$gambar = $key->gambar;
			
			$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
				 . " benchmark as benchmark, goal as goal, current as current"
				 . " From master_performance_detail where id_performance = '$id_performance'";
			$query_phy = $this->db->query($sql_phy);
			if($query_phy -> num_rows() > 0){
				$result_phy = $query_phy->result();
				if($result_phy > 0){
					$table = "";
					foreach ($result_phy as $item) {
						$komponenPsichology .=  "'".$item->komponen;
						$d_id 		= $item->id;
						$current 	= $item->goal;
						$benchmark 	= $item->current;
						if ($benchmark <= 0) $benchmark = 1;
						if ($current <= 0) $current = 1;
						$hasil_benc 	= ($benchmark/$benchmark)*100;
						$hasil_current 	= ($current/$benchmark)*100;
						$result_benchPsichology 	.= number_format($hasil_benc, 2, '.', ' ');
						$result_currentPsichology 	.= number_format($hasil_current, 2, '.', ' ');
						
						if(count($komponenPsichology)) {
							$komponenPsichology .= "',";
						}
						if(count($hasil_current)) {
							$result_currentPsichology .= ",";
						}
						if(count($hasil_benc)) {
							$result_benchPsichology .= ",";
						}
					}
				}
			}
		}
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<span class="card-title activator grey-text text-darken-4"><i class="mdi-navigation-more-vert right"></i></span>
						</div>
						<div class="col s5 m5 l5 center-align">
							<img style="border: 5px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $nama_user?></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Performance Profiling</p>
						</div>
					</div>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i></span><br>
					<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
						<a href="<?php echo base_url()?>index.php/member/personalInformation">
							<li><div class="collapsible-header"><i class="mdi-action-assignment-ind"></i> Personal Information</div></li>
						</a>
						<a href="<?php echo base_url()?>index.php/member/WellnessInformation">
							<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/wellness_black.png"/></i> Wellness</div></li>
						</a>
<!-- 						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/monotony_black.png"/></i> Training Load</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/pmc_black.png"/></i> PMC</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/meal_black.png"/></i>  Meal Plan</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/recovery_black.png"/></i>  Recovery</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/chart_black.png"/></i>  Performance Profiling</div></li> -->
					</ul>
				</div>
			</div>
			<?php echo $dataPhysic ?>
			<?php echo $dataTechnical ?>
			<?php echo $dataPsichology ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {

		$('#kpfPhysical').highcharts({
			 colors: ['#FF00B2', '#293696', '#FCFC00', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
			
			chart: {
				polar: true,
				type: 'line'
			},

			title: {
				text: '<?php echo $jenisPhysic ?> Performance | Messo <?php echo $messo?>',
				// x: -80
			},

			pane: {
				size: '80%'
			},

			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			
			xAxis: {
				categories: [<?php echo $komponenPhysical ?>],
				tickmarkPlacement: 'on',
				lineWidth: 0
			},

	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },

	        tooltip: {
	            shared: true,
	            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	        },

	        // legend: {
	            // align: 'right',
	            // verticalAlign: 'top',
	            // y: 70,
	            // layout: 'vertical'
	        // },

			series: [
				{
					name: 'Benchmark',
					data: [<?php echo $result_benchmark?>],
					pointPlacement: 'on'
				},
				{
					name: 'Current',
					data: [<?php echo $result_current ?>],
					pointPlacement: 'on'
				},
			]

		});

		$('#kpfTechnical').highcharts({
			 colors: ['#FF00B2', '#293696', '#FCFC00', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
			
			chart: {
				polar: true,
				type: 'line'
			},

			title: {
				text: '<?php echo $jenisTechnical ?> Performance | Messo <?php echo $messo?>',
				// x: -80
			},

			pane: {
				size: '80%'
			},

			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			
			xAxis: {
				categories: [<?php echo $komponenTechnical ?>],
				tickmarkPlacement: 'on',
				lineWidth: 0
			},

	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },

	        tooltip: {
	            shared: true,
	            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	        },

	        // legend: {
	            // align: 'right',
	            // verticalAlign: 'top',
	            // y: 70,
	            // layout: 'vertical'
	        // },

			series: [
				{
					name: 'Benchmark',
					data: [<?php echo $result_benchTechnical?>],
					pointPlacement: 'on'
				},
				{
					name: 'Current',
					data: [<?php echo $result_currentTechnical?>],
					pointPlacement: 'on'
				}
			]

		});

		$('#kpfPsichology').highcharts({
			 colors: ['#FF00B2', '#293696', '#FCFC00', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
			
			chart: {
				polar: true,
				type: 'line'
			},

			title: {
				text: '<?php echo $jenisPsichology ?> Performance | Messo <?php echo $messo?>',
				// x: -80
			},

			pane: {
				size: '80%'
			},

			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			
			xAxis: {
				categories: [<?php echo $komponenPsichology ?>],
				tickmarkPlacement: 'on',
				lineWidth: 0
			},

	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },

	        tooltip: {
	            shared: true,
	            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	        },

	        // legend: {
	            // align: 'right',
	            // verticalAlign: 'top',
	            // y: 70,
	            // layout: 'vertical'
	        // },

			series: [
				{
					name: 'Benchmark',
					data: [<?php echo $result_benchPsichology?>],
					pointPlacement: 'on'
				},
				{
					name: 'Current',
					data: [<?php echo $result_currentPsichology?>],
					pointPlacement: 'on'
				}
			]
		});
	});
</script>