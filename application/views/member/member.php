<?php
	if($member){
		$list 	= "";
		$rlt	= 1;
		foreach($member as $row){
			$atlet 			= $row->name;
			$gambar			= $row->gambar;
			$group			= $row->master_group_name;
			$event			= $row->master_atlet_nomor_event;
			$user_id		= $row->username;
			// $role_member	= $row->role_member;
			$wellness		= $row->value_wellness;
			$wellness_date	= $row->wellness_date;
			$dttm		= date("Y-m-d");
			$query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
									. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
									
			if($query->num_rows()>0){
				$row 	= $query->row();
				$cidera = $row->cidera;
				if($cidera == ""){
					$cidera = "-";
				}
			}else{
				$cidera	= "-";
			}						
			
			if($wellness_date == $dttm){			
				if($wellness <= 59){
					$btn = "#FF0000";
				}elseif($wellness >= 60 && $wellness <= 69) {
					$btn = "#FF9D00";
				}elseif($wellness >= 70 && $wellness <= 79){
					$btn = "#E1FF00";
				}elseif($wellness >= 80 && $wellness <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}				
			}else{
				$btn = "#607D8B";
			}			
			
			// if($role_member == "ATL"){
				// $role_member = "Atlet";
			// }else{
				// $role_member = "Pelatih";
			// }
			$list .= '
				<a href="'.base_url().'index.php/member/dataMemember/'.$user_id.'" class="content"><li class="collection-item avatar">
					<img src="'.$gambar.'" alt="" class="circle">
					<span class="title">'.$atlet.'</span>
					<p>Group '.$group.' | '.$event.'
						<br> Cidera : '.$cidera.'
					</p>
					<a href="#!" class="secondary-content"><i style="color: '.$btn.'" class="mdi-action-favorite"></i></a>
				</li></a>
			';
		}
	}else{
		$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Member</span>
				</li>';
	}
?>
<!-- START CONTENT -->
<section id="content">

	<!--start container-->
	<div class="container">
		<div class="col s12 m8 l9">
			<ul class="collection">
				<?php echo $list ?>
			</ul>
		  </div>
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->