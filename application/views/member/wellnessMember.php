<?php 
	$no = 1;
	if($wellness){
		$dataTable = "";
		$no = 1;
		foreach ($wellness as $key) {
			$date 		= date('d M Y',strtotime($key->created_dttm));
			$lama_tidur = $key->lama_tidur;
			$kualitas_tidur = $key->kualitas_tidur;
			$soreness	=  $key->soreness;
			$energi 	= $key->energi;
			$mood 		= $key->mood;
			$stress 	= $key->stress;
			$mental 	= $key->mental;
			$jml_nutrisi = $key->jml_nutrisi;
			$kualitas_nutrisi = $key->kualitas_nutrisi;
			$hidrasi 	= $key->hidrasi;
			
			$lama_tidur_clr 	= getColor($lama_tidur);
			$kualitas_tidur_clr = getColor($kualitas_tidur);
			$soreness_clr 		= getColor($soreness);
			$energi_clr 		= getColor($energi);
			$mood_clr 			= getColor($mood);
			$stress_clr 		= getColor($stress);
			$mental_clr 		= getColor($mental);
			$jml_nutrisi_clr 	= getColor($jml_nutrisi);
			$kualitas_nutrisi_clr 	= getColor($kualitas_nutrisi);
			$hidrasi_clr 		= getColor($hidrasi);
			
			$nilai = array(
				$lama_tidur = $key->lama_tidur * 2,
				$kualitas_tidur = $key->kualitas_tidur * 2,
				$soreness =  $key->soreness * 2,
				$energi = $key->energi * 2,
				$mood = $key->mood * 2,
				$stress = $key->stress * 2,
				$mental = $key->mental * 2,
				$jml_nutrisi = $key->jml_nutrisi * 2,
				$kualitas_nutrisi = $key->kualitas_nutrisi * 2,
				$hidrasi = $key->hidrasi * 2
			);
			
			$cidera = $key->cidera;
			if($cidera == ""){
				$cidera = "-";
			}
			
			$total = array_sum($nilai);
			
			if($total <= 59){
				$btn = "#FF0000";
			}elseif($total >= 60 && $total <= 69) {
				$btn = "#FF9D00";
			}elseif($total >= 70 && $total <= 79){
				$btn = "#E1FF00";
			}elseif($total >= 80 && $total <= 89){
				$btn = "#9BFF77";
			}else{
				$btn = "#00CE25";
			}
			
			$dataTable .= "
				<tr>
					<td>$no</td>
					<td>$date</td>
					<td style='background : $lama_tidur_clr; text-align:center'>$key->lama_tidur</td>
					<td style='background : $kualitas_tidur_clr; text-align:center'>$key->kualitas_tidur</td>
					<td style='background : $soreness_clr; text-align:center'>$key->soreness</td>
					<td style='background : $energi_clr; text-align:center'>$key->energi</td>
					<td style='background : $mood_clr; text-align:center'>$key->mood</td>
					<td style='background : $stress_clr; text-align:center'>$key->stress</td>
					<td style='background : $mental_clr; text-align:center'>$key->mental</td>
					<td style='background : $jml_nutrisi_clr; text-align:center'>$key->jml_nutrisi</td>
					<td style='background : $kualitas_nutrisi_clr; text-align:center'>$key->kualitas_nutrisi</td>
					<td style='background : $hidrasi_clr; text-align:center'>$key->hidrasi</td>
					<td style='text-align:center'>$key->berat</td>
					<td style='text-align:center'>$key->nadi</td>
					<td style='text-align:center'>$cidera</td>
					<td style='background : $btn; text-align:center'>$total</td>
				</tr>
			";
			$no++;
		}
	}else{
		$dataTable = "<tr><td colspan='13'>Tidak Ada Data</td></tr>";
	}
	
	function getColor($data){
		if($data == 1){
			$btn = "#FF0000";
		}elseif($data == 2) {
			$btn = "#FF9D00";
		}elseif($data == 3){
			$btn = "#E1FF00";
		}elseif($data == 4){
			$btn = "#9BFF77";
		}else{
			$btn = "#00CE25";
		}
		
		return $btn;
	}
	
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}
		
		$btnCreate = '';
	}else{		
		if($role_type == "ATL" AND $atlet == $username){
			$btnCreate = '
				<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
					<a class="btn-floating btn-large red" href="'.base_url().'index.php/wellness/createWellness">
					  <i class="large mdi-editor-mode-edit"></i>
					</a>
				</div>';
		}else{
			$btnCreate = '';
		}
		$wellness = "#607D8B";
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<span class="card-title activator grey-text text-darken-4"><i class="mdi-navigation-more-vert right"></i></span>
						</div>
						<div class="col s5 m5 l5 center-align">
							<img style="border: 5px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $nama_user?></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">
								<?php echo $group_name ?> -
								<?php echo $no_event ?>
							</p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Performance Profiling</p>
						</div>
					</div>
					
					<div class="col s12 m12 l5 center-align">
						<a href="#" onClick="showGrafik()" class="btn-flat blue white-text"><span class="mdi-action-trending-up"></span> Grafik</a> &nbsp&nbsp
						<a href="#" onClick="showFilter()" class="btn-flat blue white-text"><i class="mdi-content-sort"></i> Filter</a>
					</div>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i></span><br>
					<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
						<a href="<?php echo base_url()?>index.php/member/personalInformation">
							<li>
								<div class="collapsible-header"><i class="mdi-action-assignment-ind"></i> Personal Information</div>
							</li>
						</a>
						<a href="<?php echo base_url()?>index.php/member/WellnessInformation">
							<li>
								<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/wellness_black.png"/></i> Wellness</div>
							</li>
						</a>
<!-- 						<li>
							<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/monotony_black.png"/></i> Training Load</div>
						</li>
						<li>
							<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/pmc_black.png"/></i> PMC</div>
						</li>
						<li>
							<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/meal_black.png"/></i> Meal Plan</div>
						</li>
						<li>
							<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/recovery_black.png"/></i> Recovery</div>
						</li>
						<li>
							<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/chart_black.png"/></i> Performance Profiling</div>
						</li> -->
					</ul>
				</div>
			</div>
		</div>
		<div class="row" id="filter">
			<div class="col s12 m12 l12">
				<div class="card-panel">
					<div class="row">
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Bulan</p>
							<select id="month">
								<?php
									$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
									for($bulan=1; $bulan<=12; $bulan++){
										if($month == $bulan){
											$select = "selected";
										}else{
											$select = "";
										}
										if($bulan<=9) {
											echo "<option $select value='0$bulan'>$bln[$bulan]</option>"; 
										}else{ 
											echo "<option $select value='$bulan'>$bln[$bulan]</option>"; 
										}
									}
								?>
							</select>
						</div>
						<div class="input-field col s12 m5">
							<p style="margin-top:0px">Pilih Tahun</p>
							<select id="year">
								<?php
								$mulai= date('Y') - 50;
								for($i = $mulai;$i<$mulai + 100;$i++){
									$sel = $i == $year ? ' selected="selected"' : '';
									echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
								}
								?>
							</select>
						</div>

						<div class="input-field col s12 m2">
							<button class="btn btn-flat blue white-text" onClick="showWellness()">Filter</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12 m12 l6 card">
			<div id="responsive-table">
				<div class="row">
					<div class="col s12 m12 16" id="DataWellness">
						<table class="responsive-table striped">
							<thead>
								<tr>
									<td data-field="no" class="blue white-text">No</td>
									<td data-field="tanggal" class="blue white-text">Tanggal</td>
									<td data-field="lengthTidur" class="blue white-text">Lama Tidur</td>
									<td data-field="QTidur" class="blue white-text">Kwt Tidur</td>
									<td data-field="soreness" class="blue white-text">Soreness</td>
									<td data-field="fatigue" class="blue white-text">Fatigue</td>
									<td class="blue white-text">Mood</td>
									<td class="blue white-text">Stress</td>
									<td class="blue white-text">Fokus</td>
									<td class="blue white-text">Jml Nutrisi</td>
									<td class="blue white-text">Kwt Nutrisi</td>
									<td class="blue white-text">Hidrasi</td>
									<td class="blue white-text">BB</td>
									<td class="blue white-text">RHR</td>
									<td class="blue white-text">Cidera</td>
									<td class="blue white-text">Total</td>
								</tr>
							</thead>
							<tbody id="wellness">
								<?php echo $dataTable ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 16" id="DataWellness2">
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
<script>
	function showFilter() {
        $("#filter").toggle(); 
    }
	
	function showWellness(){
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		var month 	= $("#month").val();
		var year 	= $("#year").val();
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/member/showFilter',
			data : {month:month,year:year},
			success: function(data){
				$("#DataWellness").html(data);
				$("#DataWellness2").html("");
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});	
	}
	
	function showGrafik(){
		var month 	= $("#month").val();
		var year 		= $("#year").val();
		
		$("#DataWellness").html("<div class='progress'><div class='indeterminate'></div></div>");
		
		var options = {
			chart: {
				renderTo: 'DataWellness',
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				}
			},
			// colors: ["#c62828","#2e7d32","#2196f3"],
			title: {
				text: 'Wellness Physiology Score'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			xAxis: {
				categories: [],
				crosshair: true
			},
			yAxis: [
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				},
				{
					min: 0,
					max: 5,
					allowDecimals: false,
					title: {
						text: ''
					}
				}
			],
			series: [
				{
					showInLegend :true,
					type: 'column',
					name: 'RHR',
					data: [{}],
					color: "#ffeb3b",
				},
				{
					showInLegend :true,
					type: 'column',
					name: 'BB',
					data: [{}],
					color: "#0d47a1",
				},
				// {
					// type: 'column',
					// margin: [ 50, 50, 100, 80],
					// name: 'BB',
					// data: [{}],
					// colors: ["#48485A"],
					// legend: {
						// enabled: true
					// }
				// },
				{
					showInLegend :true,
					type: 'spline',
					name: 'Fatigue',
					data: [{}],
					color: "#c62828",
					allowPointSelect: true,
					yAxis: 1
				},
				{
					showInLegend :true,
					type: 'spline',
					name: 'Soreness',
					data: [{}],
					color: "#2e7d32",
					allowPointSelect: true,
					yAxis: 1
				},
				{
					showInLegend :true,
					type: 'spline',
					name: 'Hidrasi',
					color: "#2196f3",
					data: [{}],
					allowPointSelect: true,
					yAxis: 1
				},
			]
		};
		
		var optionsPsiko = {
			chart: {
				renderTo: 'DataWellness2',
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				},
				spline: {
					dataLabels: {
						enabled: true,
						color: '#000000',
						style: {
							fontWeight: 'bold'
						}
					}
				}
			},
			// colors: [],
			title: {
				text: 'Wellness Psychology Score'
			},
			navigation: {
				buttonOptions: {
					align: "right",
					enabled: false,
				},
			},
			xAxis: {
				categories: [],
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: [
				{
					allowDecimals: false,
					title: {
						text: ''
					}
				},
				{
					min: 0,
					max: 5,
					allowDecimals: false,
					title: {
						text: ''
					}
				}
			],
			series: [
				{
					type: 'column',
					name: 'Wellness',
					data: [{}],
					color: "#2196F3",
					legend: {
						enabled: true
					}
				},
				{
					type: 'spline',
					name: 'Mood',
					data: [{}],					
					yAxis: 1,
					color: "#2e7d32",
					legend: {
						enabled: true
					}
				},
				{
					type: 'spline',
					name: 'Stress',
					data: [{}],
					color: '#c62828',
					legend: {
						enabled: true
					}, 
					yAxis: 1
				},
				{
					type: 'spline',
					name: 'Focus',
					data: [{}],
					color: '#ffeb3b',
					legend: {
						enabled: true
					},
					yAxis: 1
				},
			]
		};
		
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url();?>index.php/member/viewGrafik',
			data : {month:month,year:year},
			dataType: "json",
			success: function(data){
				options.xAxis.categories = data.categories;
				// options.colors = data.colors;
				options.series[0].data = data.nadi;
				options.series[1].data = data.berat;
				options.series[2].data = data.fatigue;
				options.series[3].data = data.soreness;
				options.series[4].data = data.hidrasi;
				
				optionsPsiko.xAxis.categories = data.categories;
				// options.colors = data.colors;
				optionsPsiko.series[0].data = data.wellness;
				optionsPsiko.series[1].data = data.mood;
				optionsPsiko.series[2].data = data.stress;
				optionsPsiko.series[3].data = data.focus;
				var chart 		= new Highcharts.Chart(options);
				var chart 		= new Highcharts.Chart(optionsPsiko);
			},error: function(xhr, ajaxOptions, thrownError){            
				alert(xhr.responseText);
			}
		});
	}
</script>
	