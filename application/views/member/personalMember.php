<?php 
	$now = date("Y-m-d");
	
	if($wellness_date == $now){			
		if($value_wellness <= 59){
			$wellness = "#FF0000";
		}elseif($value_wellness >= 60 && $value_wellness <= 69) {
			$wellness = "#FF9D00";
		}elseif($value_wellness >= 70 && $value_wellness <= 79){
			$wellness = "#E1FF00";
		}elseif($value_wellness >= 80 && $value_wellness <= 89){
			$wellness = "#9BFF77";
		}else{
			$wellness = "#00CE25";
		}	
	}else{
		$wellness = "#607D8B";
	}
	
	$event = "";
	$alamat = "";
	$telp = "";
	$handphone = "";
	$email = "";
	$tempat_lahir = "";
	$tgl_lahir = "";
	$agama = "";
	$tinggi_badan = "";
	$golongan_darah = "";
	$pendidikan = "";
	$pekerjaan = "";
	$status = "";
	$jenis_kelamin = "";
	$optstatus = "";
	$optkelamin = "";
	$kaos = "";
	$jaket = "";
	$celana = "";
	$sepatu = "";
	if($personal){
		foreach($personal as $personal){
			$event = $personal->master_atlet_nomor_event;
			$alamat = $personal->master_atlet_address;
			$telp = $personal->master_atlet_telp;
			$handphone = $personal->master_atlet_handphone;
			$email = $personal->master_atlet_email;
			$tempat_lahir = $personal->master_atlet_tempat_lahir;
			$tgl_lahir = $personal->master_atlet_tanggal_lahir;
			$agama = $personal->master_atlet_agama;
			$tinggi_badan = $personal->master_atlet_tinggi_badan;
			$golongan_darah = $personal->master_atlet_golongan_darah;
			$pendidikan = $personal->master_atlet_pendidikan;
			$pekerjaan = $personal->master_atlet_pekerjaan;
			$status = $personal->master_atlet_status;
			$jenis_kelamin = $personal->master_atlet_jenis_kelamin;
			$kaos = $personal->kaos;
			$jaket = $personal->jaket;
			$celana = $personal->celana;
			$sepatu = $personal->sepatu;
		}
	}
		
	if($jenis_kelamin == "laki"){
		$optkelamin = "Laki Laki";
	}else if($jenis_kelamin == "perempuan"){
		$optkelamin = "Perempuan";
	}else{
		$optkelamin = "";
	}
	
	
	if($status == "menikah"){
		$optstatus = "Menikah";
	}else if($status == "single"){
		$optstatus = "Belum Menikah";
	}else{
		$optstatus = "";
	}
?>
<?php 
	$orangtua = "";
	$pasangan = "";
	$anak	= "";
	$nomordll	= "";
	if($keluarga){
		foreach($keluarga as $keluarga){ 
			$orangtua = $keluarga->master_atlet_keluarga_orangtua;
			$pasangan = $keluarga->master_atlet_keluarga_pasangan;
			$anak	  = $keluarga->master_atlet_keluarga_anak;
			$nomordll = $keluarga->master_atlet_keluarga_nomor_lain;
		}
	}
?>
<?php 
	$cedera = "";
	$alergi = "";
	$lemak  = "";
	if($health){
		foreach($health as $health){ 
			$cedera = $health->cedera;
			$alergi = $health->alergi;
			$lemak  = $health->lemak;
		}
	}
?>
<?php 
	$nama_club = "";
	$alamat_club = "";
	$email_club  = "";
	$prestasi  = "";
	if($club){
		foreach($club as $club){ 
			$nama_club		= $club->club;
			$alamat_club 	= $club->alamat_club;
			$email_club 	= $club->email_club;
			$prestasi 		= $club->prestasi;
		}
	}
?>
<section id="content">
	<!--start container-->
	<div class="container">
		<div class="col s12 m12 l6">
			<div id="flight-card" class="card">
				<div class="card-header sea-games">
					<div class="card-title">
						<div class="col s5 m5 l5 center-align">
							<span class="card-title activator grey-text text-darken-4"><i class="mdi-navigation-more-vert right"></i></span>
						</div>
						<div class="col s5 m5 l5 center-align">
							<img style="border: 5px solid <?php echo $wellness ?>" src="<?php echo $gambar_atl?>" width="100px" height="100px" class="circle z-depth-1 responsive-img activator">
						</div>
						<div class="col s5 m5 l5 center-align">
							<h5 class="flight-card-title black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $nama_user?></h5>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;"><?php echo $group_name ?> - <?php echo $no_event ?></p>
							<p class="flight-card-date black-text" style="text-shadow: 1px 1px 1px #ffffff;">Personal Information</p>
						</div>
					</div>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i></span><br>
					<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
						<a href="<?php echo base_url()?>index.php/member/personalInformation">
							<li><div class="collapsible-header"><i class="mdi-action-assignment-ind"></i> Personal Information</div></li>
						</a>
						<a href="<?php echo base_url()?>index.php/member/WellnessInformation">
							<li>
								<div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/wellness_black.png"/></i> Wellness</div>
							</li>
						</a>
<!-- 						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/monotony_black.png"/></i> Training Load</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/pmc_black.png"/></i> PMC</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/meal_black.png"/></i>  Meal Plan</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/recovery_black.png"/></i>  Recovery</div></li>
						<li><div class="collapsible-header"><i><img style="margin-top:5px" width="20px" src="<?php echo base_url()?>assets/icon/chart_black.png"/></i>  Performance Profiling</div></li> -->
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
		  <div class="col s12 m8 l9">
			<div class="row">
				<div class="col s12">
					<ul class="tabs tab-demo-active z-depth-1 cyan">
					  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#basic">Basic</a>
					  </li>
					  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#family">Family</a>
					  </li>
					  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#health">Health</a>
					  </li>
					  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#pb">PB</a>
					  </li>
					  <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#apparel">Apparel</a>
					  </li>
					</ul>
				</div>
				<div class="col s12">
					<div id="basic" class="col s12  lighten-4">
						<div class="col s12 m8 l9"><br>
							<div class="table-responsive">
								<table class="striped">
									<thead><tr><th>Nama Lengkap</th></tr></thead>
									<tbody><tr><td><?php echo $nama_user ?></td></tr></tbody>
									<thead><tr><th>Cabang Olahraga</th></tr></thead>
									<tbody><tr><td><?php echo $group_name ?></td></tr></tbody>
									<thead><tr><th>Nomor Event</th></tr></thead>
									<tbody><tr><td><?php echo $event ?></td></tr></tbody>
									<thead><tr><th>Alamat</th></tr></thead>
									<tbody><tr><td><?php echo $alamat ?></td></tr></tbody>
									<thead><tr><th>Telpon</th></tr></thead>
									<tbody><tr><td><?php echo $telp ?></td></tr></tbody>
									<thead><tr><th>Handphone</th></tr></thead>
									<tbody><tr><td><?php echo $handphone ?></td></tr></tbody>
									<thead><tr><th>Email</th></tr></thead>
									<tbody><tr><td><?php echo $email ?></td></tr></tbody>
									<thead><tr><th>Tempat Lahir</th></tr></thead>
									<tbody><tr><td><?php echo $tempat_lahir ?></td></tr></tbody>
									<thead><tr><th>Agama</th></tr></thead>
									<tbody><tr><td><?php echo $agama ?></td></tr></tbody>
									<thead><tr><th>Tinggi Badan</th></tr></thead>
									<tbody><tr><td><?php echo $tinggi_badan ?></td></tr></tbody>
									<thead><tr><th>Golongan Darah</th></tr></thead>
									<tbody><tr><td><?php echo $golongan_darah ?></td></tr></tbody>
									<thead><tr><th>Jenis Kelamin</th></tr></thead>
									<tbody><tr><td><?php echo $optkelamin ?></td></tr></tbody>
									<thead><tr><th>Pendidikan</th></tr></thead>
									<tbody><tr><td><?php echo $pendidikan ?></td></tr></tbody>
									<thead><tr><th>Pekerjaan</th></tr></thead>
									<tbody><tr><td><?php echo $pekerjaan ?></td></tr></tbody>
									<thead><tr><th>Status</th></tr></thead>
									<tbody><tr><td><?php echo $optstatus ?></td></tr></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="family" class="col s12  lighten-4">
						<div class="col s12 m8 l9">
							<div class="table-responsive">
								<table class="striped">
									<thead><tr><th>Nama Orang Tua</th></tr></thead>
									<tbody><tr><td><?php echo $orangtua ?></td></tr></tbody>
									<thead><tr><th>Nama Suami/Istri</th></tr></thead>
									<tbody><tr><td><?php echo $pasangan ?></td></tr></tbody>
									<thead><tr><th>Nama Anak</th></tr></thead>
									<tbody><tr><td><?php echo $anak ?></td></tr></tbody>
									<thead><tr><th>Nama / Nomor kontak yang bisa dihubungi apabila terjadi sesuatu :</th></tr></thead>
									<tbody><tr><td><?php echo $nomordll ?></td></tr></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="health" class="col s12  lighten-4">
						<div class="col s12 m8 l9">
							<div class="table-responsive">
								<table class="striped">
									<thead><tr><th>Cedera Yang Pernah Dialami</th></tr></thead>
									<tbody><tr><td><?php echo $cedera ?></td></tr></tbody>
									<thead><tr><th>Alergi</th></tr></thead>
									<tbody><tr><td><?php echo $alergi ?></td></tr></tbody>
									<thead><tr><th>Presentasi Lemak Tubuh</th></tr></thead>
									<tbody><tr><td><?php echo $lemak ?></td></tr></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="pb" class="col s12  lighten-4">
						<div class="col s12 m8 l9">
							<div class="table-responsive">
								<table class="striped">
									<thead><tr><th>PB/PP</th></tr></thead>
									<tbody><tr><td><?php echo $nama_club ?></td></tr></tbody>
									<thead><tr><th>Alamat PB/PP</th></tr></thead>
									<tbody><tr><td><?php echo $alamat_club ?></td></tr></tbody>
									<thead><tr><th>Email PB/PP</th></tr></thead>
									<tbody><tr><td><?php echo $email_club ?></td></tr></tbody>
									<thead><tr><th>Prestasi Terakhir</th></tr></thead>
									<tbody><tr><td><?php echo $prestasi ?></td></tr></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="apparel" class="col s12  lighten-4">
						<div class="col s12 m8 l9">
							<div class="table-responsive">
								<table class="striped">
									<thead><tr><th>Ukuran Kaos</th></tr></thead>
									<tbody><tr><td><?php echo $kaos ?></td></tr></tbody>
									<thead><tr><th>Ukuran Jaket</th></tr></thead>
									<tbody><tr><td><?php echo $jaket ?></td></tr></tbody>
									<thead><tr><th>Ukuran Celana</th></tr></thead>
									<tbody><tr><td><?php echo $celana ?></td></tr></tbody>
									<thead><tr><th>Ukuran Sepatu</th></tr></thead>
									<tbody><tr><td><?php echo $sepatu ?></td></tr></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
	</div>
</section>