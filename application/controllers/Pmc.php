<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Pmc extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_pmc');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		$this->ModelActivityUser->setActivityUser($username,6,13);
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($role_type == "CHC"){
			redirect("pmc/set_atlet");
		}
		if($role_type == "ATL"){
			redirect("pmc/listPMC/".$username);
		}
		if($role_type == "KSC"){
			redirect("pmc/setGroup");
		}
		if($role_type == "SSC"){
			redirect("pmc/setGroup");
		}
		if($role_type == "PRIMA"){
			redirect("pmc/setGroup");
		}
		if($role_type == "RCV"){
			redirect("pmc/setGroup");
		}
		if($role_type == "MLP"){
			redirect("pmc/setGroup");
		}
		if($role_type == "HPD"){
			redirect("pmc/setGroup");
		}
		if($role_type == "SATLAK"){
			redirect("pmc/setGroup");
		}
		if($role_type == "PSY"){
			redirect("pmc/setGroup");
		}
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
				
		$this->load->model('m_home');
		$data['group'] = $this->m_home->getGroup($username);
		
		$data["page"] = "pmc/setGroup";
		$this->load->view("layout",$data);
	}
	
	function set_atlet($group_id = NULL){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$this->session->set_userdata("group_id",$group_id);
		
		$this->load->model('m_home');
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		$data["group_id"]	= $group_id;
		
		$data["page"] = "pmc/set_atlet";
		$this->load->view("layout",$data);
	}
	
	function searchAtlet() {
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
			
		$name 		= $this->input->post('input_data');
		$group_id	= $this->input->post('group_id');
		
		$query = $this->m_pmc->searchAtlet($name,$group_id);
			
			// print_r($query);
			// return;
			
			if($query){
				$list = "<ul class='collection'>";
				foreach ($query as $row){
					$name 		= $row->name;
					$gambar		= $row->gambar;
					$group		= $row->master_group_name;
					$event		= $row->master_atlet_nomor_event;
					$atlet		= $row->username;
					$wellness	= $row->value_wellness;
					$wellness_date	= $row->wellness_date;
					$dttm		= date("Y-m-d");
					
					$query = $this->db->query(" SELECT cidera FROM master_kondisi WHERE username='$atlet'"
											. " AND created_dttm >= '$dttm 00:00:00' AND created_dttm <= '$dttm 23:59:59'");
					if($query->num_rows()>0){
						$row 	= $query->row();
						$cidera = $row->cidera;
						if($cidera == ""){
							$cidera = "-";
						}
					}else{
						$cidera	= "-";
					}
					
					if($wellness_date == $dttm){			
						if($wellness <= 59){
							$btn = "#FF0000";
						}elseif($wellness >= 60 && $wellness <= 69) {
							$btn = "#FF9D00";
						}elseif($wellness >= 70 && $wellness <= 79){
							$btn = "#E1FF00";
						}elseif($wellness >= 80 && $wellness <= 89){
							$btn = "#9BFF77";
						}else{
							$btn = "#00CE25";
						}				
					}else{
						$btn = "#607D8B";
					}
					$list .= '
						<a href="'.base_url().'index.php/pmc/listPmc/'.$atlet.'" class="content"><li class="collection-item avatar">
							<img src="'.$gambar.'" alt="" class="circle">
							<span class="title">'.$name.'</span>
							<p>'.$group.' |  <i style="text-align:right">'.$event.'</i></p>
							<p>Cidera : '.$cidera.'</p>
							<a href="#!" class="secondary-content"><i style="color: '.$btn.'" class="mdi-action-favorite"></i></a>
						</li></a>
					';
				}
				$list .="</ul>";				
			}else{
				$list = '<ul class="collection"><li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
			}
			
			echo $list;
    }
	
	function listPMC($atlet){
		$this->session->set_userdata("atlet",$atlet);
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		if($this->session->userdata("year")){
			$year	= $this->session->userdata("year");
		}else{
			$year	= date("Y");
		}
		
		if($this->session->userdata("month")){
			$month	= $this->session->userdata("month");
		}else{
			$month	= date("m");
		}
		
		$data["year"] 		= $year;
		$data["month"] 		= $month;
		$data['group_id']	= $this->session->userdata("group_id");
		$data["pmc"] 		= $this->m_pmc->getMonotony($atlet,$year,$month);
		
		$data["page"] = "pmc/pmc_list";
		$this->load->view("layout",$data);
	}
	
	function pmcData($monotony_id){
		$atlet = $this->session->userdata("atlet");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["monotony_id"] = $monotony_id;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		$data['group_id']	= $this->session->userdata("group_id");
		$data['pmcData'] 	= $this->m_pmc->getPmcData($monotony_id);
		
		$data["page"] = "pmc/pmc_data";
		$this->load->view("layout",$data);
	}
	
	function setPMC($group_id){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		if($this->session->userdata("atlet")){
			$atlet_id	= $this->session->userdata("atlet");
		}else{
			$atlet_id 	= "";
		}
		
		if($this->session->userdata("year")){
			$year	= $this->session->userdata("year");
		}else{
			$year	= date("Y");
		}

		$data["year"] = $year;		
		$data['atletID']	= $atlet_id;
		$this->load->model("m_home");
		$data['atlet'] 		= $this->m_home->getAtlet($group_id);
		
		$data["page"] = "pmc/setPMC";
		$this->load->view("layout",$data);
	}
	
	function getPMCAtlet(){
		$atlet		= $_POST["atlet"];
		$year		= $_POST["year"];
		$pmc 		= $this->m_pmc->getMonotony($atlet,$year,"all");
		
		$ret = "";
		if($pmc){
			foreach ($pmc as $key) {
				$date 			= $key->mdlmonotony_date;
				$monotony_id 	= $key->distribute_monotony;
				$vdt = date("Y-m-d",strtotime($date));
				$date = date("d M Y H:i:s",strtotime($date));
				$ret .= "
					<option value='$monotony_id'>$date</option>
				";
			}
		}else{
			$ret .= "<option>Belum Ada Training Load</option>";
		}
		
		echo $ret;
	}
	
	function getPMCData(){
		
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		
		$monotony_id	= $_POST["monotonyID"];
		
		$pmcData = $this->m_pmc->getPmcData($monotony_id);
		
		$ret = "";
		if($pmcData){
			$tmpDate = 1;
			$ret .= '<table class="striped">';
			foreach($pmcData as $key){
				$detail_id			= $key->detail_id;
				$detail_date		= $key->detail_date;
				$detailSession		= $key->detail_session;
				$detailVolume		= $key->detail_volume;
				$detail_actual		= $key->detail_actual;
				$detailIntensity	= $key->detail_intensity;
				$detail_warmUpVolume	= $key->detail_warmUpVolume;
				$detail_warmUpActual	= $key->detail_warmUpActual;
				$detail_cooldownVolume	= $key->detail_cooldownVolume;
				$detail_cooldownActual	= $key->detail_cooldownActual;
				$detailVolumeTechnic		= $key->detail_volumeTechnic;
				$detail_actualTechnic		= $key->detail_actualTechnic;
				$detail_warmUpVolumeTechnic	= $key->detail_warmUpVolumeTechnic;
				$detail_warmUpActualTechnic	= $key->detail_warmUpActualTechnic;
				$detail_coolDownVolumeTechnic	= $key->detail_coolDownVolumeTechnic;
				$detail_coolDownActualTechnic	= $key->detail_coolDownActualTechnic;
				
				if($detailVolumeTechnic == "" OR $detailVolumeTechnic == 0){
					$detailVolumeTechnic = $detailVolume;
				}
				
				$daySet = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
				
				$day	= date("w", strtotime($detail_date));
				
				$hari		= $daySet[$day];
				$tangal	= date("d M Y", strtotime($detail_date));
				
				if($tmpDate<>$detail_date){
					$ret .='
						<thead><tr><th colspan="7" class="indigo white-text"><h6><b>'.$hari.', '.$tangal.'</b></h6></th></tr></thead>
					';
					$tmpDate = $detail_date;
				}
				
				$sql = "SELECT * FROM mdlmonotony_intensity order by intensity_id";
				$query = $this->db->query($sql);
				$result = $query->result();
				if($result){
					$opt = "<select class='form-control' name='actualPhysic[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_actual == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$opt .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$opt .="</select>";
					
					$optWarmup = "<select class='form-control' name='actualPhysicWarmUp[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_warmUpActual == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$optWarmup .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$optWarmup .="</select>";
					
					$optCooldown = "<select class='form-control' name='actualPhysicCoolDown[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_cooldownActual == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$optCooldown .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$optCooldown .="</select>";
					
					$optTechnic = "<select class='form-control' name='actualTechnic[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_actualTechnic == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$optTechnic .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$optTechnic .="</select>";
					
					$optWarmupTechnic = "<select class='form-control' name='actualWarmUpTechnic[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_warmUpActualTechnic == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$optWarmupTechnic .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$optWarmupTechnic .="</select>";
					
					$optCooldownTechnic = "<select class='form-control' name='actualCoolDownTechnic[]' style='width:50px'>";
						foreach ($result as $key) {
							$actual = $key->intensity_id;
							$ket = $key->intensity_name;
							if($detail_coolDownActualTechnic == $actual){
								$slc = "selected";
							}else{
								$slc = "";
							}
							$optCooldownTechnic .= "<option $slc value='$actual'>$actual - $ket</option>";
						}
					$optCooldownTechnic .="</select>";
				}else{
					$opt="";
					$optWarmup="";
					$optCooldown="";
					$optTechnic="";
					$optWarmupTechnic="";
					$optCooldownTechnic="";
				};
				
				if($role_type == "KSC"){
					$btnVolumePhysic 			= '<input type="number" name="volumePhysic[]" style="width:50px" value="'.$detailVolume.'"/>';
					$btnVolumeWarmUpPhysic 		= '<input type="number" name="volumePhysicWarmUp[]" style="width:50px" value="'.$detail_warmUpVolume.'"/>';
					$btnVolumeCoolDownPhysic 	= '<input type="number" name="volumePhysicCoolDown[]" style="width:50px" value="'.$detail_cooldownVolume.'"/>';
					$btnActualPhysic			= $opt;
					$btnActualPhysicWarmUp		= $optWarmup;
					$btnActualPhysicCoolDown	= $optCooldown;
				}else{					
					$btnVolumePhysic 			= $detailVolume;
					$btnVolumeWarmUpPhysic 		= $detail_warmUpVolume;
					$btnVolumeCoolDownPhysic 	= $detail_cooldownVolume;
					$btnActualPhysic			= $detail_actual;
					$btnActualPhysicWarmUp		= $detail_warmUpActual;
					$btnActualPhysicCoolDown	= $detail_cooldownActual;
				}
				
				if($role_type == "CHC"){
					$btnVolumeTechnic 			= '<input type="number" name="volumeTechnic[]" style="width:50px" value="'.$detailVolumeTechnic.'"/>';
					$btnVolumeWarmUpTechnic		= '<input type="number" name="volumeWarmUpTechnic[]" style="width:50px" value="'.$detail_warmUpVolumeTechnic.'"/>';
					$btnVolumeCoolDownTechnic	= '<input type="number" name="volumeCoolDownTechnic[]" style="width:50px" value="'.$detail_coolDownVolumeTechnic.'"/>';
					$btnActualTechnic			= $optTechnic;
					$btnActualTechnicWarmUp		= $optWarmupTechnic;
					$btnActualTechnicCoolDown	= $optCooldownTechnic;
				}else{					
					$btnVolumeTechnic			= $detailVolumeTechnic;
					$btnVolumeWarmUpTechnic		= $detail_warmUpVolumeTechnic;
					$btnVolumeCoolDownTechnic	= $detail_coolDownVolumeTechnic;
					$btnActualTechnic			= $detail_actualTechnic;
					$btnActualTechnicWarmUp		= $detail_warmUpActualTechnic;
					$btnActualTechnicCoolDown	= $detail_coolDownActualTechnic;
				}
				
				$ret .='<tbody>'
					 . '<input type="hidden" name="detail_id[]" value="'.$detail_id.'"/>'
					 . '<tr><th colspan="7" class="red white-text"><h6><b>Session Name : '.$detailSession.'</b></h6></th></tr>'
					 // . '<tr><td colspan="7">'.$detailSession.'</td></tr>'
					 . '<tr><th colspan="2"><h6><b>Physic</b></h6></th><th colspan="2"><h6><b>Technic</b></h6></th></tr>'
					 . '<tr><td colspan="2"><b>Warm Up</b></td><td colspan="2"><b>Warm Up</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$btnVolumeWarmUpPhysic.'</td><td>'.$btnActualPhysicWarmUp.'</td><td>'.$btnVolumeWarmUpTechnic.'</td><td>'.$btnActualTechnicWarmUp.'</td></tr>'
					 . '<tr><td colspan="2"><b>Primary Training</b></td><td colspan="2"><b>Primary Training</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$btnVolumePhysic.'</td><td>'.$btnActualPhysic.'</td><td>'.$btnVolumeTechnic.'</td><td>'.$btnActualTechnic.'</td></tr>'
					 . '<tr><td colspan="2"><b>Cool Down</b></td><td colspan="2"><b>Cool Down</b></td></tr>'
					 . '<tr><td>Vol/Time</td><td>Actual</td><td>Vol/Time</td><td>Actual</td></tr>'
					 . '<tr><td>'.$btnVolumeCoolDownPhysic.'</td><td>'.$btnActualPhysicCoolDown.'</td><td>'.$btnVolumeCoolDownTechnic.'</td><td>'.$btnActualTechnicCoolDown.'</td></tr>'
					 . '</tbody>';
				
			}
			$ret .="</table>";
		}else{
			$ret .="Data Tidak Ditemukan";
		}
		
		echo $ret;
		
	}
	
	function saveDataPMC(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$monotonyID				= $_POST["monotonyID"];
		$detail_id				= $_POST["detail_id"];
		$volumePhysic			= $_POST["volumePhysic"];
		$volumePhysicWarmUp		= $_POST["volumePhysicWarmUp"];
		$volumePhysicCoolDown	= $_POST["volumePhysicCoolDown"];
		$actualPhysic			= $_POST["actualPhysic"];
		$actualPhysicWarmUp		= $_POST["actualPhysicWarmUp"];
		$actualPhysicCoolDown	= $_POST["actualPhysicCoolDown"];
		
		for($i=0; $i<count($detail_id); $i++) {
			$sql	= " UPDATE mdlmonotony_detail SET detail_volume = '{$volumePhysic[$i]}', detail_actual = '{$actualPhysic[$i]}',"
					. " detail_warmUpVolume = '{$volumePhysicWarmUp[$i]}', detail_warmUpActual = '{$actualPhysicWarmUp[$i]}',"
					. " detail_cooldownVolume = '{$volumePhysicCoolDown[$i]}', detail_cooldownActual = '{$actualPhysicCoolDown[$i]}'"
					. " WHERE detail_id = '{$detail_id[$i]}'";
			$query	= $this->db->query($sql);
		}
		
		if($query){
			$this->ModelActivityUser->setActivityUser($username,3,13);
			echo "sukses";
		}else{
			$this->ModelActivityUser->setActivityUser($username,11,13);
			echo "gagal";
		}
		return;
	}
	
	function saveDataPmcTechnic(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$monotonyID				= $_POST["monotonyID"];
		$detail_id				= $_POST["detail_id"];
		$volumeTechnic			= $_POST["volumeTechnic"];
		$volumeWarmUpTechnic	= $_POST["volumeWarmUpTechnic"];
		$volumeCoolDownTechnic	= $_POST["volumeCoolDownTechnic"];
		$actualTechnic			= $_POST["actualTechnic"];
		$actualWarmUpTechnic	= $_POST["actualWarmUpTechnic"];
		$actualCoolDownTechnic	= $_POST["actualCoolDownTechnic"];
		
		for($i=0; $i<count($detail_id); $i++) {
			$sql	= " UPDATE mdlmonotony_detail SET detail_volumeTechnic = '{$volumeTechnic[$i]}', detail_actualTechnic = '{$actualTechnic[$i]}',"
					. " detail_warmUpVolumeTechnic = '{$volumeWarmUpTechnic[$i]}', detail_warmUpActualTechnic = '{$actualWarmUpTechnic[$i]}',"
					. " detail_coolDownVolumeTechnic = '{$volumeCoolDownTechnic[$i]}', detail_coolDownActualTechnic = '{$actualCoolDownTechnic[$i]}'"
					. " WHERE detail_id = '{$detail_id[$i]}'";
			$query	= $this->db->query($sql);
		}
		
		if($query){
			$this->ModelActivityUser->setActivityUser($username,3,13);
			echo "sukses";
		}else{
			$this->ModelActivityUser->setActivityUser($username,11,13);
			echo "gagal";
		}
		return;
	}
	
	function showPMCYearWeek(){
		$atlet	= $this->session->userdata("atlet");
		$year	= $_POST["year"];
		
		$pmc	= $this->m_pmc->getPmcYear($year,$atlet);
		
		if($pmc){
			$tmpDate = 1;
			foreach($pmc as $row){
				$detail_id			= $row->detail_id;
				$detail_date		= $row->detail_date;
				$detail_intensity	= $row->detail_intensity;
				
				//PMC Physic
				$durasiInti			= $row->detail_volume;
				$durasiWarmUp		= $row->detail_warmUpVolume;
				$durasiCooldown		= $row->detail_cooldownVolume;
				$rpeInti			= $row->detail_actual;
				$rpeWarmUp			= $row->detail_warmUpActual;
				$rpeCooldown		= $row->detail_cooldownActual;
				$TssHourInti		= $this->getTssHour($rpeInti);
				$TssHourWarmUp		= $this->getTssHour($rpeWarmUp);
				$TssHourCooldown	= $this->getTssHour($rpeCooldown);
				
				$warmUp				= ($durasiWarmUp/60)*$TssHourWarmUp;
				$training			= ($durasiInti/60)*$TssHourInti;
				$coolDown			= ($durasiCooldown/60)*$TssHourCooldown;
				
				
				//PMC Technic
				$durasiIntiTechnic		= $row->detail_volumeTechnic;
				$durasiWarmUpTehnic		= $row->detail_warmUpVolumeTechnic;
				$durasiCooldownTechnic	= $row->detail_coolDownVolumeTechnic;
				$rpeIntiTechnic			= $row->detail_actualTechnic;
				$rpeWarmUpTechnic		= $row->detail_warmUpActualTechnic;
				$rpeCooldownTechnic		= $row->detail_coolDownActualTechnic;
				$TssHourIntiTechnic		= $this->getTssHour($rpeIntiTechnic);
				$TssHourWarmUpTechnic	= $this->getTssHour($rpeWarmUpTechnic);
				$TssHourCooldownTechnic	= $this->getTssHour($rpeCooldownTechnic);
				
				$warmUpTechnic		= ($durasiWarmUpTehnic/60)*$TssHourWarmUpTechnic;
				$trainingTechnic	= ($durasiIntiTechnic/60)*$TssHourIntiTechnic;
				$coolDownTechnic	= ($durasiCooldownTechnic/60)*$TssHourCooldownTechnic;
				
				
				$TotalTss[$detail_date][$detail_id]	= ($warmUp+$training+$coolDown)+($warmUpTechnic+$trainingTechnic+$coolDownTechnic);
				
				// if($tmpDate<>$detail_date){
					// echo array_sum($TotalTss[$detail_date]);
					// $tmpDate = $detail_date;
				// }
			}
		}
	}
	
	function showPMCYear(){
		$atlet	= $this->session->userdata("atlet");
		$year	= $_POST["year"];
		
		$pmc	= $this->m_pmc->getPmcYear($year,$atlet);
		
		if($pmc){
			$tmpDate = 1;
			foreach($pmc as $row){
				$detail_id			= $row->detail_id;
				$detail_date		= $row->detail_date;
				$detail_intensity	= $row->detail_intensity;
				
				//PMC Physic
				$durasiInti			= $row->detail_volume;
				$durasiWarmUp		= $row->detail_warmUpVolume;
				$durasiCooldown		= $row->detail_cooldownVolume;
				$rpeInti			= $row->detail_actual;
				$rpeWarmUp			= $row->detail_warmUpActual;
				$rpeCooldown		= $row->detail_cooldownActual;
				$TssHourInti		= $this->getTssHour($rpeInti);
				$TssHourWarmUp		= $this->getTssHour($rpeWarmUp);
				$TssHourCooldown	= $this->getTssHour($rpeCooldown);
				
				$warmUp				= ($durasiWarmUp/60)*$TssHourWarmUp;
				$training			= ($durasiInti/60)*$TssHourInti;
				$coolDown			= ($durasiCooldown/60)*$TssHourCooldown;
				
				
				//PMC Technic
				$durasiIntiTechnic		= $row->detail_volumeTechnic;
				$durasiWarmUpTehnic		= $row->detail_warmUpVolumeTechnic;
				$durasiCooldownTechnic	= $row->detail_coolDownVolumeTechnic;
				$rpeIntiTechnic			= $row->detail_actualTechnic;
				$rpeWarmUpTechnic		= $row->detail_warmUpActualTechnic;
				$rpeCooldownTechnic		= $row->detail_coolDownActualTechnic;
				$TssHourIntiTechnic		= $this->getTssHour($rpeIntiTechnic);
				$TssHourWarmUpTechnic	= $this->getTssHour($rpeWarmUpTechnic);
				$TssHourCooldownTechnic	= $this->getTssHour($rpeCooldownTechnic);
				
				$warmUpTechnic		= ($durasiWarmUpTehnic/60)*$TssHourWarmUpTechnic;
				$trainingTechnic	= ($durasiIntiTechnic/60)*$TssHourIntiTechnic;
				$coolDownTechnic	= ($durasiCooldownTechnic/60)*$TssHourCooldownTechnic;
				
				
				$TotalTss[$detail_date][$detail_id]	= ($warmUp+$training+$coolDown)+($warmUpTechnic+$trainingTechnic+$coolDownTechnic);
				
				// if($tmpDate<>$detail_date){
					// echo array_sum($TotalTss[$detail_date]);
					// $tmpDate = $detail_date;
				// }
			}
			
			$first = true;
			foreach($TotalTss as $date => $id){
				$dailyTss	= array_sum($id);
				$ATL_exp	= EXP(-1/7);
				$ATL		= 179.61;
				
				$CTL_exp	= EXP(-1/42);
				$CTL		= 62.36;
				
				if ( $first )
				{
					$ATL_Start 	= $ATL;					
					$CTL_Start	= $CTL;
					
					$first 		= false;
				}
				else
				{
					$ATL_Start = $dailyTss*(1-$ATL_exp)+$ATL_Start*$ATL_exp;
					$CTL_Start = $dailyTss*(1-$CTL_exp)+$CTL_Start*$CTL_exp;
					// do something
				}
				
				// $D3*(1-ATL_exp)+ATL_start*ATL_exp
				
				$fatigue	= $dailyTss*(1-$ATL_exp)+$ATL_Start*$ATL_exp;
				$fitness	= $dailyTss*(1-$CTL_exp)+$CTL_Start*$CTL_exp;				
				$tsb		= $fitness-$fatigue;
				
				$arrDate[]		= $date;
				$arrFatigue[]	= (int)$fatigue;
				$arrFitness[]	= (int)$fitness;
				$arrTsb[]		= (int)$tsb;
			}
			
			$data = array('categories'=>$arrDate, 'fatigue'=>$arrFatigue, 'fitness'=>$arrFitness, 'tsb'=>$arrTsb);
			$json = json_encode($data);
			echo $json;
		}
	}
	
	function showPMCMonth(){
		$atlet	= $this->session->userdata("atlet");
		$year	= $_POST["year"];
		$month	= $_POST["month"];
		
		$pmc	= $this->m_pmc->getPmcMonth($year,$month,$atlet);
		
		if($pmc){
			$tmpDate = 1;
			foreach($pmc as $row){
				$detail_id			= $row->detail_id;
				$detail_date		= $row->detail_date;
				$detail_intensity	= $row->detail_intensity;
				
				//PMC Physic
				$durasiInti			= $row->detail_volume;
				$durasiWarmUp		= $row->detail_warmUpVolume;
				$durasiCooldown		= $row->detail_cooldownVolume;
				$rpeInti			= $row->detail_actual;
				$rpeWarmUp			= $row->detail_warmUpActual;
				$rpeCooldown		= $row->detail_cooldownActual;
				$TssHourInti		= $this->getTssHour($rpeInti);
				$TssHourWarmUp		= $this->getTssHour($rpeWarmUp);
				$TssHourCooldown	= $this->getTssHour($rpeCooldown);
				
				$warmUp				= ($durasiWarmUp/60)*$TssHourWarmUp;
				$training			= ($durasiInti/60)*$TssHourInti;
				$coolDown			= ($durasiCooldown/60)*$TssHourCooldown;
				
				
				//PMC Technic
				$durasiIntiTechnic		= $row->detail_volumeTechnic;
				$durasiWarmUpTehnic		= $row->detail_warmUpVolumeTechnic;
				$durasiCooldownTechnic	= $row->detail_coolDownVolumeTechnic;
				$rpeIntiTechnic			= $row->detail_actualTechnic;
				$rpeWarmUpTechnic		= $row->detail_warmUpActualTechnic;
				$rpeCooldownTechnic		= $row->detail_coolDownActualTechnic;
				$TssHourIntiTechnic		= $this->getTssHour($rpeIntiTechnic);
				$TssHourWarmUpTechnic	= $this->getTssHour($rpeWarmUpTechnic);
				$TssHourCooldownTechnic	= $this->getTssHour($rpeCooldownTechnic);
				
				$warmUpTechnic		= ($durasiWarmUpTehnic/60)*$TssHourWarmUpTechnic;
				$trainingTechnic	= ($durasiIntiTechnic/60)*$TssHourIntiTechnic;
				$coolDownTechnic	= ($durasiCooldownTechnic/60)*$TssHourCooldownTechnic;
				
				
				$TotalTss[$detail_date][$detail_id]	= ($warmUp+$training+$coolDown)+($warmUpTechnic+$trainingTechnic+$coolDownTechnic);
				
				// if($tmpDate<>$detail_date){
					// echo array_sum($TotalTss[$detail_date]);
					// $tmpDate = $detail_date;
				// }
			}
			
			$first = true;
			foreach($TotalTss as $date => $id){
				$dailyTss	= array_sum($id);
				$ATL_exp	= EXP(-1/7);
				$ATL		= 179.61;
				
				$CTL_exp	= EXP(-1/42);
				$CTL		= 62.36;
				
				if ( $first )
				{
					$ATL_Start 	= $ATL;					
					$CTL_Start	= $CTL;
					
					$first 		= false;
				}
				else
				{
					$ATL_Start = $dailyTss*(1-$ATL_exp)+$ATL_Start*$ATL_exp;
					$CTL_Start = $dailyTss*(1-$CTL_exp)+$CTL_Start*$CTL_exp;
					// do something
				}
				
				// $D3*(1-ATL_exp)+ATL_start*ATL_exp
				
				$fatigue	= $dailyTss*(1-$ATL_exp)+$ATL_Start*$ATL_exp;
				$fitness	= $dailyTss*(1-$CTL_exp)+$CTL_Start*$CTL_exp;				
				$tsb		= $fitness-$fatigue;
				
				$arrDate[]		= $date;
				$arrFatigue[]	= (int)$fatigue;
				$arrFitness[]	= (int)$fitness;
				$arrTsb[]		= (int)$tsb;
			}
			
			$data = array('categories'=>$arrDate, 'fatigue'=>$arrFatigue, 'fitness'=>$arrFitness, 'tsb'=>$arrTsb);
			$json = json_encode($data);
			echo $json;
			
			// print_r($TotalTss);
		}
	}
	
	function getTssHour($detail_intensity){
		if($detail_intensity == 1){
			$TssHour	= 20;
		}else if($detail_intensity == 2){
			$TssHour	= 30;
		}else if($detail_intensity == 3){
			$TssHour	= 40;
		}else if($detail_intensity == 4){
			$TssHour	= 50;
		}else if($detail_intensity == 5){
			$TssHour	= 60;
		}else if($detail_intensity == 6){
			$TssHour	= 70;
		}else if($detail_intensity == 7){
			$TssHour	= 80;
		}else if($detail_intensity == 8){
			$TssHour	= 100;
		}else if($detail_intensity == 8){
			$TssHour	= 120;
		}else if($detail_intensity == 10){
			$TssHour	= 140;
		}else{
			$TssHour 	= 0;
		}
		
		return $TssHour;
	}
	
	function showFilter(){
		$year	= $_POST["year"];
		$month	= $_POST["month"];
		$atlet	= $this->session->userdata("atlet");
		$this->session->set_userdata("year",$year);
		$monotony = $this->m_pmc->getMonotony($atlet,$year,$month);
		
		$ret = '<tbody>';
		if($monotony){
			foreach ($monotony as $key) {
				$date 			= $key->mdlmonotony_date;
				$monotony_id 	= $key->distribute_monotony;
				$vdt = date("Y-m-d",strtotime($date));
				$date = date("d M Y H:i:s",strtotime($date));
				$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row 	= $query->row();
					$startMonotony 	= $row->startMonotony;
					$endMonotony 	= $row->endMonotony;
					$start			= date("d M Y",strtotime($startMonotony));
					$end			= date("d M Y",strtotime($endMonotony));
				}else{
					$start	= "";
					$end	= "";
				}
				$ret .= "
					<tr>
						<td style='font-weight:bold'><a class='btn-flat btn-brand' href='".base_url()."index.php/pmc/pmcData/$monotony_id''>$start - $end</a></td>
					</tr>
				";
			}
		}else{
			$ret .= "<tr><td>Data Kosong</td></tr>";
		}
		$ret .= '</tbody>';
		
		$list = "<table class='striped'>"
			  . "<thead><tr><th style='font-size:12pt;text-align:center'>Weekly Training Load $year</th></tr></thead>"
			  . "$ret"
			  . "</table>";
			  
		echo $list;
	}
}