<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		$this->ModelActivityUser->setActivityUser($username,6,4);
		
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data["page"] = "home/home";
		$this->load->view("layout",$data);
	}
	
	public function logout(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$this->ModelActivityUser->setActivityUser($username,2,7);
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->unset_userdata('login');
        redirect('home','refresh');
    }
	
	
	//NAMBAHIN YANG DARI GUFRON
	 public function mysql_aes_key($key)
    {
        $new_key = str_repeat(chr(0), 16);
        for($i=0,$len=strlen($key);$i<$len;$i++)
        {
            $new_key[$i%16] = $new_key[$i%16] ^ $key[$i];
        }
        return $new_key;
    }

    public function aes_encrypt($val)
    {
        $key = mysql_aes_key('Ralf_S_Engelschall__trainofthoughts');
        $pad_value = 16-(strlen($val) % 16);
        $val = str_pad($val, (16*(floor(strlen($val) / 16)+1)), chr($pad_value));
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
    }

    public function aes_decrypt($val)
    {
        $key = mysql_aes_key('Ralf_S_Engelschall__trainofthoughts');
        $val = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
        return rtrim($val, "..16");
    }

    public function random_val()
    {
        return substr(md5(uniqid(rand(), true)), 0, 5);
    }
	
		public function getEncription($str)
		{
			initEn();
			//return getEnc($str);
			echo getEnc($str);
		}
	
		public function getDecription()
		{
			$enc = $_GET['x'];
			initEn();
			//return getDec($str);
			echo getDec($enc);
		}
}