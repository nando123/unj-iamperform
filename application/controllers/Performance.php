<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Performance extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_performance');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		// if(!$this->session->userdata("login")){
			// redirect("login/form");
		// }
	}
	
	function updatePerformance(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$this->ModelActivityUser->setActivityUser($username,4,9);
		$d_id = $_POST["d_id"];
		$komponen = $_POST["komponen"];
		$messo = $_POST["messo"];
		$current = $_POST["current"];
		$benchmark = $_POST["benchmark"];
		
		$sql = " UPDATE master_performance_detail SET komponen = '$komponen',"
			 . " benchmark = '$messo', goal='$current', current='$benchmark'"
			 . " WHERE id_performance_detail = '$d_id'";
		$this->db->query($sql);
		echo "<script>window.history.back()</script>";
	}
	
	function deletePerformance($id_performance){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$this->ModelActivityUser->setActivityUser($username,5,9);
		$sql = "DELETE a.*,b.* FROM master_performance as a "
			 . "LEFT JOIN master_performance_detail as b on b.id_performance = a.id_performance "
			 . "WHERE a.id_performance = '$id_performance'";
		$query = $this->db->query($sql);
		
		echo "<script>window.history.back()</script>";
	}
	
	function showDeleteConfirm(){
		$id_performance = $_POST["id_performance"];
		$modal = '
			<form method="post" action="'.base_url().'index.php/performance/updatePerformance">
			<div id="myModal_'.$id_performance.'" class="modalPopup">
				<!-- Modal content -->
                  <div class="modal-content">
                    <p>Yakin Ingin Menghapus Data Ini ?</p>
					<a href="'.base_url().'index.php/performance/deletePerformance/'.$id_performance.'" class="btn-flat red white-text">Delete</a>  
					&nbsp<span onclick="closeModal(\''.$id_performance.'\')" class="btn-flat white-text grey">Cancel</span>
                  </div>
			</div>
			</form>
		';
		echo $modal;
	}
	
	function showPopUp(){
		$d_id = $_POST["d_id"];
		$komponen = $_POST["komponen"];
		$messo = $_POST["messo"];
		$current = $_POST["current"];
		$benchmark = $_POST["benchmark"];
		
		$modal = '
			<form method="post" action="'.base_url().'index.php/performance/updatePerformance">
			<div id="myModal_'.$d_id.'" class="modalPopup">
				<!-- Modal content -->
				<div class="modal-content">
					<div class="row margin">
						<div class="input-field col s12">
							<span>Komponen</span>
							<input value="'.$komponen.'" name="komponen" type="text" required>
							<input value="'.$d_id.'" name="d_id" type="hidden" required>
						</div>
					</div>
					<div class="row margin">
						<div class="input-field col s12"
							<span>Messo</span>
							<input value="'.$messo.'" name="messo" type="number" required>
						</div>
					</div>
					<div class="row margin">
						<div class="input-field col s12">
							<span>Current</span>
							<input value="'.$current.'" name="current" type="number" required>
						</div>
					</div>
					<div class="row margin">
						<div class="input-field col s12">
							<span>Benchmark</span>
							<input value="'.$benchmark.'" name="benchmark" type="number" required>
						</div>
					</div>
					<div class="row margin">
						<button class="btn-flat indigo white-text">Update</button>  &nbsp
						<span class="btn-flat grey" onclick="closeModal(\''.$d_id.'\')">Cancel</span>
					</div>
				</div>
			</div>
			</form>
		';
		echo $modal;
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		$this->ModelActivityUser->setActivityUser($username,6,9);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($role_type == "CHC"){
			redirect("performance/set_atlet");
		}
		if($role_type == "ATL"){
			redirect("performance/data/".$username);
		}
		if($role_type == "KSC"){
			redirect("performance/setGroup");
		}
		if($role_type == "SSC"){
			redirect("performance/setGroup");
		}
		if($role_type == "PRIMA"){
			redirect("performance/setGroup");
		}
		if($role_type == "RCV"){
			redirect("performance/setGroup");
		}
		if($role_type == "MLP"){
			redirect("performance/setGroup");
		}
		if($role_type == "HPD"){
			redirect("performance/setGroup");
		}
		if($role_type == "SATLAK"){
			redirect("performance/setGroup");
		}
		if($role_type == "PSY"){
			redirect("performance/setGroup");
		}
	}
	
	
	
	
	function ChooseAtlet(){
		$session	= $this->session->userdata("login");
		$name			= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$group_id	= $this->session->userdata("group_id");
		$atlet		= $this->session->userdata("atlet");
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["atlet"] = $atlet;
		
		$get = $this->uri->uri_to_assoc(1);
		$set = $get['data'];
		$data['set'] = $set;
		
		$this->load->model('m_home');
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		$data["page"] = "performance/chooseAtlet";
		$this->load->view('layout',$data);
		$this->session->unset_userdata("errorMsg");
	}
	
	function set_performance(){
    $session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$atlet		= $this->session->userdata("atlet");
		$data["atlet"] = $atlet;
         
		$data["page"] = "performance/setPerformance";
		$this->load->view('layout',$data);
    }
	
	function setAtlet2Create(){
		$arrAtlet	= $_POST["atlet"];
		
		$this->session->set_userdata("arrAtlet",$arrAtlet);
		echo "sukses";
	}
	
	function formPerformance(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$atlet		= $this->session->userdata("atlet");
		$data["atlet"] = $atlet;
		
		// $get = $this->uri->uri_to_assoc(1);
		// $atlet = $get['atlet'];			
		$get = $this->uri->uri_to_assoc(1);
		$set = $get['data'];
		$data['set'] = $set;
		
		$group_id = $this->session->userdata("group_id");
		
		$this->load->model('m_home');
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		
		$data["page"] = "performance/formPerformance";
		$this->load->view('layout',$data);
		$this->session->unset_userdata("errorMsg");
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
				
		$this->load->model('m_home');
		$data['group'] = $this->m_home->getGroup($username);
		
		$data["page"] = "performance/setGroup";
		$this->load->view("layout",$data);
	}
	
	function set_atlet($group_id = NULL){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$this->session->set_userdata("group_id",$group_id);
		
		$this->load->model('m_home');
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		
		$data["page"] = "performance/set_atlet";
		$this->load->view("layout",$data);
	}
	
	function data($atlet){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$this->session->set_userdata("atlet",$atlet);
		
		$sql = " SELECT a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
		}
		
		$data['kpf'] 	= $this->m_performance->getKPFAtlet($atlet);
		$data['page']	= 'performance/performance';
		$this->load->view('layout',$data);
	}
	
	function formPerformanceAdd($set,$id_performance){
		if($this->session->userdata('login'))
        {
			$current_dttm = date('Y-m-d H:i:s');
			$atlet 		= $this->session->userdata('atlet');
			$session 	= $this->session->userdata("login");
			$name		= $session["name"];
			$username	= $session["username"];
			$gambar		= $session["gambar"];
			$role_type	= $session["role_type"];
			
			$data["gambar"] 	= $gambar;
			$data["name"] 		= $name;
			$data["username"] 	= $username;
			$data["role_type"] 	= $role_type;
			
			$data['set'] 	= $set;
			$data['atlet'] 	= $atlet;
			$data["dttm"] 	= $current_dttm;
			$data['id_performance'] = $id_performance;
			$data["page"] 	= "performance/formPerformanceAdd";
			$this->load->view('layout',$data);
        }else{
            redirect('login','refresh');   
        }
	}
	
	function saveNewPerformance(){
		$session 	= $this->session->userdata("login");
		$atlet 		= $this->session->userdata('atlet');
		$name			= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$komponen 	= $_POST['komponen'];
		$current 		= $_POST['current'];
		$benchmark 	= $_POST['benchmark'];
		$jenis 			= $_POST['jenis'];
		$option			= $_POST['option'];
		
		$id_performance = $_POST['id_performance'];
		$current_dttm = date('Y-m-d H:i:s');
		
		$data = array($id_performance,$username,$atlet,$current_dttm,$komponen,$current,$benchmark,$jenis,$option);
		$save = $this->m_performance->saveNewperformance($data);
		if($save){
			$this->ModelActivityUser->setActivityUser($username,3,9);
			redirect('performance/data/'.$atlet.'','refresh');
		}else{
			$this->ModelActivityUser->setActivityUser($username,11,9);
			echo "gagal";
		}
	}
	
	function viewTable(){
		$session = $this->session->userdata('login');
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		$atlet = $this->session->userdata("atlet");
		
		$id_performance = $_POST["id_performance"];
		$sql = "select a.* FROM master_performance as a where a.id_performance = '$id_performance'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$row = $query->row();
			$date = $row->created_dttm;
			$jenis = $row->jenis_performance;
			$messo = $row->messo;
		}
		if($role_type == "SCH"){
			$btnadd = "<a data-backdrop='static' data-toggle='modal' href='".base_url()."index.php/atlet/add_detail_performance/$jenis/".$id_performance."'><span class='btn btn-brand'>Tambah</span></a>";
		}else{
			$btnadd = "";
		}
		$sql_phy = " SELECT id_performance_detail as id, id_performance, komponen as komponen,"
			 . " benchmark as benchmark, goal as goal, current as current"
			 . " From master_performance_detail where id_performance = '$id_performance'";
		$query_phy = $this->db->query($sql_phy);
		if($query_phy -> num_rows() > 0){
			$result_phy = $query_phy->result();
			if($result_phy > 0){
				$data = "";
				foreach ($result_phy as $item) {
					// $hasil = ($item->current/$item->benchmark)*100;
					// $result = number_format($hasil, 2, '.', ' ');
					$d_id = $item->id;
					if($role_type == "CHC" AND date("Y-m-d", strtotime($date)) == date("Y-m-d")){
						$btnedit = "<a data-toggle='modal' href='#ui_modal_".$d_id."'>$item->komponen</a>";
					}else{
						$btnedit = "$item->komponen";
					}
					
					$data .="<tr><td>$btnedit</td>"
							."<td>$item->current</td><td>$item->goal</td></tr>";
							
					$modal ='<p><strong>Komponen</strong> <input name="komponen" class="form-control" value="'.$item->komponen.'"/></p>'
						   .'<p><strong>Benchmark</strong> <input type="number" step="any" name="benchmark" class="form-control" value="'.$item->benchmark.'"/></p>'
						   .'<p><strong>Goal</strong> <input type="number" step="any" name="goal" class="form-control" value="'.$item->goal.'"/></p>'
						   .'<p><strong>Achievment</strong> <input type="number" step="any" name="current" class="form-control" value="'.$item->current.'"/></p>';
						   
					$data .='<div aria-hidden="true" class="modal fade" id="ui_modal_'.$d_id.'" role="dialog" tabindex="-1">
									<div class="modal-dialog">
									<form method="post" action="'.base_url().'index.php/atlet/edit_performance">
										<div class="modal-content">
											<div class="modal-heading">
												<a class="modal-close" data-dismiss="modal">×</a>
											</div>
											<div class="modal-inner">
												<div id="itemlist">
													'.$modal.'
												</div>
												<input name="id_performance_detail" type="hidden" value="'.$d_id.'"/>
												<input name="atlet" type="hidden" value="'.$atlet.'"/>
											</div>
											<div class="modal-footer">
												<p class="text-right"><button class="btn btn-flat btn-brand waves-attach" type="submit">Update</button>
												<span class="btn btn-flat btn-red waves-attach" data-dismiss="modal">Cancel</span></p>
											</div>
										</div>
									</form>
									</div>
								</div>';
				}
				$data .="<tr><td colspan='4'>$btnadd</td></tr></tbody></table></div>"
					    ."<br>";
		
				$data .= '
				<div aria-hidden="true" class="modal modal-va-middle fade" id="delete_'.$id_performance.'" role="dialog" tabindex="-1">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-inner">
								<p class="h5 margin-top-sm text-black-hint">Delete ?</p>
							</div>
							<div class="modal-footer">
								<p class="text-right">
									<a class="btn btn-flat btn-brand waves-attach" href="'.base_url().'index.php/home/delete/'.$id_performance.'/'.$jenis.'"">CONFIRM</a>
									<a class="btn btn-flat btn-red waves-attach" data-dismiss="modal">CANCEL</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				';
			}
		}
		
		$table = '
				<div id="grafik_'.$id_performance.'">
					<table class="striped">
						<thead>
							<tr><th class="indigo white-text" colspan="3">Messo '.$messo.'</th></tr>
							<tr>
								<th class="indigo white-text" data-field="id">Komponen</th>
								<th class="indigo white-text" data-field="price">Benchmark</th>
								<th class="indigo white-text" data-field="price">Current</th>
							</tr>
						</thead>
						<tbody>
						'.$data.'
						</tbody>
					</table>
				</div>
			';
		
		echo $table;
		// echo "test";
	}
	
	function viewGrafik(){
		$id_performance = $this->input->post("id_performance");
		
		$perf = $this->m_performance->getKPFAtletID($id_performance);
		
		if($perf){
			list($first,$second)=$perf;
			$result_benchmark 	= "";
			$result_current		= "";
			foreach ($first as $key) {
				$jenis = $key->jenis_performance;
				$atlet = $key->id_user_atlet;
				$catatan = $key->catatan;

				foreach ($second as $item) {
					$komponen 	= $item->komponen;
					$current 		= $item->goal;
					$benchmark 	= $item->current;
					$option			= $item->option;
					if ($benchmark <= 0) $benchmark = 1;
					if ($current <= 0) $current = 1;
					
					if($option == 'ascending'){
						$hasil_current = ($benchmark/$current)*100;
					}else{
						$hasil_current = ($current/$benchmark)*100;
					}
					
					$hasil_benchmark = ($benchmark/$benchmark)*100;
					$result_benchmark = number_format($hasil_benchmark, 2, '.', ' ');
					$result_current = number_format($hasil_current, 2, '.', ' ');
		
					$arrkomponen[] 	= $komponen;
					$arrbenchmark[] = (int)$result_benchmark;
					$arrcurrent[] 	= (int)$result_current;
				}
				
				$data = array('categories'=>$arrkomponen, 'benchmark'=>$arrbenchmark, 'current'=>$arrcurrent);
				$json = json_encode($data);
				echo $json;
			}
		}
	}
	
	public function save_performance() {
		$session 	= $this->session->userdata("login");
		$group_id	= $this->session->userdata("group_id");
		$name			= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$arrAtlet		= $this->session->userdata("arrAtlet");
		
		$type 			= $_POST['type'];
		
		if(!isset($_POST['komponen']) OR !isset($_POST['current']) OR !isset($_POST['benchmark'])){
			$errorMsg	= '
				<span class="btn red col s12">Anda Belum Memasukkan Komponen Penilaian</span>
			';
			
			$this->session->set_userdata("errorMsg",$errorMsg);
			redirect('performance/formPerformance/data/'.$type.'','refresh');
			return;
		}
		
		$komponen 	= $_POST['komponen'];
		$current 		= $_POST['current'];
		$benchmark 	= $_POST['benchmark'];
		$jenis 			= $_POST['jenis'];
		$type 			= $_POST['type'];
		$catatan 		= $_POST['catatan'];
		$messo	 		= $_POST['messo'];
		$option			= $_POST['option'];
		
		$data = array(
			'username' 	=> $username,
			'arrAtlet'	=> $arrAtlet,
			'komponen' 	=> $komponen,
			'messo' 		=> $messo,
			'goal' 			=> $current,
			'current' 	=> $benchmark,
			'jenis' 		=> $jenis,
			'catatan'		=> $catatan,
			'option'		=> $option
		);
		
		$save = $this->m_performance->save_performance($data);
		
		// return;
		if($save){
			$errorMsg	= '
				<span class="btn cyan col s12">Berhasil Membuat Profiling :)</span>
			';
			$this->ModelActivityUser->setActivityUser($username,3,9);
			$this->session->set_userdata("errorMsg",$errorMsg);
			redirect('performance/set_atlet/'.$group_id.'','refresh');
		}else{
			$this->ModelActivityUser->setActivityUser($username,11,9);
			$errorMsg	= '
				<span class="btn red col s12">Gagal Membuat Profiling :(</span>
			';
			
			$this->session->set_userdata("errorMsg",$errorMsg);
			redirect('performance/formPerformance/data/'.$type.'','refresh');
		}
    }
	
	
	//NAMBAHIN YANG DARI GUFRON
	 public function mysql_aes_key($key)
    {
        $new_key = str_repeat(chr(0), 16);
        for($i=0,$len=strlen($key);$i<$len;$i++)
        {
            $new_key[$i%16] = $new_key[$i%16] ^ $key[$i];
        }
        return $new_key;
    }

    public function aes_encrypt($val)
    {
        $key = mysql_aes_key('Ralf_S_Engelschall__trainofthoughts');
        $pad_value = 16-(strlen($val) % 16);
        $val = str_pad($val, (16*(floor(strlen($val) / 16)+1)), chr($pad_value));
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
    }

    public function aes_decrypt($val)
    {
        $key = mysql_aes_key('Ralf_S_Engelschall__trainofthoughts');
        $val = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
        return rtrim($val, "..16");
    }

    public function random_val()
    {
        return substr(md5(uniqid(rand(), true)), 0, 5);
    }
	
		public function getEncription($str)
		{
			initEn();
			//return getEnc($str);
			echo getEnc($str);
		}
	
		public function getDecription()
		{
			$enc = $_GET['x'];
			initEn();
			//return getDec($str);
			echo getDec($enc);
		}
}