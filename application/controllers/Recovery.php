<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Recovery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_recovery');
		$this->load->model('m_home');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		$this->ModelActivityUser->setActivityUser($username,6,11);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($role_type == "CHC"){
			redirect("recovery/set_atlet");
		}
		if($role_type == "ATL"){
			redirect("recovery/listRecovery/".$username);
		}
		if($role_type == "KSC"){
			redirect("recovery/setGroup");
		}
		if($role_type == "SSC"){
			redirect("recovery/setGroup");
		}
		if($role_type == "PRIMA"){
			redirect("recovery/setGroup");
		}
		if($role_type == "RCV"){
			redirect("recovery/setGroup");
		}
		if($role_type == "MLP"){
			redirect("recovery/setGroup");
		}
		if($role_type == "HPD"){
			redirect("recovery/setGroup");
		}
		if($role_type == "SATLAK"){
			redirect("recovery/setGroup");
		}
		if($role_type == "PSY"){
			redirect("recovery/setGroup");
		}
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data['group'] = $this->m_home->getGroup($username);
		
		$data["page"] = "recovery/setGroup";
		$this->load->view("layout",$data);
	}
	
	function set_atlet($group_id = NULL){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$this->session->set_userdata("group_id",$group_id);
		
		
		$data["group_id"] = $group_id;
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		
		$data["page"] = "recovery/set_atlet";
		$this->load->view("layout",$data);
	}
	
	function searchAtlet(){
		$search_data = $this->input->post('input_data');
		$group_id = $this->input->post('group_id');
		$query = $this->m_recovery->get_autocomplete($search_data,$group_id);
		
		if($query){
			$list = "<ul class='collection'>";
			foreach ($query as $row){
				$name 		= $row->name;
				$gambar		= $row->gambar;
				$group		= $row->master_group_name;
				$event		= $row->master_atlet_nomor_event;
				$atlet		= $row->username;
				$wellness	= $row->value_wellness;
				$wellness_date	= $row->wellness_date;
				$dttm		= date("Y-m-d");
			
				if($wellness_date == $dttm){			
					if($wellness <= 59){
						$btn = "#FF0000";
					}elseif($wellness >= 60 && $wellness <= 69) {
						$btn = "#FF9D00";
					}elseif($wellness >= 70 && $wellness <= 79){
						$btn = "#E1FF00";
					}elseif($wellness >= 80 && $wellness <= 89){
						$btn = "#9BFF77";
					}else{
						$btn = "#00CE25";
					}				
				}else{
					$btn = "#607D8B";
				}
				
				$list .= '
					<a href="'.base_url().'index.php/recovery/listRecovery/'.$atlet.'" class="content"><li class="collection-item avatar">
						<img src="'.$gambar.'" alt="" class="circle">
						<span class="title">'.$name.'</span>
						<p>'.$group.'
							<br> '.$event.'
						</p>
						<a href="#!" class="secondary-content"><i style="color: '.$btn.'" class="mdi-action-favorite"></i></a>
					</li></a>
				';
			}
			$list .="</ul>";
		}else{
			$list = '<ul class="collection"><li class="collection-item avatar">
				<span class="title">Tidak Ada Atlet</span>
			</li>';
		}
		
		echo $list;
	}
	
	function listRecovery($atlet){
		$this->session->set_userdata("atlet",$atlet);
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["atlet"] = $atlet;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		if($this->session->userdata("year")){
			$year 	= $this->session->userdata("year");
		}else{
			$year	= date("Y");
		}
		if($this->session->userdata("month")){
			$month 	= $this->session->userdata("month");
		}else{
			$month	= date("m");
		}
		$data["year"]	= $year;
		$data["month"]	= $month;
		$data["listRecovery"] = $this->m_recovery->getMonotony($atlet,$year,$month);
		
		$data["page"] = "recovery/recovery_list";
		$this->load->view("layout",$data);
	}
	
	function showFilter(){
		$year		= $_POST["year"];
		$month	= $_POST["month"];
		$atlet	= $this->session->userdata("atlet");
		$this->session->set_userdata("year",$year);
		$this->session->set_userdata("month",$month);
		$monotony = $this->m_recovery->getMonotony($atlet,$year,$month);
		
		$ret = '<tbody>';
		if($monotony){
			foreach ($monotony as $key) {
				$date 			= $key->mdlmonotony_date;
				$monotony_id 	= $key->distribute_monotony;
				$vdt = date("Y-m-d",strtotime($date));
				$date = date("d M Y",strtotime($date));
				
				$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row 	= $query->row();
					$startMonotony 	= $row->startMonotony;
					$endMonotony 	= $row->endMonotony;
					$start			= date("d M Y",strtotime($startMonotony));
					$end			= date("d M Y",strtotime($endMonotony));
				}else{
					$start	= "";
					$end	= "";
				}
				
				$ret .= "
					<tr>
						<td style='font-weight:bold'><a class='btn-flat btn-brand' href='".base_url()."index.php/recovery/recoveryData/$monotony_id''>$start - $end</a></td>
					</tr>
				";
			}
		}else{
			$ret .= "<tr><td>Data Kosong</td></tr>";
		}
		$ret .= '</tbody>';
		
		$list = "<table class='striped'>"
			  . "<thead><tr><th style='font-size:12pt;text-align:center'>Weekly Training Load $year</th></tr></thead>"
			  . "$ret"
			  . "</table>";
			  
		echo $list;
	}
	
	function recoveryData($monotony_id){
		$this->session->set_userdata("monotony_id",$monotony_id);
		$atlet		= $this->session->userdata("atlet");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["atlet"] = $atlet;
		
		$sql = " SELECT d.master_group_category,a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
				$group_cat = $key->master_group_category;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
			$data['group_cat'] = $group_cat;
		}
		
		$data['recovery'] = $this->m_recovery->getRecovery($monotony_id);
		$data['monotony_id'] = $monotony_id;
		
		$data["page"] = "recovery/recovery_data";
		$this->load->view("layout",$data);
		
	}
	
	function modalPlan(){
		$dttm			= $_POST["dttm"];
		$monotony_id	= $_POST["monotony_id"];
		
		$modal = '
			<form method="post" action="'.base_url().'index.php/recovery/setPlanPoint">
			<div id="myModal_'.$dttm.'" class="modalPopup">
				<!-- Modal content -->
                  <div class="modal-content">
                    <h5>Recovery Plan</h5>
					<div class="form-group">
						<label>Recovery Plan Point</label>
						<input type="number" name="PlanPoint" class="form-contorl"/>
						<input type="hidden" name="monotony_id" class="form-contorl" value="'.$monotony_id.'"/>
						<input type="hidden" name="dttm" class="form-contorl" value="'.$dttm.'"/>
					</div>
					<button type="submit" class="btn-flat indigo white-text">Set Plan</button>  
					&nbsp<span onclick="closeModal(\''.$dttm.'\')" class="btn-flat white-text grey">Cancel</span>
                  </div>
			</div>
			</form>
		';
		echo $modal;
	}
	
	function recoveryDetail($date,$monotony_id){
		$atlet = $this->session->userdata("atlet");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["monotony_id"] 	= $monotony_id;
		$data["date"] 			= $date;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		$data["recoveryDetail"] = $this->m_recovery->getRecoveryDetail($date,$monotony_id,$atlet);
		$data["page"] = "recovery/recoveryDetail";
		$this->load->view("layout",$data);
	}
	
	
	function viewGrafik(){
		$atlet = $this->session->userdata("atlet");
		$sql = " SELECT d.master_group_category,a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
				$group_cat = $key->master_group_category;
			}
		}
		$monotony_id = $_POST["monotony_id"];
		$recovery	 = $this->m_recovery->getRecovery($monotony_id);
		
		if($recovery){
			$ttlMonotony = 0;
			$tmpdttm = 0;
			foreach ($recovery as $num => $key) {
				$date 		= $key->detail_date;
				$intensity	= $key->detail_intensity;
				$volume		= $key->detail_volume;
				
				$tmp_dttm = explode(" ", $date);
				$dtmm1 = $tmp_dttm[0];
				
				$sql	= "SELECT * FROM master_kondisi WHERE created_dttm >= '$dtmm1 00:00:00' AND created_dttm <= '$dtmm1 23:59:59'";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row		= $query->row();
					$soreness	= $row->soreness;
					$energi		= $row->energi;
					
					if($soreness == ""){
						$soreness = 0;
					}	
					if($energi == ""){
						$energi = 0;
					}				
				}else{
					$soreness 	= 0;
					$energi 	= 0;
				}
				
				$sql	= " SELECT SUM(b.recovery_point) as recovery_point FROM `master_recovery` as a"
						. " LEFT JOIN master_recovery_point as b on b.point_id = a.point_id"
						. " WHERE a.monotony_id = '$monotony_id'"
						. " AND a.recovery_dttm = '$dtmm1'";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row	= $query->row();
					$recovery_point	= $row->recovery_point;
					
					if($recovery_point == ""){
						$recovery_point = 0;
					}				
				}else{
					$recovery_point = 0;
				}
				
				$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' AND detail_date = '$date' group by detail_date");
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyPerDay = $row->monotonyPerDay;
				}else{
					$monotonyPerDay	= 0;
				}
				
				$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerWeek FROM `mdlmonotony_detail` "
										 . " WHERE detail_reference = '$monotony_id' ");
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyPerWeek = $row->monotonyPerWeek;
				}else{
					$monotonyPerWeek	= 0;
				}
				
				if($group_cat == "GW400"){		
					if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
						$target = 25;
					}else if($monotonyPerWeek <= 5000 OR $monotonyPerWeek < 6500){
						$target = 35;
					}else{
						$target = 40;
					}			
				}else{		
					if($monotonyPerWeek <= 2000 OR $monotonyPerWeek < 3500){
						$target = 25;
					}else if($monotonyPerWeek <= 3500 OR $monotonyPerWeek < 5000){
						$target = 35;
					}else{
						$target = 40;
					}
				}
				
				$plan =	($target/$monotonyPerWeek)*$monotonyPerDay;
				$recoveryPlan = number_format($plan);
				
				$sql	= " SELECT SUM(a.detail_intensity*a.detail_volume) as training_load"
						. " FROM `mdlmonotony_detail` as a"
						. " WHERE a.detail_reference = '$monotony_id'"
						. " AND a.detail_date = '$dtmm1' GROUP BY a.detail_date";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row	= $query->row();
					$ttl_trainingLoad	= $row->training_load;
					
					if($ttl_trainingLoad == ""){
						$ttl_trainingLoad = 0;
					}				
				}else{
					$ttl_trainingLoad = 0;
				}
				
				if($tmpdttm<>$date){
					$arrTanggal[]	= $date;
					$arrPoint[]		= (int)$recovery_point;
					$arrPlan[]		= (int)$recoveryPlan;
					$arrTraining[]	= (int)$ttl_trainingLoad;
					$arrSoreness[]	= (int)$soreness;
					$arrFatigue[]	= (int)$energi;
					$tmpdttm = $date;  
				}
				
			}
		
			$arrayData = array(
				'categories'=>$arrTanggal, 
				'point'=>$arrPoint,
				'plan'=>$arrPlan,
				'TrainingLoad'=>$arrTraining,
				'soreness'=>$arrSoreness,
				'fatigue'=>$arrFatigue,
			);
			$json = json_encode($arrayData);
			
			echo $json;
		}
	}
	
	function createRecovery($date,$monotony_id){
		$atlet		= $this->session->userdata("atlet");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["atlet"] = $atlet;
		$data["monotony_id"] 	= $monotony_id;
		$data["date"] 			= $date;
		
		$data['actual'] = $this->m_recovery->get_actual();
		
		$data["page"] = "recovery/createRecovery";
		$this->load->view("layout",$data);
	}
		
	function createDay(){
		$atlet		= $this->session->userdata("atlet");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$data["atlet"] = $atlet;
		
		$data['actual'] = $this->m_recovery->get_actual();
		
		$data["page"] = "recovery/createRecoveryDay";
		$this->load->view("layout",$data);
	}
	
	function saveRecovery(){
		$session 	= $this->session->userdata("login");
		$atlet		= $this->session->userdata("atlet");
		$monotony_id		= $this->session->userdata("monotony_id");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		
		$this->ModelActivityUser->setActivityUser($username,3,11);
		
		$recovery_dttm	= $_POST["date"];
		$monotony_id	= $_POST["monotony_id"];
		$created_dttm	= date('Y-m-d H:i:s');
		$atlet_id		= $atlet;
		$point 			= $_POST["point"];
		
		for($i = 0; $i < count($point); $i++){
			$recovery_id 	= $this->RecoveryID();
			
			$sql	= " INSERT INTO master_recovery (recovery_id,monotony_id,point_id,atlet_id,recovery_dttm,created_dttm)"
					. " VALUES('$recovery_id','$monotony_id','{$point[$i]}','$atlet_id','$recovery_dttm','$created_dttm')";
			$this->db->query($sql);
		}
		return true;
	}
	
	function saveRecoveryDay(){
		$session 	= $this->session->userdata("login");
		$atlet		= $this->session->userdata("atlet");
		$monotony_id		= $this->session->userdata("monotony_id");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_id	= $session["role_id"];
		$this->ModelActivityUser->setActivityUser($username,3,11);
		
		$created_dttm	= date('Y-m-d H:i:s');
		$atlet_id		= $atlet;
		$point 			= $_POST["point"];
		
		for($i = 0; $i < count($point); $i++){
			$recovery_id 	= $this->RecoveryID();
			
			$sql	= " INSERT INTO master_recovery (recovery_id,monotony_id,point_id,atlet_id,recovery_dttm,created_dttm)"
					. " VALUES('$recovery_id','','{$point[$i]}','$atlet_id',now(),now())";
			$this->db->query($sql);
		}
		return true;
	}
	
	function RecoveryID(){
		$tr = "RECO_";
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$sql = "SELECT left(a.recovery_id,5) as tr, mid(a.recovery_id,6,4) as fyear," 
			 . " mid(a.recovery_id,10,2) as fmonth, mid(a.recovery_id,12,2) as fday,"
			 . " right(a.recovery_id,4) as fno FROM master_recovery AS a"
			 . " where left(a.recovery_id,5) = '$tr' and mid(a.recovery_id,6,4) = '$year'"
			 . " and mid(a.recovery_id,10,2) = '$month' and mid(a.recovery_id,12,2)= '$day'"
			 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
			 
		$result = $this->db->query($sql);	
			
		if($result->num_rows($result) > 0) {
			$row = $result->row();
			$tr = $row->tr;
			$fyear = $row->fyear;
			$fmonth = $row->fmonth;
			$fday = $row->fday;
			$fno = $row->fno;
			$fno++;
		} else {
			$tr = $tr;
			$fyear = $year;
			$fmonth = $month;
			$fday = $day;
			$fno = 0;
			$fno++;
		}
		if (strlen($fno)==1){
			$strfno = "000".$fno;
		} else if (strlen($fno)==2){
			$strfno = "00".$fno;
		} else if (strlen($fno)==3){
			$strfno = "0".$fno;
		} else if (strlen($fno)==4){
			$strfno = $fno;
		}
		
		$RecoveryID = $tr.$fyear.$fmonth.$fday.$strfno;

		return $RecoveryID;
	}
	
	function setPlan(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($this->session->userdata("vdt")){
			$vdt	= $this->session->userdata("vdt");
		}else{
			$vdt	= date("Y-m-d");
		}
		if($this->session->userdata("vdt2")){
			$vdt2	= $this->session->userdata("vdt2");
		}else{
			$vdt2	= date("Y-m-d");
		}

		$data["vdt"] = $vdt;
		$data["vdt2"] = $vdt2;
		
		$group_id	= $this->session->userdata("group_id");
		
		$data["group_id"] = $group_id;
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		$data['monotonyList']	= $this->m_recovery->getRecoveryGroup($group_id);
		
		$data['page'] = 'recovery/setPlanRecovery';
		$this->load->view('layout',$data);
	}
	
	function getRecoveryAtlet(){
		$monotony_id	= $_POST["monotony_id"];
		// $sql = " SELECT MIN(detail_date) as start, MAX(detail_date) as end"
			 // . " FROM `mdlmonotony_detail` WHERE detail_reference = '$monotony_id'";
		// $query	= $this->db->query($sql);
		// if($query->num_rows()>0){
			// $row	= $query->row();
			// $start	= $row->start;
			// $end	= $row->end;
		// }
		
		$tmp = explode("_",$monotony_id);
		$start  = $tmp[0];
		$end = $tmp[1];
		
		// echo $start.$end;
		// return;
		
		$group_id	= $this->session->userdata("group_id");
		$atlet		= $this->m_home->getAtlet($group_id);
		if($atlet){
			$list = '<div class="col s12 m4 10">
						<p>
							<input type="checkbox" onClick="toggle(this)" id="all"/>
							<label for="all" class="black-text"><b>Select All</b></label>
						</p><hr>
					</div>';
			foreach($atlet as $row){
				$name 		= $row->name;
				$username	= $row->username;
				$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
					 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
					 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
					 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
					 . " WHERE a.username = '$username'";
				$query = $this->db->query($sql);
				if($query->num_rows() > 0){
					$result = $query->result();
					foreach ($result as $key) {
						$group_name = $key->master_group_name;
						$no_event 	= $key->master_atlet_nomor_event;
						$nama		= $key->name;
						$atlet		= $key->username;
						$gambar_atl = $key->gambar_atl;
						$wellness	= $key->value_wellness;
						$wellness_date = $key->wellness_date;
					}
				}
				$sql = " SELECT a.distribute_monotony,b.detail_date,b.detail_reference, "
					 . " SUM(b.detail_volume*b.detail_intensity) as monotonyPerDay "
					 . " FROM mdlmonotony_distribute as a"
					 . " LEFT JOIN mdlmonotony_detail as b on b.detail_reference = a.distribute_monotony"
					 . " LEFT JOIN mdlmonotony as c on c.mdlmonotony_id = a.distribute_monotony"
					 . " WHERE a.distribute_athlete = '$username'"
					 . " AND b.detail_date >= '$start'"
					 . " AND b.detail_date <= '$end'"
					 . " AND c.mdlmonotony_active = '0'"
					 . " GROUP BY b.detail_reference"
					 . " ORDER BY b.detail_date DESC, monotonyPerDay DESC LIMIT 1";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$disabled	= "";
				}else{
					$disabled 	= "disabled";
				}
				
				$dttm		= date("Y-m-d");				
				if($wellness_date == $dttm){			
					if($wellness <= 59){
						$btn = "#FF0000";
					}elseif($wellness >= 60 && $wellness <= 69) {
						$btn = "#FF9D00";
					}elseif($wellness >= 70 && $wellness <= 79){
						$btn = "#E1FF00";
					}elseif($wellness >= 80 && $wellness <= 89){
						$btn = "#9BFF77";
					}else{
						$btn = "#00CE25";
					}				
				}else{
					$btn = "#607D8B";
				}
				
				$list .= '
					<div class="col s12 m4 10">
					<p>
						<input '.$disabled.' type="checkbox" class="filled-in" id="atlet_'.$username.'" name="atlet[]" value="'.$username.'"/>
						<label for="atlet_'.$username.'">'.$name.'</label>
						<a href="#!" class="secondary-content"><i style="color: '.$btn.'; font-size: 25px" class="mdi-action-favorite"></i></a>
					</p><hr>
					</div>
				';
			}
		}else{
			$list = '<li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
		}
		
		echo $list;
		return;
	
	}
	
	function setRecoveryPlan(){
		$arrAtlet	 = $_POST["atlet"];
		$monotony_id = $_POST["trainingLoad"];
		$tmp = explode("_",$monotony_id);
		$start  = $tmp[0];
		$end = $tmp[1];
		// $sql = " SELECT a.distribute_monotony,b.detail_date,b.detail_reference, "
			 // . " SUM(b.detail_volume*b.detail_intensity) as monotonyPerDay "
			 // . " FROM mdlmonotony_distribute as a"
			 // . " LEFT JOIN mdlmonotony_detail as b on b.detail_reference = a.distribute_monotony"
			 // . " LEFT JOIN mdlmonotony as c on c.mdlmonotony_id = a.distribute_monotony"
			 // . " WHERE a.distribute_athlete = '$username'"
			 // . " AND b.detail_date >= '$start'"
			 // . " AND b.detail_date <= '$end'"
			 // . " AND c.mdlmonotony_active = '0'"
			 // . " GROUP BY b.detail_reference"
			 // . " ORDER BY b.detail_date DESC, monotonyPerDay DESC LIMIT 1";
		// $query	= $this->db->query($sql);
		// if($query->num_rows()>0){
			// $row	= $query->row();
			// $monotony_id = $row->distribute_monotony;
		// }else{
			// $monotony_id 	= "disabled";
		// }
		
		// echo $monotony_id;
		// return;
		// $sql = " SELECT MIN(detail_date) as start, MAX(detail_date) as end"
			 // . " FROM `mdlmonotony_detail` WHERE detail_reference = '$monotony_id'";
		// $query	= $this->db->query($sql);
		// if($query->num_rows()>0){
			// $row	= $query->row();
			// $start	= $row->start;
			// $end	= $row->end;
		// }
		
		$this->session->set_userdata("vdt",$start);
		$this->session->set_userdata("vdt2",$end);
		$this->session->set_userdata("sessMonotony",$monotony_id);
		$this->session->set_userdata("arrAtlet",$arrAtlet);
		
		
		echo "sukses";
	}
	
	function formRecoveryPlan(){
		$arrAtlet = $this->session->userdata("arrAtlet");
		foreach($arrAtlet as $row => $atlet){
		}	
			// echo $atlet;
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$vdt		= $this->session->userdata("vdt");
		$vdt2		= $this->session->userdata("vdt2");
		$jmlHari	= $this->hitungHari($vdt,$vdt2);
		$monotony_id	= $this->session->userdata("sessMonotony");
		
		$data["username"] = $atlet;
		$data['jmlHari'] = $jmlHari;
		$data['vdt'] 	 = $vdt;
		$data['vdt2'] 	 = $vdt2;
		$data['monotonyID'] = $monotony_id;
		$data['group_id'] = $this->session->userdata("group_id");
		$data['page'] 	 = 'recovery/formRecoveryPlan';
		$this->load->view('layout',$data);
	}
	
	function hitungHari($awal,$akhir){
		$tglAwal = strtotime($awal);
		$tglAkhir = strtotime($akhir);
		$jeda = abs($tglAkhir - $tglAwal);
		return floor($jeda/(60*60*24))+1;
	}
	
	function showFormPlan(){
		$dttm 		= $_POST["date"];
		
		$sql 	= "SELECT a.*, b.recovery_type, b.recovery_image as image FROM master_recovery_point as a"
				. " LEFT JOIN master_recovery_type as b on b.type_id = a.type_id";
		$query	= $this->db->query($sql);
		$input = "";
		if($query->num_rows()>0){
			$result = $query->result();
			$tmp = 0;
			$rowspan = 0;
			foreach($result as $key){
				$point_id		= $key->point_id;
				$recovery_nm	= $key->recovery_name;
				$point			= $key->recovery_point;
				$image			= $key->image;
				$type_id		= $key->type_id;
				$type_nm		= $key->recovery_type;
				
				// $cek	= $this->db->query("SELECT * FROM master_recovery_plan WHERE monotony_id = '$monotonyID' AND recoveryPlanDttm='$dttm' AND recoveryPoint='$point_id'");
				// if($cek->num_rows()>0){
					// $slc	= "checked";			
				// }else{
					$slc	= "";
				// }
				
				$img = '';
				if($tmp<>$type_id){
					$input .= "<thead><tr><th colspan='3'><img width='100%' src='".base_url()."assets/images/".$image."'/></th></tr><tr><th colspan='3'><h6 class='margin-top-no margin-bottom-no text-black'><b>$type_nm</b></h6></th></tr></thead>";
					$tmp = $type_id;
				}
				$input.="<tr><td><input $slc type='checkbox' name='point[]' value='$point_id' id='point_$point_id'/><label for='point_$point_id'></label> $recovery_nm</td><td>$point Pts</td></tr>";
			}		
		}
		
		$modal = '
			<div id="myModal_'.$dttm.'" class="modalPopup">
				<!-- Modal content -->
                <div class="modal-content">
					<h6 class="close" onclick="closeModal(\''.$dttm.'\')">X</h6>
					<p>Recovery Plan Per Tanggal <b>'.date('d M Y',strtotime($dttm)).'</b></p>
					<div class="table-responsive">
						<table class="table">
							'.$input.'							
							<tr><td><button class="btn btn-flat indigo white-text" onclick="savePlan(\''.$dttm.'\')">Simpan</button> &nbsp
							<button class="btn btn-flat red white-text" onclick="closeModal(\''.$dttm.'\')">Cancel</button></td></tr>
						</table>
					</div>
				</div>
			</div>
		';
		
		echo $modal;
	}
	
	function savePlan(){
		$session 	= $this->session->userdata("login");
		$username	= $session["username"];
		$dttm		= $_POST["dttm"];
		$point		= $_POST["data"];
		$arrAtlet	= $this->session->userdata("arrAtlet");
		$this->ModelActivityUser->setActivityUser($username,3,11);
		
		for($i=0;$i<count($arrAtlet);$i++){
			$cek	= $this->db->query("SELECT * FROM master_recovery_plan WHERE atlet_id = '{$arrAtlet[$i]}' AND recoveryPlanDttm='$dttm'");
			if($cek->num_rows()>0){
				$this->db->query("DELETE FROM master_recovery_plan WHERE atlet_id = '{$arrAtlet[$i]}' AND recoveryPlanDttm='$dttm'");				
			}
			
			for($j = 0; $j < count($point); $j++){
				$recoveryPlanID 	= $this->RecoveryPlanID();
				
				$sql	= " INSERT INTO master_recovery_plan (recoveryPlanID,recoveryPoint,atlet_id,recoveryPlanDttm,created_dttm,created_user_id)"
						. " VALUES('$recoveryPlanID','{$point[$j]}','{$arrAtlet[$i]}','$dttm',now(),'$username')";
				$query 	= $this->db->query($sql);
				if($query){
					$status = "sukses";
				}else{
					$status = "gagal";
				}
			}
		}
		
		echo $status;
	}
	
	function RecoveryPlanID(){
		$tr = "PLAN_";
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$sql = "SELECT left(a.recoveryPlanID,5) as tr, mid(a.recoveryPlanID,6,4) as fyear," 
			 . " mid(a.recoveryPlanID,10,2) as fmonth, mid(a.recoveryPlanID,12,2) as fday,"
			 . " right(a.recoveryPlanID,4) as fno FROM master_recovery_plan AS a"
			 . " where left(a.recoveryPlanID,5) = '$tr' and mid(a.recoveryPlanID,6,4) = '$year'"
			 . " and mid(a.recoveryPlanID,10,2) = '$month' and mid(a.recoveryPlanID,12,2)= '$day'"
			 . " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
			 
		$result = $this->db->query($sql);	
			
		if($result->num_rows($result) > 0) {
			$row = $result->row();
			$tr = $row->tr;
			$fyear = $row->fyear;
			$fmonth = $row->fmonth;
			$fday = $row->fday;
			$fno = $row->fno;
			$fno++;
		} else {
			$tr = $tr;
			$fyear = $year;
			$fmonth = $month;
			$fday = $day;
			$fno = 0;
			$fno++;
		}
		if (strlen($fno)==1){
			$strfno = "000".$fno;
		} else if (strlen($fno)==2){
			$strfno = "00".$fno;
		} else if (strlen($fno)==3){
			$strfno = "0".$fno;
		} else if (strlen($fno)==4){
			$strfno = $fno;
		}
		
		$recoveryPlanID = $tr.$fyear.$fmonth.$fday.$strfno;

		return $recoveryPlanID;
	}
	
	function viewPlan(){
		$dttm			= $_POST["dttm"];
		$atlet	= $_POST["atlet"];
		
		$cek	= $this->db->query(" SELECT a.*,b.* FROM master_recovery_plan as a"
							     . " LEFT JOIN master_recovery_point as b on a.recoveryPoint = b.point_id"
								 . " WHERE a.atlet_id = '$atlet' AND a.recoveryPlanDttm='$dttm'");
		if($cek->num_rows()>0){
			$plan	= "";
			$no		= 1;
			foreach($cek->result() as $row){
				$recoveryNm		= $row->recovery_name;
				$recoveryPts	= $row->recovery_point;
				$plan .= "<tr><td>$no</td><td>$recoveryNm</td><td>$recoveryPts Point</td></tr>";
				
				$no++;
			}
		}else{
			$plan	= "<tr><td colspan='4'>Belum Ada Recovery Plan</td></tr>";
		}
		
		$modal = '
			<div id="myModal_'.$dttm.''.$atlet.'" class="modalPopup">
				<!-- Modal content -->
                <div class="modal-content" style="width:100%;z-index:999;">
					<h6 class="close" onclick="closeModal(\''.$dttm.'\',\''.$atlet.'\')">X</h6>
					<p>Recovery Plan Per Tanggal <b>'.date('d M Y',strtotime($dttm)).'</b></p>
					<div class="table-responsive">
						<table class="striped">
							<thead><tr><th>No</th><th>Jenis Recovery</th><th>Point</th></tr></thead>
							'.$plan.'
						</table>
					</div>
				</div>
			</div>
		';
		
		echo $modal;
	}
}