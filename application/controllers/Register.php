<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Register extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_login');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function form(){
// 		$this->ModelActivityUser->setActivityUser('',6,16);
		$this->load->view("register/form");
	}
	
	function createUser(){
		$name 			= $_POST['name'];
        $username 		= $_POST['username'];
		$licence_code 	= $_POST['licence'];
        $device_id 		= $this->detectDevice();
        $device_name 	= $this->detectDevice();
        $password 		= $_POST['password'];
        $provinsi 		= $_POST['provinsi'];
        $birth 			= $_POST['birth'];
        $gender			= $_POST['gender'];
		
		$data = array($name,$username,$licence_code,$device_id,$device_name,$password,$provinsi,$birth,$gender);
		
		$save = $this->m_login->createUser($data);
		
		if($save == "sukses"){			
			$this->ModelActivityUser->setActivityUser($username,8,16);
		}else{
			$this->ModelActivityUser->setActivityUser($username,11,16);
		}
		
		echo $save;
		return;
	}
	
	function detectDevice(){
		$userAgent = $_SERVER["HTTP_USER_AGENT"];
		$devicesTypes = array(
			"computer" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
			"tablet"   => array("tablet", "android", "ipad", "tablet.*firefox"),
			"mobile"   => array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
			"bot"      => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
		);
		foreach($devicesTypes as $deviceType => $devices) {  
			foreach($devices as $device) {
				if(preg_match("/" . $device . "/i", $userAgent)) {
					$deviceName = $device;
				}
			}
		}
		return ucfirst($deviceName);
	}
}