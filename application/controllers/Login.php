<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_login');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function form(){		
		if($this->session->userdata("login")){
			redirect("home");
		}
// 		$this->ModelActivityUser->setActivityUser('',12,18);
		$this->load->view("login/form");
	}
	
	function validation(){
		$username = $_POST["username"];
		$password = $_POST["password"];
        $result = $this->m_login->login($username,$password);
		
		if($result == "wrong_password"){
			$this->ModelActivityUser->setActivityUser($username,9,14);
			$message = '<div class=""><div class="input-field col s12">'
					  .'<span class="btn cyan col s12">Username / Password Salah</span></div></div>';
			$this->session->set_userdata("errormsg",$message);
			redirect("login/form");
			return;
		}
		if($result == "not_allowed"){
			$this->ModelActivityUser->setActivityUser($username,7,14);
			$message = '<div class=""><div class="input-field col s12">'
					  .'<span class="btn cyan col s12">Anda Tidak Mempunyai Akses</span></div></div>';
			$this->session->set_userdata("errormsg",$message);
			redirect("login/form");
			return;
		}
		if($result == "error"){
			$this->ModelActivityUser->setActivityUser($username,7,14);
			$message = '<div class=""><div class="input-field col s12">'
					  .'<span class="btn cyan col s12">Anda Belum Terdaftar</span></div></div>';
			$this->session->set_userdata("errormsg",$message);
			redirect("login/form");
			return;
		}
		
		if($result){
			foreach($result as $row){
				$name 		= $row->name;
				$username 	= $row->username;
				$gambar 	= $row->gambar;
				$role_name	= $row->role_name;
				$role_id 	= $row->role_id;
				$role_type 	= $row->role_type;
				$group_id 	= $row->group_id;
				
				$this->ModelActivityUser->setActivityUser($username,1,18);
				
				$data = array(
					'name' 		=> $name,
					'username' 	=> $username,
					'gambar' 	=> $gambar,
					'role_id' 	=> $role_id,
					'role_name' => $role_name,
					'role_type' => $role_type,
					'group_id' 	=> $group_id
				);
				
				$this->session->set_userdata('login',$data);
			}
			
			// print_r($result);
			// echo $role_type;
			// return;
			
			redirect("home");
		}
	}
	
	function validationMobile(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$username = $request->username;
		$password = $request->password;
        $result = $this->m_login->login($username,$password);
		
		if($result == "wrong_password"){
			$status = "wrong_password";
		}
		if($result == "not_allowed"){
			$status = "not_allowed";
		}
		if($result == "error"){
			$status = "not_allowed";
		}
		
		if($result){
			$status = "success";
			foreach($result as $row){
				$name 		= $row->name;
				$username 	= $row->username;
				$gambar 	= $row->gambar;
				$role_name	= $row->role_name;
				$role_id 	= $row->role_id;
				$role_type 	= $row->role_type;
				$group_id 	= $row->group_id;
				
				$this->ModelActivityUser->setActivityUser($username,1,18);
			}
		}
		
		$arrLogin = array(
						"status" => $status,
						"name"	 => $name,
						"username" => $username,
						"gambar" => $gambar,
						"role_name" => $role_name,
						"role_id" => $role_id,
						"role_type" => $role_type,
						"group_id" => $group_id
					);
		$json	= json_encode($arrLogin);
		echo $json;
		return;
	}
	
	public function logout(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$this->ModelActivityUser->setActivityUser($username,2,7);
        $this->session->unset_userdata('login');
        redirect('login/form','refresh');
    }
}