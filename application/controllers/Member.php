<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Member extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('M_member');
		$this->load->model("m_wellness");
		$this->load->model("m_home");
		$this->load->model("ModelActivityUser");
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$this->ModelActivityUser->setActivityUser($username,6,1);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($role_type == "ATL"){
			redirect("member/memberList/".$group_id);
		}
		if($role_type == "RCV"){
			redirect("member/setGroup");
		}
		if($role_type == "MLP"){
			redirect("member/setGroup");
		}
		if($role_type == "KSC"){
			redirect("member/setGroup");
		}
		if($role_type == "SSC"){
			redirect("member/setGroup");
		}
		if($role_type == "SATLAK"){
			redirect("member/setGroup");
		}
		if($role_type == "PRIMA"){
			redirect("member/setGroup");
		}
		if($role_type == "HPD"){
			redirect("member/setGroup");
		}
		
		$data["page"] = "home/home";
		$this->load->view("layout",$data);
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data['group'] = $this->M_member->getGroup($username);
		
		$data["page"] = "member/setGroup";
		$this->load->view("layout",$data);
	}
	
	function memberList($group_id){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data['member']	= $this->M_member->getMember($group_id);
		
		$data["page"] = "member/member";
		$this->load->view("layout",$data);
	}
	
	function dataMemember($user_id){
		$this->session->set_userdata("atlet_id",$user_id);
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$user_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		$kpfPhysical  	= $this->M_member->getKPFphysical($atlet);
		$KPFtechnical  	= $this->M_member->getKPFtechnical($atlet);
		$KPFpsichology  	= $this->M_member->getKPFpsichology($atlet);
		
		$data["kpfPhysical"] 	= $kpfPhysical;
		$data["KPFtechnical"] 	= $KPFtechnical;
		$data["KPFpsichology"] 	= $KPFpsichology;
		$data["page"] = "member/dataMemember";
		$this->load->view("layout",$data);
	}
	
	function personalInformation(){
		$user_id	= $this->session->userdata("atlet_id");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$user_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		$data['personal'] 	 = $this->m_home->get_userData($user_id);
		$data['keluarga'] 	 = $this->m_home->get_userFamilyData($user_id);
		$data['health'] 	 = $this->m_home->get_userHealthData($user_id);
		$data['club'] 		 = $this->m_home->get_userClubData($user_id);
		
		$data["page"] = "member/personalMember";
		$this->load->view("layout",$data);
	}
	
	function WellnessInformation(){
		$user_id	= $this->session->userdata("atlet_id");
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$user_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar;
			$data['gambar_atl'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		if($this->session->userdata("year")){
			$year	= $this->session->userdata("year");
		}else{
			$year	= date("Y");
		}
		
		if($this->session->userdata("month")){
			$month	= $this->session->userdata("month");
		}else{
			$month	= date("m");
		}
		
		$data['month']	= $month;
		$data['year']		= $year;
		$data['wellness'] 	 = $this->m_wellness->getWellness($user_id,$month,$year);
		
		$data["page"] = "member/wellnessMember";
		$this->load->view("layout",$data);
	}
	
	function showFilter(){
		$atlet = $this->session->userdata('atlet_id');
		$month = $_POST['month'];
		$year  = $_POST['year'];
		
		$this->session->set_userdata('month',$month);
		$this->session->set_userdata('year',$year);
		$penilaian = $this->m_wellness->getWellness($atlet,$month,$year);
		$ret = '
			<table class="responsive-table striped">
                    <thead>
                      <tr>
						<td data-field="no" class="blue white-text">No</td>
						<td data-field="tanggal" class="blue white-text">Tanggal</td>
						<td data-field="lengthTidur" class="blue white-text">Lama Tidur</td>
						<td data-field="QTidur" class="blue white-text">Kualitas Tidur</td>
						<td data-field="soreness" class="blue white-text">Soreness</td>
						<td data-field="fatigue" class="blue white-text">Fatigue</td>
						<td class="blue white-text">Mood</td>
						<td class="blue white-text">Stress</td>
						<td class="blue white-text">Fokus</td>
						<td class="blue white-text">Jml Nutrisi</td>
						<td class="blue white-text">Kualitas Nutrisi</td>
						<td class="blue white-text">Hidrasi</td>
						<td class="blue white-text">BB</td>
						<td class="blue white-text">RHR</td>
						<td class="blue white-text">Cidera</td>
						<td class="blue white-text">Total</td>
                      </tr>
                    </thead>
					</tbody>
		';
		if($penilaian){
			$no = 1;
			foreach ($penilaian as $key) {
				$date = date('d M Y',strtotime($key->created_dttm));
				$lama_tidur = $key->lama_tidur;
				$kualitas_tidur = $key->kualitas_tidur;
				$soreness	=  $key->soreness;
				$energi 	= $key->energi;
				$mood 		= $key->mood;
				$stress 	= $key->stress;
				$mental 	= $key->mental;
				$jml_nutrisi = $key->jml_nutrisi;
				$kualitas_nutrisi = $key->kualitas_nutrisi;
				$hidrasi 	= $key->hidrasi;
				
				$lama_tidur_clr 	= $this->getColor($lama_tidur);
				$kualitas_tidur_clr = $this->getColor($kualitas_tidur);
				$soreness_clr 		= $this->getColor($soreness);
				$energi_clr 		= $this->getColor($energi);
				$mood_clr 			= $this->getColor($mood);
				$stress_clr 		= $this->getColor($stress);
				$mental_clr 		= $this->getColor($mental);
				$jml_nutrisi_clr 	= $this->getColor($jml_nutrisi);
				$kualitas_nutrisi_clr 	= $this->getColor($kualitas_nutrisi);
				$hidrasi_clr 		= $this->getColor($hidrasi);
			
				$cidera = $key->cidera;
				if($cidera == ""){
					$cidera = "-";
				}
				
				$nilai = array(
					$lama_tidur = $key->lama_tidur * 2,
					$kualitas_tidur = $key->kualitas_tidur * 2,
					$soreness =  $key->soreness * 2,
					$energi = $key->energi * 2,
					$mood = $key->mood * 2,
					$stress = $key->stress * 2,
					$mental = $key->mental * 2,
					$jml_nutrisi = $key->jml_nutrisi * 2,
					$kualitas_nutrisi = $key->kualitas_nutrisi * 2,
					$hidrasi = $key->hidrasi * 2
				);
				$total = array_sum($nilai);
				if($total <= 59){
					$btn = "#FF0000";
				}elseif($total >= 60 && $total <= 69) {
					$btn = "#FF9D00";
				}elseif($total >= 70 && $total <= 79){
					$btn = "#E1FF00";
				}elseif($total >= 80 && $total <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}
				
				$ret .= "
					<tr>
						<td>$no</td>
						<td>$date</td>
						<td style='background : $lama_tidur_clr; text-align:center'>$key->lama_tidur</td>
						<td style='background : $kualitas_nutrisi_clr; text-align:center'>$key->kualitas_tidur</td>
						<td style='background : $soreness_clr; text-align:center'>$key->soreness</td>
						<td style='background : $energi_clr; text-align:center'>$key->energi</td>
						<td style='background : $mood_clr; text-align:center'>$key->mood</td>
						<td style='background : $stress_clr; text-align:center'>$key->stress</td>
						<td style='background : $mental_clr; text-align:center'>$key->mental</td>
						<td style='background : $jml_nutrisi_clr; text-align:center'>$key->jml_nutrisi</td>
						<td style='background : $kualitas_nutrisi_clr; text-align:center'>$key->kualitas_nutrisi</td>
						<td style='background : $hidrasi_clr; text-align:center'>$key->hidrasi</td>
						<td style='text-align:center'>$key->berat</td>
						<td style='text-align:center'>$key->nadi</td>
						<td style='text-align:center'>$cidera</td>
						<td style='background : $btn; text-align:center'>$total</td>
					</tr>
				";
				$no++; 
			}
		} else{
			$ret .= "<tr><td colspan='15'>Tidak Ada Data</td></tr>";
		}
		
		$ret .="</tbody></table>";
		
		echo $ret;
		return;
	}
	
	private function getColor($data){
		if($data == 1){
			$btn = "#FF0000";
		}elseif($data == 2) {
			$btn = "#FF9D00";
		}elseif($data == 3){
			$btn = "#E1FF00";
		}elseif($data == 4){
			$btn = "#9BFF77";
		}else{
			$btn = "#00CE25";
		}
		
		return $btn;
	}
	
	function viewGrafik(){		
		$atlet = $this->session->userdata('atlet_id');
		$month = $_POST["month"];
		$year  = $_POST["year"];
		$wellness = $this->m_wellness->getWellness($atlet,$month,$year);
		
		if($wellness){
			foreach ($wellness as $key) {
				$date = date('d M',strtotime($key->created_dttm));

				$nilai = array(
					$lama_tidur = $key->lama_tidur * 2,
					$kualitas_tidur = $key->kualitas_tidur * 2,
					$soreness =  $key->soreness * 2,
					$energi = $key->energi * 2,
					$mood = $key->mood * 2,
					$stress = $key->stress * 2,
					$mental = $key->mental * 2,
					$jml_nutrisi = $key->jml_nutrisi * 2,
					$kualitas_nutrisi = $key->kualitas_nutrisi * 2,
					$hidrasi = $key->hidrasi * 2
				);
				
				$nadi 		= $key->nadi;
				$berat 		= $key->berat;
				$energi		= $key->energi;
				$soreness 	= $key->soreness;
				$hidrasi	= $key->hidrasi;
				$mood	 	= $key->mood;
				$stress	 	= $key->stress;
				$mental	 	= $key->mental;

				$total = array_sum($nilai);
				$sum = array_sum($nilai);
				if($sum <= 59){
					$btn = "#FF0000";
				}elseif($sum >= 60 && $sum <= 69) {
					$btn = "#FF9D00";
				}elseif($sum >= 70 && $sum <= 79){
					$btn = "#E1FF00";
				}elseif($sum >= 80 && $sum <= 89){
					$btn = "#9BFF77";
				}else{
					$btn = "#00CE25";
				}
				
				$arrBtn[] 		= $btn;
				$arrTanggal[] 	= $date;
				$arrScore[] 	= (int)$total;
				$arrNadi[]		= (int)$nadi;
				$arrBerat[]		= (int)$berat;
				$arrEnergi[]	= (int)$energi;
				$arrSoreness[]	= (int)$soreness;
				$arrHidrasi[]	= (int)$hidrasi;
				$arrMood[]		= (int)$mood;
				$arrStress[]	= (int)$stress;
				$arrMental[]	= (int)$mental;

				// if(count($total)) {
					// $total .= ",";
					// $btn .= "',";
				// }
			}
			$arrayData = array(
				'categories'=>$arrTanggal, 
				'nadi'=>$arrNadi, 
				'berat'=>$arrBerat, 
				'fatigue'=>$arrEnergi, 
				'soreness'=>$arrSoreness, 
				'hidrasi'=>$arrHidrasi,
				'wellness'=>$arrScore,
				'mood'=>$arrMood,
				'stress'=>$arrStress,
				'focus'=>$arrMental
			);
			$json = json_encode($arrayData);
			
			echo $json;
		}
	}
}