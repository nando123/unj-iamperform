<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_home');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		$this->ModelActivityUser->setActivityUser($username,6,2);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data["role_type"] = $role_type;
		$sql = " SELECT a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$username'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
			}
			$data['gambar'] = $gambar_atl;
			$data['name'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
		}
		$data["username"]	 = $username;
		$data['personal'] 	 = $this->m_home->get_userData($username);
		$data['keluarga'] 	 = $this->m_home->get_userFamilyData($username);
		$data['health'] 	 = $this->m_home->get_userHealthData($username);
		$data['club'] 		 = $this->m_home->get_userClubData($username);
		
		$data["page"] = "home/profile";
		$this->load->view("layout",$data);
	}
	
	function updatePersonal(){ 
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name		= $session["name"];
		$this->ModelActivityUser->setActivityUser($username,4,2);
		
		$nomor_event = $this->input->post('nomor_event');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$handphone = $this->input->post('handphone');
		$email = $this->input->post('email');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$agama = $this->input->post('agama');
		$tinggi_badan = $this->input->post('tinggi_badan');
		$golongan_darah = $this->input->post('golongan_darah');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$pendidikan = $this->input->post('pendidikan');
		$pekerjaan = $this->input->post('pekerjaan');
		$status = $this->input->post('status');
		$nomor_ktp = $this->input->post('nomor_ktp');
		$npwp = $this->input->post('npwp');
		
		$data = array(
			'username' => $username,
			'nama' => $name,
			'cabang' => '',
			'nomor_event' => $nomor_event,
			'alamat' => $alamat,
			'telp' => $telp,
			'handphone' => $handphone,
			'email' => $email,
			'tempat_lahir' => $tempat_lahir,
			'tgl_lahir' => $tgl_lahir,
			'agama' => $agama,
			'tinggi_badan' => $tinggi_badan,
			'golongan_darah' => $golongan_darah,
			'jenis_kelamin' => $jenis_kelamin,
			'pendidikan' => $pendidikan,
			'pekerjaan' => $pekerjaan,
			'status' => $status,
			'nomor_ktp' => $nomor_ktp,
			'npwp' => $npwp
		);
		
		// print_r($data);
		// return;
		
		$personal = $this->m_home->savePersonal($data);
		
		echo "<script>window.history.back(-1)</script>";
		return;
	}
	
	function updateFamily(){
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name		= $session["name"];
		$this->ModelActivityUser->setActivityUser($username,4,2);
			
		$orangtua = $this->input->post('orangtua');
		$pasangan = $this->input->post('pasangan');
		$anak = $this->input->post('anak');
		$nomor_lain = $this->input->post('nomor_lain');
		
		$data = array(
			'username' 	=> $username,
			'orangtua' 	=> $orangtua,
			'pasangan' 	=> $pasangan,
			'anak' 		=> $anak,
			'nomor_lain' => $nomor_lain
		);
		
		$family = $this->m_home->updateFamily($data);
		
		echo "<script>window.history.back(-1)</script>";
		return;
	}
	
	function updateHealth(){
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name		= $session["name"];
		$this->ModelActivityUser->setActivityUser($username,4,2);
			
		$cedera = $this->input->post('cedera');
		$alergi = $this->input->post('alergi');
		$lemak = $this->input->post('lemak');
		
		$data = array(
			'username' => $username,
			'cedera' => $cedera,
			'alergi' => $alergi,
			'lemak' => $lemak
		);
		
		$family = $this->m_home->updateHealth($data);
		
		echo "<script>window.history.back(-1)</script>";
		return;
	}
	
	function updateClub(){
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name		= $session["name"];
		$this->ModelActivityUser->setActivityUser($username,4,2);
			
		$club = $this->input->post('club');
		$alamat_club = $this->input->post('alamat_club');
		$email_club = $this->input->post('email_club');
		$prestasi = $this->input->post('prestasi');
		$pengalaman = $this->input->post('pengalaman');
		
		$data = array(
			'username' => $username,
			'club' => $club,
			'alamat_club' => $alamat_club,
			'email_club' => $email_club,
			'prestasi' => $prestasi,
			'pengalaman' => ''
		);
		
		
		$club = $this->m_home->updateClub($data);
		
		echo "<script>window.history.back(-1)</script>";
		return;
	}
	
	function updateApparel(){
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name		= $session["name"];
		$this->ModelActivityUser->setActivityUser($username,4,2);
			
		$kaos = $this->input->post('kaos');
		$jaket = $this->input->post('jaket');
		$celana = $this->input->post('celana');
		$sepatu = $this->input->post('sepatu');
		
		$data = array(
			'username' => $session['username'],
			'kaos' => $kaos,
			'jaket' => $jaket,
			'celana' => $celana,
			'sepatu' => $sepatu
		);
		
		
		$club = $this->m_home->updateApparel($data);
		
		echo "<script>window.history.back(-1)</script>";
		return;
	}
}