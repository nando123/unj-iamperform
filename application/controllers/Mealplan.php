<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Mealplan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		// $this->load->model('M_Home');
		$this->load->model('ModelActivityUser');
		$this->load->database();
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$this->ModelActivityUser->setActivityUser($username,6,12);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data["page"] = "mealplan/mealplan";
		$this->load->view("layout",$data);
	}
	
	public function logout(){
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->unset_userdata('login');
        redirect('home','refresh');
    }
}