<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Account extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_home');
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$this->ModelActivityUser->setActivityUser($username,6,3);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$data["role_type"] = $role_type;
		$sql = " SELECT a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$username'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
			}
			$data['gambar'] = $gambar_atl;
			$data['name'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
		}
		$data["username"] = $username;
		
		$data["page"] = "home/account";
		$this->load->view("layout",$data);
	}
	
	function updateName(){ 
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$name = $_POST["name"];
		
		$this->ModelActivityUser->setActivityUser($username,4,3);
		
		$sql = "UPDATE users SET name='$name' WHERE username = '$username'";
		$query = $this->db->query($sql);
		if($query){
			echo "sukses";
		}else{
			echo "gagal";
		}
	}
	
	function formpassword(){
		$form = '
			<div class="row margin">
				<div class="input-field col s12">
					<label for="oldpassword" >Old Password </label>
					<input id="oldpassword" type="password">
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12">
					<label for="newpassword" >New Password </label>
					<input id="newpassword" type="password">
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12">
					<label for="renewpassword" >Re-Type New Password </label>
					<input id="renewpassword" type="password">
				</div>
			</div>
			<p class="red-text" id="errorPassword"></p>
			<div class="row margin">
				<span class="btn-flat cyan white-text" onClick="updatePassword()">Save</span>
			</div>
			<script>	
				function updatePassword(){
					var oldpassword = $("#oldpassword").val();
					var newpassword = $("#newpassword").val();
					var renewpassword = $("#renewpassword").val();
					
					if(newpassword != renewpassword){
						$("#errorPassword").html("Password Tidak Sama");
						return;
					}
					
					$.ajax({
						type : "POST",
						url  : "'.base_url().'index.php/account/saveNewPassword",
						data : {oldpassword:oldpassword,newpassword:newpassword,renewpassword:renewpassword},
						// dataType: "json",
						success: function(data){
							if(data == "wrong_password"){
								$("#errorPassword").html("Password Salah");
								return;
							}
							if(data == "error"){
								$("#errorPassword").html("Tidak Dapat Mengganti Password");
								return;
							}
							if(data == "sukses"){
								window.location.reload();
							}
						},error: function(xhr, ajaxOptions, thrownError){            
							alert(xhr.responseText);
						}
					});	
				}
			</script>
		';
		
		echo $form;
		return;
	}
	
	function saveNewPassword(){
		$session = $this->session->userdata("login");
		$username	= $session["username"];
		$oldpassword = $_POST["oldpassword"];
		$newpassword = $_POST["newpassword"];
		$renewpassword = $_POST["renewpassword"];
		
		$sql = " SELECT a.*,b.* FROM users as a"
			 . " LEFT JOIN master_role as b on b.role_id = a.role_id"
			 . " WHERE a.username = '$username'";
			
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			$row 	= $query->row();
			$salt	= $row->salt;
			$enc_p	= $row->encrypted_password;
			$code	= $row->license_code;
			$hash 	= $this->checkhashSSHA($salt, $oldpassword);			
			if ($enc_p == $hash) {
				$password_md5 = md5($newpassword);
				$uuid = uniqid('', true);
				$hash = $this->hashSSHA($newpassword);
				$encrypted_password = $hash["encrypted"]; // encrypted password
				$salt = $hash["salt"]; // salt
				$sql = " UPDATE users SET unique_id = '$uuid',encrypted_password = '$encrypted_password' ,salt = '$salt'"
						. " WHERE username = '$username'";
				$query = $this->db->query($sql);
				if($query){
					$this->ModelActivityUser->setActivityUser($username,4,18);
					echo "sukses";
					return;
				}else{
					$this->ModelActivityUser->setActivityUser($username,12,18);
					echo "error";
				}
			}else{
				$this->ModelActivityUser->setActivityUser($username,12,18);
				echo "wrong_password";
				return;
			}
		}
	}
	
	function hashSSHA($password) {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
	
	function checkhashSSHA($salt, $password) {
		$hash = base64_encode(sha1($password . $salt, true) . $salt);

		return $hash;
	}

	function createThumb($path1, $path2, $file_type, $new_w, $new_h, $squareSize = ''){
		/* read the source image */
		$source_image = FALSE;

		if (preg_match("/jpg|JPG|jpeg|JPEG/", $file_type)) {
			$source_image = imagecreatefromjpeg($path1);
		}
		else if (preg_match("/png|PNG/", $file_type)) {
			if (!$source_image = @imagecreatefrompng($path1)) {
				$source_image = imagecreatefromjpeg($path1);
			}
		}
		elseif (preg_match("/gif|GIF/", $file_type)) {
			$source_image = imagecreatefromgif($path1);
		}  
		if ($source_image == FALSE) {
			$source_image = imagecreatefromjpeg($path1);
		}

		$orig_w = imageSX($source_image);
		$orig_h = imageSY($source_image);

		if ($orig_w < $new_w && $orig_h < $new_h) {
			$desired_width = $orig_w;
			$desired_height = $orig_h;
		} else {
			$scale = min($new_w / $orig_w, $new_h / $orig_h);
			$desired_width = ceil($scale * $orig_w);
			$desired_height = ceil($scale * $orig_h);
		}

		if ($squareSize != '') {
			$desired_width = $desired_height = $squareSize;
		}

		/* create a new, "virtual" image */
		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
		// for PNG background white----------->
		$kek = imagecolorallocate($virtual_image, 255, 255, 255);
		imagefill($virtual_image, 0, 0, $kek);

		if ($squareSize == '') {
			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $orig_w, $orig_h);
		} else {
			$wm = $orig_w / $squareSize;
			$hm = $orig_h / $squareSize;
			$h_height = $squareSize / 2;
			$w_height = $squareSize / 2;

			if ($orig_w > $orig_h) {
				$adjusted_width = $orig_w / $hm;
				$half_width = $adjusted_width / 2;
				$int_width = $half_width - $w_height;
				imagecopyresampled($virtual_image, $source_image, -$int_width, 0, 0, 0, $adjusted_width, $squareSize, $orig_w, $orig_h);
			}

			elseif (($orig_w <= $orig_h)) {
				$adjusted_height = $orig_h / $wm;
				$half_height = $adjusted_height / 2;
				imagecopyresampled($virtual_image, $source_image, 0,0, 0, 0, $squareSize, $adjusted_height, $orig_w, $orig_h);
			} else {
				imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $squareSize, $squareSize, $orig_w, $orig_h);
			}
		}

		if (@imagejpeg($virtual_image, $path2, 90)) {
			imagedestroy($virtual_image);
			imagedestroy($source_image);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function uploadProfile(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$group_id	= $session["group_id"];
		if (isset($_FILES['image_upload_file'])) {
			$output['status'] = FALSE;
			set_time_limit(0);
			$allowedImageType = array(
				"image/gif",
				"image/jpeg",
				"image/pjpeg",
				"image/png",
				"image/x-png"
			);
			
			if ($_FILES['image_upload_file']["error"] > 0) {
				$output['error'] = "Error in File";
			} elseif (!in_array($_FILES['image_upload_file']["type"], $allowedImageType)) {
				$output['error'] = "You can only upload JPG, PNG and GIF file";
			} elseif (round($_FILES['image_upload_file']["size"] / 1024) > 4096) {
				$output['error'] = "You can upload file size up to 4 MB";
			} else {
				/*create directory with 777 permission if not exist - start*/
				// createDir(IMAGE_SMALL_DIR);
				// createDir(IMAGE_MEDIUM_DIR);
				/*create directory with 777 permission if not exist - end*/
				$path[0]     = $_FILES['image_upload_file']['tmp_name'];
				$file        = pathinfo($_FILES['image_upload_file']['name']);
				$fileType    = $file["extension"];
				$desiredExt  = 'jpg';
				$fileNameNew = rand(333, 999) . time() . ".$desiredExt";
				$path[1]     = 'assets/pictures/'.$fileNameNew;
				$path[2]     = 'assets/pictures/'.$fileNameNew;
				
				$url = base_url().'assets/pictures/'.$fileNameNew;
				$sql = "UPDATE users SET gambar = '$url' WHERE username = '$username'";
				$this->db->query($sql);
				
				if ($this->createThumb($path[0], $path[1], $fileType, 250, 250, 250)) {
					
					if ($this->createThumb($path[1], $path[2], "$desiredExt", 250, 250, 250)) {
						$output['status']       = TRUE;
						$output['image_medium'] = $path[1];
						$output['image_small']  = $path[2];
					}
				}
			}
			$this->ModelActivityUser->setActivityUser($username,4,19);
			echo json_encode($output);
		}
	}
}