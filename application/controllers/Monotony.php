<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Monotony extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_monotony');
		$this->load->model('m_home');
		
		$this->load->model('ModelActivityUser');
		date_default_timezone_set('Asia/Jakarta');
		
		if(!$this->session->userdata("login")){
			redirect("login/form");
		}
	}
	
	function index(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$this->ModelActivityUser->setActivityUser($username,6,10);
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if($role_type == "CHC"){
			redirect("monotony/set_atlet");
		}
		if($role_type == "ATL"){
			redirect("monotony/listMonotony/".$username);
		}
		if($role_type == "KSC"){
			redirect("monotony/setGroup");
		}
		if($role_type == "SSC"){
			redirect("monotony/setGroup");
		}
		if($role_type == "PRIMA"){
			redirect("monotony/setGroup");
		}
		if($role_type == "RCV"){
			redirect("monotony/setGroup");
		}
		if($role_type == "MLP"){
			redirect("monotony/setGroup");
		}
		if($role_type == "HPD"){
			redirect("monotony/setGroup");
		}
		if($role_type == "SATLAK"){
			redirect("monotony/setGroup");
		}
		if($role_type == "PSY"){
			redirect("monotony/setGroup");
		}
	}
	
	function setGroup(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
				
		
		$data['group'] = $this->m_home->getGroup($username);
		
		$data["page"] = "monotony/setGroup";
		$this->load->view("layout",$data);
	}
	
	function set_atlet($group_id = NULL){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$this->session->set_userdata("group",$group_id);
		
		
		$data['atlet'] = $this->m_home->getAtlet($group_id);
		$data["group_id"]	= $group_id;
		
		$data["page"] = "monotony/set_atlet";
		$this->load->view("layout",$data);
	}
	
	function createStepZero($group_id){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;

		$data["selectWeek"]	= $this->m_home->getWeek();
		$data['atlet'] = $this->m_home->getAtlet($group_id);		
		
		$data['page'] = 'monotony/createFormZero';
		$this->load->view('layout',$data);
	}
	
	function createStepOne(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$startWeek	= $this->session->userdata("startWeek");
		$listWeekByID	=	$this->m_home->listWeekByID($startWeek);
		list($week,$start_date,$end_date,$year)	= $listWeekByID;
		$jmlHari	= $this->m_home->hitungHari($start_date,$end_date);
		$year		= date("Y", strtotime($start_date));
		$month		= date("m", strtotime($start_date));
		$day		= date("d", strtotime($start_date));
		$ret 		= "";
		for($i=0;$i<=$jmlHari;$i++){
			$date 	 = mktime(0,0,0,$month,$day+$i,$year);
			$dayDate = date("d M Y", $date);
			$date 	 = date("Y-m-d", $date);
			
			$ret .=' 
			<div class="col-sm-6">
				<div class="form-group form-group-label">
					<label class="floating-label" for="inputSessi">'.$dayDate.' / Session</label>
					<input onKeypress="cekValue(event)" class="form-control" idinputSessi type="text" name="day'.$i.'" value="2" required>
				</div>
			</div>';
		}
		$data["ret"]	= $ret;
		$data['page'] = 'monotony/createFormOne';
		$this->load->view('layout',$data);
	}
	
	function createStepTwo(){
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		
		$weekID			= $this->session->userdata("startWeek");
		$listWeekByID 	= $this->m_home->listWeekByID($weekID);
		
		$data['form'] = array(
			"start"				=> $start,
			"day" 				=> "day[]",
			"session" 			=> "session[%idx%][]",
			"scale" 			=> "scale[%idx%][]",
			"volume" 			=> "volume[%idx%][]",
			"phase" 			=> "phase",
			"goal" 				=> "goal",
			"description"		=> "description",
			"lastweek" 			=> "lastweek",
			"save" 				=> "save"
		);
		
		$daySet = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
		
		$data['data'] = array(
			"start" 	=> $start,
		);
		$data['data']['date'] 		= array();
		$counter = 0;
		$key 	 = "day";
		while(isset($_POST['day'.$counter])) {

			$w 	= date('w', strtotime($start. ' +'.($counter).' day'));
			
			$data['data']['date'][$counter]['COUNT'] 	= $_POST['day'.$counter];
			$data['data']['date'][$counter]['DAY'] 		= $daySet[$w];
			$data['data']['date'][$counter]['DATE'] 	= date('Y-m-d', strtotime($start. ' +'.($counter).' day'));
			$data['data']['rowcount']  				   += $_POST['day'.$counter];
			$counter++;

		}//END WHILE
		
		
		$data['page'] 	= 'monotony/createFormTwo';
		$this->load->view('layout',$data);
	}
	
	function setAtletMonotony(){
		$atlet		= $_POST["atlet"];
		$startDate	= $_POST["startDate"];
		
		$session	= $this->session->set_userdata("atletMonotony",$atlet);
		$session	= $this->session->set_userdata("startWeek",$startDate);
		
		echo "sukses";
	}
	
	public function searchAtlet() {
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
			
		$name 		= $this->input->post('input_data');
		$group_id	= $this->input->post('group_id');
		
		if(isset($group_id)){
			$group_id = $group_id;
		}else{
			$group_id = $session["group_id"];
		}
		
		$query = $this->m_monotony->get_autocomplete($name,$group_id);
			
			// print_r($query);
			// return;
			
			if($query){
				$list = "<ul class='collection'>";
				foreach ($query as $row){
					$name 		= $row->name;
					$gambar		= $row->gambar;
					$group		= $row->master_group_name;
					$event		= $row->master_atlet_nomor_event;
					$atlet		= $row->username;
					$wellness	= $row->value_wellness;
					if($wellness <= 59){
						$btn = "#FF0000";
					}elseif($wellness >= 60 && $wellness <= 69) {
						$btn = "#FF9D00";
					}elseif($wellness >= 70 && $wellness <= 79){
						$btn = "#E1FF00";
					}elseif($wellness >= 80 && $wellness <= 89){
						$btn = "#9BFF77";
					}else{
						$btn = "#00CE25";
					}
					$list .= '
						<a href="'.base_url().'index.php/monotony/listMonotony/'.$atlet.'" class="content"><li class="collection-item avatar">
							<img src="'.$gambar.'" alt="" class="circle">
							<span class="title">'.$name.'</span>
							<p>'.$group.'
								<br> '.$event.'
							</p>
							<a href="#!" class="secondary-content"><i style="color: '.$btn.'" class="mdi-action-favorite"></i></a>
						</li></a>
					';
				}
				$list .="</ul>";				
			}else{
				$list = '<ul class="collection"><li class="collection-item avatar">
					<span class="title">Tidak Ada Atlet</span>
				</li>';
			}
			
			echo $list;
    }
	
	function dataMonotony($monotony_id){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$atlet = $this->session->userdata("atlet");
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar_atl'] = $gambar_atl;
			$data['pic'] = $gambar_atl;
			$data['nama_user'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		$table 	= $this->m_monotony->getMonotonyID($monotony_id);
		$trmnt	= $this->m_monotony->getMonotonyTarget($monotony_id);
		
		if($trmnt){
			list($target,$desc,$last)= $trmnt;
		}else{
			$target = '';
			$desc 	= '';
		}
		
		$cell = array(
				"value" 		=> array(),
				"wtl" 			=> 0
		);
		
		if($table){
			$sample 				= array();
			$TotalScaleVolume 		= 0;
			foreach($table as $num => $row){
				$mdldate[$num] 		= $row->detail_date;
				$sessType[$num] 	= $row->detail_session;
				$detIntensity[$num]	= $row->detail_intensity;
				$detVolume[$num]	= $row->detail_volume;
				$detTrainingLoad[$num] = $detIntensity[$num] * $detVolume[$num];
				if(!isset($cell['value'][$row->detail_date])) {
					$cell['value'][$row->detail_date]['count'] = 1;
					$cell['value'][$row->detail_date]['trainingday']  	= 0 + $detTrainingLoad[$num];
					$sample[$row->detail_date] 							= 0 + $detTrainingLoad[$num];
				}else {
					$cell['value'][$row->detail_date]['count'] 			+= 1;
					$cell['value'][$row->detail_date]['trainingday'] 	+= $detTrainingLoad[$num];
					$sample[$row->detail_date] 							+= $detTrainingLoad[$num];
				}
			}
			$STDEV 		= ceil($this->m_monotony->STDEV($sample));
			$TOTAL 		= ceil($this->m_monotony->SUM($sample));
			$AVERAGE 	= ceil($this->m_monotony->AVERAGE($sample));
			if($AVERAGE==0 OR $STDEV==0) {
				$DIV 	= 0;
			}else {
				$DIV 	= $AVERAGE / $STDEV;
			}//END ELSE IF
			$VARIATION 	= round($DIV, 1);
			$LOAD 		= round($TOTAL * ($AVERAGE/$STDEV));

		}
		
		$data['variation']	= $VARIATION;
		$data['target']		= $target;
		$data['desc']		= $desc;
		$data['load']		= $LOAD;
		$data['total']		= $TOTAL;
		$data['last']		= $last;
		$data['average']	= $AVERAGE;
		$data['stdev']		= $STDEV;
		$data['cell']		= $cell;
		$data['table']		= $table;
		$data['monotony_id'] = $monotony_id;
		
		$data["page"] = "monotony/monotony_data";
		$this->load->view("layout",$data);
	}
	
	function viewGrafik(){
		$monotony_id = $_POST["monotony_id"];
		$table 	= $this->m_monotony->getMonotonyID($monotony_id);
		if($table){
			foreach($table as $row){
				$volume = $row->detail_volume;
				$date	= $row->detail_date;
				$rpe	= $row->detail_intensity;
				$actual	= $row->detail_actual;
				
				$load	= $rpe*$volume;
				
				$arrLoad[] 		= (int)$load;
				$arrdate[]		= date("d M Y", strtotime($date));
				$arrRpe[]		= (int)$rpe;
				$arrActual[]	= (int)$actual;
			}
			$arrayData = array('categories'=>$arrdate, 'load'=>$arrLoad, 'rpe'=>$arrRpe, 'actual'=>$arrActual);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}
	}
	
	function viewGrafikDay(){
		$monotony_id = $_POST["monotony_id"];
		$table 	= $this->m_monotony->getGrafikDay($monotony_id);
		if($table){
			foreach($table as $row){
				$date	= $row->detail_date;
				$rpe	= $row->detail_intensity;
				$actual	= $row->detail_actual;
				
				$load	= $row->trainingLoad;
				
				$arrLoad[] 		= (int)$load;
				$arrdate[]		= date("d M Y", strtotime($date));
				$arrRpe[]		= (int)$rpe;
				$arrActual[]	= (int)$actual;
			}
			$arrayData = array('categories'=>$arrdate, 'load'=>$arrLoad, 'rpe'=>$arrRpe, 'actual'=>$arrActual);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}
	}
	
	function viewGrafikWeek(){
		$year	= $_POST["year"];
		$month	= $_POST["month"];
		$atlet	= $this->session->userdata("atlet");
		$monotony = $this->m_monotony->getMonotony($atlet,$year,$month);
		
		if($monotony){
			foreach ($monotony as $key) {
				$date 			= $key->mdlmonotony_date;
				$monotony_id 	= $key->distribute_monotony;
				$vdt = date("Y-m-d",strtotime($date));
				
				$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row 	= $query->row();
					$startMonotony 	= $row->startMonotony;
					$endMonotony 	= $row->endMonotony;
					$start			= date("d M Y",strtotime($startMonotony));
					$end			= date("d M Y",strtotime($endMonotony));
				}else{
					$start	= "";
					$end	= "";
				}
				
				$query	= $this->db->query(" SELECT SUM(detail_volume*detail_intensity) as monotonyPerDay FROM `mdlmonotony_detail`"
									 . " WHERE detail_reference = '$monotony_id' group by detail_reference");
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyPerDay = $row->monotonyPerDay;
				}else{
					$monotonyPerDay	= 0;
				}
				
				$arrDate[]		= $start."-".$end;
				$arrVolume[]	= (int)$monotonyPerDay;
			}
			
			$arrayData = array('categories'=>$arrDate, 'volume'=>$arrVolume);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}
	}
	
	function listMonotony($atlet){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		$group_id	= $this->session->userdata("group");
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		$this->session->set_userdata("atlet",$atlet);
		
		$sql = " SELECT a.value_wellness,a.wellness_date,a.gambar as gambar_atl,a.name,a.username,b.master_atlet_nomor_event,d.master_group_name FROM users as a"
			 . " LEFT JOIN master_information_personal as b on b.master_atlet_username = a.username"
			 . " LEFT JOIN master_role as c on c.role_id = a.role_id"
			 . " LEFT JOIN master_group as d on d.master_group_id = c.group_id"
			 . " WHERE a.username = '$atlet'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach ($result as $key) {
				$group_name = $key->master_group_name;
				$no_event = $key->master_atlet_nomor_event;
				$nama	= $key->name;
				$atlet	= $key->username;
				$gambar_atl = $key->gambar_atl;
				$value_wellness = $key->value_wellness;
				$wellness_date = $key->wellness_date;
			}
			$data['gambar'] = $gambar_atl;
			$data['pic'] = $gambar_atl;
			$data['atlet_name'] = $nama;
			$data['group_name'] = $group_name;
			$data['no_event'] = $no_event;
			$data['atlet'] = $atlet;
			$data['wellness_date'] = $wellness_date;
			$data['value_wellness'] = $value_wellness;
		}
		
		if($this->session->userdata("year")){
			$year 	= $this->session->userdata("year");
		}else{
			$year	= date("Y");
		}
		
		if($this->session->userdata("month")){
			$month 	= $this->session->userdata("month");
		}else{
			$month	= date("m");
		}
		
		$data["monotony"] = $this->m_monotony->getMonotony($atlet,$year,$month);
		$data["atlet"] = $atlet;
		$data["year"] = $year;
		$data["month"] = $month;
		$data["group_id"] = $group_id;
		$data['page']	= 'monotony/monotony_list';
		$this->load->view('layout',$data);
	}
	
	function showFilter(){
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		$gambar		= $session["gambar"];
		$role_type	= $session["role_type"];
		$role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		$data["gambar"] = $gambar;
		$data["name"] = $name;
		$data["username"] = $username;
		$data["role_type"] = $role_type;
		$data["role_name"] = $role_name;
		
		$year	= $_POST["year"];
		$month	= $_POST["month"];
		$atlet	= $this->session->userdata("atlet");
		$this->session->set_userdata("year",$year);
		$this->session->set_userdata("month",$month);
		$monotony = $this->m_monotony->getMonotony($atlet,$year,$month);
		
		$ret = '<tbody>';
		if($monotony){
			foreach ($monotony as $key) {
				$date 			= $key->mdlmonotony_date;
				$monotony_id 	= $key->distribute_monotony;
				$vdt = date("Y-m-d",strtotime($date));
				$date = date("d M Y H:i:s",strtotime($date));
				
				$sql	= "SELECT MIN(detail_date) as startMonotony, MAX(detail_date) as endMonotony FROM mdlmonotony_detail WHERE detail_reference = '$monotony_id' ";
				$query	= $this->db->query($sql);
				if($query->num_rows()>0){
					$row 	= $query->row();
					$startMonotony 	= $row->startMonotony;
					$endMonotony 	= $row->endMonotony;
					$start			= date("d M Y",strtotime($startMonotony));
					$end			= date("d M Y",strtotime($endMonotony));
				}else{
					$startMonotony	= "";
				}
				
				if($role_type == "KSC"){
					$btnDelete	= "<span class='mdi-action-delete red-text waves-effect waves-light' onClick='showDelete(\"$monotony_id\")'></span>";
				}else{
					$btnDelete = "";
				}
				
				$ret .= "
					<tr id='data_$monotony_id'>
						<tr>
							<td class='indigo white-text' colspan='3'>Training Load</td>
						</tr>
						<tr>
							<td style='font-weight:bold'>
								<a href='".base_url()."index.php/monotony/dataMonotony/$monotony_id''>$start - $end</a>
							</td>
							<td>$btnDelete</td>
						</tr>
						<tr>
							<td colspan='3'>Dibuat Pada $date</td>
						</tr>
					</tr>
				";
			}
		}else{
			$ret .= "<tr><td>Data Kosong</td></tr>";
		}
		$ret .= '</tbody>';
		
		$list = "<table class='striped'>"
			  . "<thead><tr><th style='font-size:12pt;text-align:center'>Weekly Training Load $year</th></tr></thead>"
			  . "$ret"
			  . "</table>";
			  
		echo $list;
	}
	
	// function createMonotonyOne(){
		// $session = $this->session->userdata("login");
		// $name		= $session["name"];
		// $username	= $session["username"];
		// $gambar		= $session["gambar"];
		// $role_type	= $session["role_type"];
		// $role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		// $data["gambar"] = $gambar;
		// $data["name"] = $name;
		// $data["username"] = $username;
		// $data["role_type"] = $role_type;
		// $data["role_name"] = $role_name;
		// $atlet = $this->session->userdata("atlet");
		// $data['dump'] = array(
			// "datenow" 	=> date("Y-m-d"),
			// "format" 	=> "yyyy-mm-dd"
		// );
		// $data['form'] = array(
			// "start"	=> "start",
			// "day1" 	=> "day1",
			// "day2" 	=> "day2",
			// "day3" 	=> "day3",
			// "day4" 	=> "day4",
			// "day5" 	=> "day5",
			// "day6" 	=> "day6",
			// "day7" 	=> "day7"
		// );
		// $data['page']	= 'monotony/formMonotonyOne';
		// $this->load->view('layout',$data);
	// }
	
	// function createMonotonyTwo(){
		// $session = $this->session->userdata("login");
		// $name		= $session["name"];
		// $username	= $session["username"];
		// $gambar		= $session["gambar"];
		// $role_type	= $session["role_type"];
		// $role_name	= $session["role_name"];
		// $group_id	= $session["group_id"];
		
		// $data["gambar"] = $gambar;
		// $data["name"] = $name;
		// $data["username"] = $username;
		// $data["role_type"] = $role_type;
		// $data["role_name"] = $role_name;
		// $atlet = $this->session->userdata("atlet");
		// $vdt	= $_POST["start"];
		// $start 	= date("Y-m-d",strtotime($vdt));
		
		// $data['form'] = array(
			// "start"				=> "start",
			// "day" 				=> "day[]",
			// "session" 			=> "session[%idx%][]",
			// "scale" 			=> "scale[%idx%][]",
			// "volume" 			=> "volume[%idx%][]",
			// "phase" 			=> "phase",
			// "goal" 				=> "goal",
			// "description"		=> "description",
			// "lastweek" 			=> "lastweek",
			// "save" 				=> "save"
		// );
		
		// $daySet = array("MIN","SEN","SEL","RAB","KAM","JUM","SAB");
		
		// $data['data'] = array(
			// "start" 	=> $start,
		// );
		// $data['data']['rowcount'] 	= 0;
		// $data['data']['date'] 		= array();
		// $counter = 1;
		// $key 	 = "day";
		// while(isset($_POST['day'.$counter])) {

			// $w 	= date('w', strtotime($_POST['start']. ' +'.($counter-1).' day'));
			
			// $data['data']['date'][$counter-1]['COUNT'] 	= $_POST['day'.$counter];
			// $data['data']['date'][$counter-1]['DAY'] 	= $daySet[$w];
			// $data['data']['date'][$counter-1]['DATE'] 	= date('Y-m-d', strtotime($_POST['start']. ' +'.($counter-1).' day'));
			// $data['data']['rowcount']  				   += $_POST['day'.$counter];
			// $counter++;

		// }//END WHILE
		
		
		// $data['page']	= 'monotony/formMonotonyTwo';
		// $this->load->view('layout',$data);
	// }
	
	function saveMonotony(){
		$session 	= $this->session->userdata('login');
		$user_id 	= $session['username'];
		
		$startWeek	= $this->session->userdata("startWeek");
		$listWeekByID	=	$this->m_home->listWeekByID($startWeek);
		list($week,$StartDay,$end_date,$year)	= $listWeekByID;
		$target 	= $_POST["goal"];
		$phase		= $_POST["phase"];
		$atlet		= $this->session->userdata("atletMonotony");
		
		$PostSession = $_POST["session"];
		$PostScale 	 = $_POST["scale"];
		$PostVolume  = $_POST["volume"];
		
		$args = array($target,$StartDay,$phase,$PostSession,$PostScale,$PostVolume);
		
		$save = $this->m_monotony->saveMonotony($args,$user_id,$atlet);
		
		$group	= $this->session->userdata("group");
		
		// echo $save;
		if($save){
			echo "sukses";
			return;
		}else{
			echo "gagal";
			return;
		}
	}
	
	function showDeleteConfirm(){
		$monotony_id = $_POST["monotony_id"];
		$modal = '
			<div id="myModal_'.$monotony_id.'" class="modalPopup">
				<!-- Modal content -->
                  <div class="modal-content">
                    <p>Yakin Ingin Menghapus Data Ini ?</p>
					<span onClick="deleteMonotony(\''.$monotony_id.'\')" class="btn-flat red white-text">Delete</span>  
					&nbsp<span onclick="closeModal(\''.$monotony_id.'\')" class="btn-flat white-text grey">Cancel</span>
                  </div>
			</div>
		';
		echo $modal;
	}
	
	function deleteMonotony(){
		$monotony_id	= $_POST["monotony_id"];
		$session = $this->session->userdata("login");
		$name		= $session["name"];
		$username	= $session["username"];
		
		$query	= $this->db->query("UPDATE mdlmonotony SET mdlmonotony_active = '1' WHERE mdlmonotony_id = '$monotony_id'");
		
		if($query){
			$this->ModelActivityUser->setActivityUser($username,5,10);
			echo "sukses";
			return;
		}else{
			$this->ModelActivityUser->setActivityUser($username,14,10);
			echo "gagal";
			return;
		}
	}
}